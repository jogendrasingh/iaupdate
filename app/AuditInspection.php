<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditInspection extends Model
{
    protected $guarded = [];
}
