<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\AuditInspection;
use Auth;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use App\User;

class ReportController extends Controller
{
    public function plannedwarehouses(){
        if (! Gate::allows('reports_manage')) {
            return abort(401);
        }

        $month = date('m');
        $year = date('Y');

        $misdata = DB::table('misdata')
                        ->select('*')
                        ->whereMonth('created_at', '=', $month)
                        ->whereYear('created_at', '=', $year)
                        ->orderBy('id','desc')
                        ->get();
        
        return view('admin/reports/plannedwarehouses',compact('misdata'));              
    }

    public function auditedwarehouses(){
        if (! Gate::allows('reports_manage')) {
            return abort(401);
        }

        $month = date('m');
        $year = date('Y');

        $auditinspections = DB::table('audit_inspections')
                        ->select('*')
                        ->whereMonth('created_at', '=', $month)
                        ->whereYear('created_at', '=', $year)
                        ->where('approved_status','=', 2)
                        ->orderBy('id','desc')
                        ->get();
        
        return view('admin/reports/auditedwarehouses',compact('auditinspections'));             
    }

    public function clientwiseplannedwarehouses(Request $request){
        if (! Gate::allows('reports_manage')) {
            return abort(401);
        }

        /* FROM DATE FILTER */
        if($request->get('from_date')){
            $from= date("Y-m-d",strtotime($request->get('from_date')));
        }else{
            $from=date("Y-m-01"); 
        }
        /* TO DATE FILTER */
        if($request->get('to_date')){
            $to=date("Y-m-d",strtotime($request->get('to_date'))); 
        }else{
            $to = date("Y-m-t");
        }

        
        $role = Auth::user()->roles->pluck('name')->first();
        $userName = Auth::user()->name;

        if($request->get('name_of_checker')){
            $userName=$checker=$request->get('name_of_checker'); 
            $role = 'checker';
        }

        //Code for all the checkers for admin
        $checkers = User::whereHas("roles", function($q){ $q->where("name", "checker"); })->get();

        //print_r($checkers->toArray());die;
        
        //DB::enableQueryLog();
        $plannedWarehouseCount = DB::table('misdata')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })        
                        ->groupBy('type_of_warehouse')
                        ->get();
        //dd(DB::getQueryLog());                

        $plannedWarehouseArr = array();

        if (count($plannedWarehouseCount) > 0) {
            $total = 0;
            foreach ($plannedWarehouseCount as $pData) {
                $plannedWarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $plannedWarehouseArr['total'] = $total;
        }

        //DB::enableQueryLog();
        $c=['approved_status','=', 2];  
        $c1 =['approved_status','=', 4];        
        $auditedwarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->where(function($q) use ($c,$c1){
                        $q->where([$c])->orWhere([$c1]);
                        })
                        ->groupBy('type_of_warehouse')
                        ->get();
         //dd(DB::getQueryLog());
                

       //print_r($auditedwarehouseCount);die;                 

        $auditedwarehouseArr = array();         
        if (count($auditedwarehouseCount) > 0) {
            $total = 0;
            foreach ($auditedwarehouseCount as $pData) {
                $auditedwarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $auditedwarehouseArr['total'] = $total;
        }               

        $checkerCond = '';
        if($role=='checker'){
            $checkerCond = 'and A.name_of_checker="'.$userName.'"';
        }

        //DB::enableQueryLog();
        $reppendl1warehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,audit_inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select audit_inspection_id, count(audit_inspection_id) as count from audit_escalations where status in('0','3') group by audit_inspection_id) r on r.audit_inspection_id=A.id  where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' ".$checkerCond."");
        //dd(DB::getQueryLog());

        //echo "<pre>";
        //print_r($reppendl1warehouseCount);die;
    

        $reppendl1warehouseArr = array();           
        if (count($reppendl1warehouseCount) > 0) {
                $pwhtotal = 0;
                $cmtotal = 0;
                $pwhcmtotal = 0;
                foreach ($reppendl1warehouseCount as $pData) {
                    if($pData->type_of_warehouse=='PWH'){
                        $pwhtotal = $pwhtotal + $pData->insCount;
                        $reppendl1warehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                            
                    }elseif($pData->type_of_warehouse=='CM'){
                        $cmtotal = $cmtotal + $pData->insCount;
                        $reppendl1warehouseArr[$pData->type_of_warehouse] = $cmtotal;
                        
                    }elseif($pData->type_of_warehouse=='PWH-CM'){
                        $pwhcmtotal = $pwhcmtotal + $pData->insCount;
                        $reppendl1warehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                        
                    }
                }
                $reppendl1warehouseArr['total'] = $pwhtotal+$cmtotal+$pwhcmtotal;
            }

        //echo "<pre>";
        //print_r($reppendl1warehouseArr);die;
                            

                        

       $notauditedwarehouseCount = DB::table('misdata')
                        ->select(
                            'misdata.type_of_warehouse',
                            DB::raw('count(*) as count')
                        )
                        ->where('misdata.created_at','>=',$from)
                        ->where('misdata.created_at','<=',$to)
                        ->whereNotExists( function ($query) use ($to, $from) {
                            $query->select(DB::raw(1))
                            ->from('audit_inspections')
                            ->whereRaw('audit_inspections.warehouse_code=misdata.warehouse_code')
                            //->whereBetween('audit_inspections.created_at', [$from, $to]);
                            ->where('audit_inspections.created_at','>=',$from)
                            ->where('audit_inspections.created_at','<=',$to);
                        })
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('misdata.name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('misdata.type_of_warehouse')
                        ->get();
        

        $notauditedwarehouseArr = array();          
        if (count($notauditedwarehouseCount) > 0) {
            $total = 0;
            foreach ($notauditedwarehouseCount as $pData) {
                $notauditedwarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $notauditedwarehouseArr['total'] = $total;
        }               


        $warehouseUnderFumigationCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where('whole_warehouse_under_fumigation','Yes')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();

        $warehouseUnderFumigationArr = array();         
        if (count($warehouseUnderFumigationCount) > 0) {
            $total = 0;
            foreach ($warehouseUnderFumigationCount as $pData) {
                $warehouseUnderFumigationArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $warehouseUnderFumigationArr['total'] = $total;
        }               
                
                

        $guardDeploymentDelaywarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        ->where('approved_status','=', 2)
                        ->whereRaw('DATEDIFF(date_of_guard_deployment_at_warehouse,warehouse_start_date)>=7')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();

        $guardDeploymentDelaywarehouseArr = array();            
        if (count($guardDeploymentDelaywarehouseCount) > 0) {
            $total = 0;
            foreach ($guardDeploymentDelaywarehouseCount as $pData) {
                $guardDeploymentDelaywarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $guardDeploymentDelaywarehouseArr['total'] = $total;
        }               
                

        $warehouseLocationClosedCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where('location_closed','Yes')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();

        $warehouseLocationClosedArr = array();          
        if (count($warehouseLocationClosedCount) > 0) {
            $total = 0;
            foreach ($warehouseLocationClosedCount as $pData) {
                $warehouseLocationClosedArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $warehouseLocationClosedArr['total'] = $total;
        }               
                

        $warehouseLocationNotStartedCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where('location_not_started_before_audit','Yes')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();

        $warehouseLocationNotStartedArr = array();          
        if (count($warehouseLocationNotStartedCount) > 0) {
            $total = 0;
            foreach ($warehouseLocationNotStartedCount as $pData) {
                $warehouseLocationNotStartedArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $warehouseLocationNotStartedArr['total'] = $total;
        }               
                

        $warehouseCmNotAvailableCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where('cm_available_during_visit','No')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();

        $warehouseCmNotAvailableArr = array();          
        if (count($warehouseCmNotAvailableCount) > 0) {
            $total = 0;
            foreach ($warehouseCmNotAvailableCount as $pData) {
                $warehouseCmNotAvailableArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $warehouseCmNotAvailableArr['total'] = $total;
        }   

        $level0warehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        ->where('approved_status','=', 0)
                        ->where('location_closed','=','No')
                        ->where('location_not_started_before_audit','=','No')
                        ->where('cm_available_during_visit','=','Yes')
                        ->where('whole_warehouse_under_fumigation','=','No')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();

        $level0warehouseArr = array();         
        if (count($level0warehouseCount) > 0) {
            $total = 0;
            foreach ($level0warehouseCount as $pData) {
                $level0warehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $level0warehouseArr['total'] = $total;
        }   

        $level1warehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        ->where('approved_status','=', 1)
                        ->where('location_closed','=','No')
                        ->where('location_not_started_before_audit','=','No')
                        ->where('cm_available_during_visit','=','Yes')
                        ->where('whole_warehouse_under_fumigation','=','No')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();

        $level1warehouseArr = array();         
        if (count($level1warehouseCount) > 0) {
            $total = 0;
            foreach ($level1warehouseCount as $pData) {
                $level1warehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $level1warehouseArr['total'] = $total;
        }           

        //echo "<pre>";
        //print_r($warehouseCmNotAvailableArr);die;                                                             
        $dataArray = array("Warehouses planned for audit", "Warehouses audited", "WH Reply Pending from Level 1", "Warehouses not audited", "Warehouses under fumigation", "Guard deployed was delayed beyond 7 days", "Location closed","Location Not started before audit","CM/Supervisor is Not Present at the Time of Audit","Warehouses at level 0","Warehouses at level 1");

        $finalArr = array();
        for ($i = 0; $i < count($dataArray); $i++) {
            
            /* DEFAULT 0 VALUE */
               $finalArr[$i]['CM'] = 0;
               $finalArr[$i]['PWH'] = 0;
               $finalArr[$i]['PWH-CM'] = 0;
               $finalArr[$i]['total'] = 0;
            /* END DEFAULT 0 VALUE */

            $finalArr[$i]['particular'] = $dataArray[$i];

            // Warehouse Planned Array
            if (count($plannedWarehouseArr) > 0) {
                foreach ($plannedWarehouseArr as $k => $data)
                    $finalArr[0][$k] = $data;
            }

            // Warehouse Audited Array
            if (count($auditedwarehouseArr) > 0) {
                foreach ($auditedwarehouseArr as $k => $data)
                    $finalArr[1][$k] = $data;
            }

            // Reply Pending Level 1 Warehouse Array
            if (count($reppendl1warehouseArr) > 0) {
                foreach ($reppendl1warehouseArr as $k => $data)
                    $finalArr[2][$k] = $data;
            }

            // Not audited Warehouse Array
            if (count($notauditedwarehouseArr) > 0) {
                foreach ($notauditedwarehouseArr as $k => $data)
                    $finalArr[3][$k] = $data;
            }

            // Warehouse Under Fumigation Array
            if (count($warehouseUnderFumigationArr) > 0) {
                foreach ($warehouseUnderFumigationArr as $k => $data)
                    $finalArr[4][$k] = $data;
            }

            // Guard Deployment Deplay Array
            if (count($guardDeploymentDelaywarehouseArr) > 0) {
                foreach ($guardDeploymentDelaywarehouseArr as $k => $data)
                    $finalArr[5][$k] = $data;
            }

            // Warehouse Location Closed Array
            if (count($warehouseLocationClosedArr) > 0) {
                foreach ($warehouseLocationClosedArr as $k => $data)
                    $finalArr[6][$k] = $data;
            }

            // Warehouse Location Not Started Before Audit Array
            if (count($warehouseLocationNotStartedArr) > 0) {
                foreach ($warehouseLocationNotStartedArr as $k => $data)
                    $finalArr[7][$k] = $data;
            }

            // Cm/Supervisor Not Available During Audit Array
            if (count($warehouseCmNotAvailableArr) > 0) {
                foreach ($warehouseCmNotAvailableArr as $k => $data)
                    $finalArr[8][$k] = $data;
            }

            // Warehouse At Level 0 Array
            if (count($level0warehouseArr) > 0) {
                foreach ($level0warehouseArr as $k => $data)
                    $finalArr[9][$k] = $data;
            }

            // Warehouse At Level 1 Array
            if (count($level1warehouseArr) > 0) {
                foreach ($level1warehouseArr as $k => $data)
                    $finalArr[10][$k] = $data;
            }

        }

        //echo "<pre>";
        //print_r($finalArr);die;
        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');

        return view('admin/reports/clientwiseplannedwarehouses',compact('finalArr','from_date','to_date','checkers','checker'));             
    }

    public function auditfindings(Request $request){
        if (! Gate::allows('reports_manage')) {
            return abort(401);
        }

         /* FROM DATE FILTER */
        if($request->get('from_date')){
            $from= date("Y-m-d",strtotime($request->get('from_date')));
        }else{
            $from=date("Y-m-01"); 
        }
        /* TO DATE FILTER */
        if($request->get('to_date')){
            $to=date("Y-m-d",strtotime($request->get('to_date'))); 
        }else{
            $to = date("Y-m-t");
        }

        $role = Auth::user()->roles->pluck('name')->first();
        $userName = Auth::user()->name;

        if($request->get('name_of_checker')){
            $userName=$checker=$request->get('name_of_checker'); 
            $role = 'checker';
        }

        //Code for all the checkers for admin
        $checkers = User::whereHas("roles", function($q){ $q->where("name", "checker"); })->get();


    $c=['approved_status','=', 2];  
    $c1 =['approved_status','=', 4];    
    $noFffeAvailableWarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where(function($q) use ($c,$c1){
                            $q->where([$c])->orWhere([$c1]);
                        })
                        ->where('fire_fighting_equipment_installed','No')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();  

    $noFffeAvailableWarehouseArr = array();         
        if (count($noFffeAvailableWarehouseCount) > 0) {
            $total = 0;
            foreach ($noFffeAvailableWarehouseCount as $pData) {
                $noFffeAvailableWarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $noFffeAvailableWarehouseArr['total'] = $total;
        }

                    
    $checkerCond = '';
        if($role=='checker'){
            $checkerCond = 'and A.name_of_checker="'.$userName.'"';
        }    

    $infestationWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where stack_health_status='Infestation' group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' ".$checkerCond."");


       
    $infestationCmWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where stack_health_status='Infestation' group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' ".$checkerCond."");

    //echo "<pre>";
    //print_r($infestationWarehouseCount);
    //print_r($infestationCmWarehouseCount);                                        

    $infestationWarehouseArr = array();
        //Infestation In chamberwise_stock table                
        if (count($infestationWarehouseCount) > 0) {
            $pwhtotal = 0;
            $pwhcmtotal = 0;
            foreach ($infestationWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='PWH'){
                    $pwhtotal = $pwhtotal + $pData->insCount;
                    $infestationWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                        
                }elseif($pData->type_of_warehouse=='PWH-CM'){
                    $pwhcmtotal = $pwhcmtotal + $pData->insCount;
                    $infestationWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                    
                }
            }
            $infestationWarehouseArr['total'] = $pwhtotal+$pwhcmtotal;
        }

        //Infestation in clientwise_stock table             
        if (count($infestationCmWarehouseCount) > 0) {
            $cmtotal = 0;
            $pwhcmtotal1 = 0;
            foreach ($infestationCmWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='CM'){
                    $cmtotal = $cmtotal + $pData->insCount;
                    $infestationWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                    
                }elseif(($pData->type_of_warehouse=='PWH-CM') && (isset($infestationWarehouseArr['warehouse_code']) && $infestationWarehouseArr['warehouse_code'] == $pData->warehouse_code)){
                    $pwhcmtotal1 = $pwhcmtotal1 + $pData->insCount;
                    $infestationWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal1;
                    
                }
            }
            $infestationWarehouseArr['total'] = $infestationWarehouseArr['total']+$cmtotal+$pwhcmtotal1;
        }


      //  print_r($infestationWarehouseArr);die;
        

    $flexBannerOfAryaNotFoundWarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where('arya_flex_banner','No')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();    

    $flexBannerOfAryaNotFoundWarehouseArr = array();            
        if (count($flexBannerOfAryaNotFoundWarehouseCount) > 0) {
            $total = 0;
            foreach ($flexBannerOfAryaNotFoundWarehouseCount as $pData) {
                $flexBannerOfAryaNotFoundWarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $flexBannerOfAryaNotFoundWarehouseArr['total'] = $total;
        }


    $stackCardNotAvailableWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where borrower_name_matching='Stack Card Not Displayed' group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' ".$checkerCond."");


       
    $stackCardNotAvailableCmWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where borrower_name_matching='Stack Card Not Displayed' group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' ".$checkerCond."");

                        
    $stackCardNotAvailableWarehouseArr = array();           

        //Stack Card Not Displayed In chamberwise_stock table               
        if (count($stackCardNotAvailableWarehouseCount) > 0) {
            $pwhtotal = 0;
            $pwhcmtotal = 0;
            foreach ($stackCardNotAvailableWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='PWH'){
                    $pwhtotal = $pwhtotal + $pData->insCount;
                    $stackCardNotAvailableWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                        
                }elseif($pData->type_of_warehouse=='PWH-CM'){
                    $pwhcmtotal = $pwhcmtotal + $pData->insCount;
                    $stackCardNotAvailableWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                    
                }
            }
            $stackCardNotAvailableWarehouseArr['total'] = $pwhtotal+$pwhcmtotal;
        }

        //Stack Card Not Displayed in clientwise_stock table                
        if (count($stackCardNotAvailableCmWarehouseCount) > 0) {
            $cmtotal = 0;
            $pwhcmtotal1 = 0;
            foreach ($stackCardNotAvailableCmWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='CM'){
                    $cmtotal = $cmtotal + $pData->insCount;
                    $stackCardNotAvailableWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                    
                }elseif(($pData->type_of_warehouse=='PWH-CM') && (isset($stackCardNotAvailableWarehouseArr['warehouse_code']) && $stackCardNotAvailableWarehouseArr['warehouse_code'] == $pData->warehouse_code)){
                    $pwhcmtotal1 = $pwhcmtotal1 + $pData->insCount;
                    $stackCardNotAvailableWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal1;
                    
                }
            }
            $stackCardNotAvailableWarehouseArr['total'] = $stackCardNotAvailableWarehouseArr['total']+$cmtotal+$pwhcmtotal1;
        }
        
                            
    
    $cleanlinessRelatedWarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where('cleanliness_of_warehouse','Not Clean')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();   
                        
    $cleanlinessRelatedWarehouseArr = array();          
        if (count($cleanlinessRelatedWarehouseCount) > 0) {
            $total = 0;
            foreach ($cleanlinessRelatedWarehouseCount as $pData) {
                $cleanlinessRelatedWarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $cleanlinessRelatedWarehouseArr['total'] = $total;
        }

    $stackFallenWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where stack_status_2='Uncountable Fallen' group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' ".$checkerCond."");


       
    $stackFallenCmWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where stack_status_2='Uncountable Fallen' group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' ".$checkerCond."");
     
   
    $stackFallenWarehouseArr = array();         
        //Stack Card Not Displayed In chamberwise_stock table               
        if (count($stackFallenWarehouseCount) > 0) {
            $pwhtotal = 0;
            $pwhcmtotal = 0;
            foreach ($stackFallenWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='PWH'){
                    $pwhtotal = $pwhtotal + $pData->insCount;
                    $stackFallenWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                        
                }elseif($pData->type_of_warehouse=='PWH-CM'){
                    $pwhcmtotal = $pwhcmtotal + $pData->insCount;
                    $stackFallenWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                    
                }
            }
            $stackFallenWarehouseArr['total'] = $pwhtotal+$pwhcmtotal;
        }

        //Stack Card Not Displayed in clientwise_stock table                
        if (count($stackFallenCmWarehouseCount) > 0) {
            $cmtotal = 0;
            $pwhcmtotal1 = 0;
            foreach ($stackFallenCmWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='CM'){
                    $cmtotal = $cmtotal + $pData->insCount;
                    $stackFallenWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                    
                }elseif(($pData->type_of_warehouse=='PWH-CM') && (isset($stackFallenWarehouseArr['warehouse_code']) && $stackFallenWarehouseArr['warehouse_code'] == $pData->warehouse_code)){
                    $pwhcmtotal1 = $pwhcmtotal1 + $pData->insCount;
                    $stackFallenWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal1;
                    
                }
            }
            $stackFallenWarehouseArr['total'] = $stackFallenWarehouseArr['total']+$cmtotal+$pwhcmtotal1;
        }


    $c=['approved_status','=', 2];  
    $c1 =['approved_status','=', 4];    
    $notVisitedByOpsTeamWarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where(function($q) use ($c,$c1){
                        $q->where([$c])->orWhere([$c1]);
                        })
                        ->whereRaw('DATEDIFF(actual_date_of_audit,date_of_last_visit_of_cluster_manager)>=30')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->groupBy('type_of_warehouse')
                        ->get();
                        
    $notVisitedByOpsTeamWarehouseArr = array();         
        if (count($notVisitedByOpsTeamWarehouseCount) > 0) {
            $total = 0;
            foreach ($notVisitedByOpsTeamWarehouseCount as $pData) {
                $notVisitedByOpsTeamWarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $notVisitedByOpsTeamWarehouseArr['total'] = $total;
        }

    //DB::enableQueryLog();
    $condition = ['overall_system_stock_pwh','<','overall_entered_stock_pwh'];
    $condition1 =['overall_system_stock_cm','<','overall_entered_stock_cm'];   
    $stockNotTalliedWarehouseCount = DB::table('audit_inspections')
                    ->select('type_of_warehouse',DB::raw('1 as insCount'),'overall_system_stock_pwh','overall_system_stock_cm','overall_entered_stock_pwh','overall_entered_stock_cm')
                    ->where('created_at','>=',$from)
                    ->where('created_at','<=',$to)
                    //->where('approved_status','=', 2)
                    ->where(function($q) use ($c,$c1){
                        $q->where([$c])->orWhere([$c1]);
                    })
                    //->whereColumn('overall_system_stock_pwh','<','overall_entered_stock_pwh')
                    //->orWhereColumn('overall_system_stock_cm','<','overall_entered_stock_cm')
                    ->where(function($q) use ($condition,$condition1){
                        $q->whereColumn([$condition])->orWhereColumn([$condition1]);
                    })
                    ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                    //->groupBy('type_of_warehouse')
                    ->get();

    
    //dd(DB::getQueryLog());        
    //echo "<pre>";
    //print_r($stockNotTalliedWarehouseCount);die;                

    $stockNotTalliedWarehouseArr = array();         
            if (count($stockNotTalliedWarehouseCount) > 0) {
                $pwhtotal = 0;
                $cmtotal = 0;
                $pwhcmtotal = 0;
                foreach ($stockNotTalliedWarehouseCount as $pData) {
                    if($pData->type_of_warehouse=='PWH'){
                        $pwhtotal = $pwhtotal + $pData->insCount;
                        $stockNotTalliedWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                            
                    }elseif($pData->type_of_warehouse=='CM'){
                        $cmtotal = $cmtotal + $pData->insCount;
                        $stockNotTalliedWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                        
                    }elseif($pData->type_of_warehouse=='PWH-CM'){
                        $pwhcmtotal = $pwhcmtotal + $pData->insCount;
                        $stockNotTalliedWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                        
                    }
                }
                $stockNotTalliedWarehouseArr['total'] = $pwhtotal+$cmtotal+$pwhcmtotal;
            }                       

    

    $cond = ['overall_system_stock_pwh','>','overall_entered_stock_pwh'];
    $cond1 = ['overall_system_stock_cm','>','overall_entered_stock_cm']; 
    //$c=['approved_status','=', 2];  
    //$c1 =['approved_status','=', 4];       
    $stockShortageWarehouseCount = DB::table('audit_inspections')
                    ->select('type_of_warehouse',DB::raw('1 as insCount'),'overall_system_stock_pwh','overall_system_stock_cm','overall_entered_stock_pwh','overall_entered_stock_cm')
                    ->where('created_at','>=',$from)
                    ->where('created_at','<=',$to)
                    //->where('approved_status','=', 2)
                    ->where(function($q) use ($c,$c1){
                        $q->where([$c])->orWhere([$c1]);
                    })
                    //->whereColumn('overall_system_stock_pwh','>','overall_entered_stock_pwh')
                    //->orWhereColumn('overall_system_stock_cm','>','overall_entered_stock_cm')
                    ->where(function($q) use ($cond,$cond1){
                        $q->whereColumn([$cond])->orWhereColumn([$cond1]);
                    })
                    ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                    //->groupBy('type_of_warehouse')
                    ->get();

    $stockShortageWarehouseArr = array();           
            if (count($stockShortageWarehouseCount) > 0) {
                $pwhtotal = 0;
                $cmtotal = 0;
                $pwhcmtotal = 0;
                foreach ($stockShortageWarehouseCount as $pData) {
                    if($pData->type_of_warehouse=='PWH'){
                        $pwhtotal = $pwhtotal + $pData->insCount;
                        $stockShortageWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                            
                    }elseif($pData->type_of_warehouse=='CM'){
                        $cmtotal = $cmtotal + $pData->insCount;
                        $stockShortageWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                        
                    }elseif($pData->type_of_warehouse=='PWH-CM'){
                        $pwhcmtotal = $pwhcmtotal + $pData->insCount;
                        $stockShortageWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                        
                    }
                }
                $stockShortageWarehouseArr['total'] = $pwhtotal+$cmtotal+$pwhcmtotal;
            }                       

    
    //echo "<pre>";                     
    //print_r($stockShortageWarehouseArr);die;


    $dataArray = array("No Firefighting equipment available","Infestation observed","Flex banner of ARYA not found","Stack Cards not available","Cleanliness related","Stack Fallen","WH not Visited by Ops Team","Stock not Tallied","Stock Shortage");



    $finalArr = array();
        for ($i = 0; $i < count($dataArray); $i++) {
            
            /* DEFAULT 0 VALUE */
               $finalArr[$i]['CM'] = 0;
               $finalArr[$i]['PWH'] = 0;
               $finalArr[$i]['PWH-CM'] = 0;
               $finalArr[$i]['total'] = 0;
            /* END DEFAULT 0 VALUE */

            $finalArr[$i]['particular'] = $dataArray[$i];

            // Warehouse With No Firefighting Equipment Available Array
            if (count($noFffeAvailableWarehouseArr) > 0) {
                foreach ($noFffeAvailableWarehouseArr as $k => $data)
                    $finalArr[0][$k] = $data;
            }

            // Infestation observed Warehouse Array
            if (count($infestationWarehouseArr) > 0) {
                foreach ($infestationWarehouseArr as $k => $data)
                    $finalArr[1][$k] = $data;
            }

            // Warehouse With Flex banner of ARYA not found Array
            if (count($flexBannerOfAryaNotFoundWarehouseArr) > 0) {
                foreach ($flexBannerOfAryaNotFoundWarehouseArr as $k => $data)
                    $finalArr[2][$k] = $data;
            }

            // Stack Cards not available Warehouse Array
            if (count($stackCardNotAvailableWarehouseArr) > 0) {
                foreach ($stackCardNotAvailableWarehouseArr as $k => $data)
                    $finalArr[3][$k] = $data;
            }

            // Cleanliness related Warehouse Array
            if (count($cleanlinessRelatedWarehouseArr) > 0) {
                foreach ($cleanlinessRelatedWarehouseArr as $k => $data)
                    $finalArr[4][$k] = $data;
            }

            // Stack Fallen Warehouse Array
            if (count($stackFallenWarehouseArr) > 0) {
                foreach ($stackFallenWarehouseArr as $k => $data)
                    $finalArr[5][$k] = $data;
            }

            // WH not Visited by Ops Team Array
            if (count($notVisitedByOpsTeamWarehouseArr) > 0) {
                foreach ($notVisitedByOpsTeamWarehouseArr as $k => $data)
                    $finalArr[6][$k] = $data;
            }

            // Stock not Tallied Warehouse Array
            if (count($stockNotTalliedWarehouseArr) > 0) {
                foreach ($stockNotTalliedWarehouseArr as $k => $data)
                    $finalArr[7][$k] = $data;
            }

            // Stock Shortage Warehouse Array
            if (count($stockShortageWarehouseArr) > 0) {
                foreach ($stockShortageWarehouseArr as $k => $data)
                    $finalArr[8][$k] = $data;
            }
 
        }    

        //echo "<pre>";
        //print_r($finalArr);die;
        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        return view('admin/reports/auditfindings',compact('finalArr','from_date','to_date','checkers','checker'));


    }



    public function clientwisedetail(Request $request){
        
    $resultArr = array();

        //$typeOfWarehouse = Input::get('type_of_warehouse');
        $typeOfWarehouse    = $request->get('type');
        $field      = $request->get('field');

        if($request->get('fdate')){
            $from_date= date("Y-m-d",strtotime($request->get('fdate')));
        }else{
            $from_date=date("Y-m-01"); 
        }
        /* TO DATE FILTER */
        if($request->get('tdate')){
            $to_date=date("Y-m-d",strtotime($request->get('tdate'))); 
        }else{
            $to_date = date("Y-m-t");
        }

        $role = Auth::user()->roles->pluck('name')->first();
        $userName = Auth::user()->name;

        if($request->get('name_of_checker')){
            $userName=$checker=$request->get('name_of_checker'); 
            $role = 'checker';
        }
        
        //print_r($request->toArray());die;

        /*    Field value 
            0 = "Warehouses planned for audit" 
            1 = "Warehouses audited"
            2 = "WH Reply Pending from Level 1"
            3 = "Warehouses not audited"
            4 = "Warehouses under fumigation"
            5 = "Guard deployed was delayed beyond 7 days"
            6 = "Location closed"
            7 = "Location Not started before audit"
            8 = "CM/Supervisor is Not Present at the Time of Audit"
            9 = "Warehouse At level 0"
            10 = "Warehouse At level 1"
          */

        $checkerCond = '';
            if($role=='checker'){
                $checkerCond = 'and A.name_of_checker="'.$userName.'"';
            }    
           

        if($field=='0'){

             $resultArr = DB::table('misdata')
                            ->select('warehouse_code','name_of_warehouse')
                            ->where('type_of_warehouse' ,$typeOfWarehouse)
                            ->where('created_at','>=',$from_date)
                            ->where('created_at','<=',$to_date)
                            ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            }) 
                            ->get();
                        
        }else if($field=='1'){
            $c=['approved_status','=', 2];  
            $c1 =['approved_status','=', 4];
            $resultArr = DB::table('audit_inspections')
                        ->select('type_of_warehouse','name_of_warehouse','warehouse_code')
                        ->where('created_at','>=',$from_date)
                        ->where('created_at','<=',$to_date)
                        ->where('type_of_warehouse' ,$typeOfWarehouse)
                        //->where('approved_status','=', 2)
                         //->orWhere('approved_status','=', 4)
                        ->where(function($q) use ($c,$c1){
                            $q->where([$c])->orWhere([$c1]);
                        })
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            }) 
                        ->get();

        }else if($field=='2'){

            
            $resultArr = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,A.name_of_warehouse,audit_inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select audit_inspection_id, count(audit_inspection_id) as count from audit_escalations where status in('0','3') group by audit_inspection_id) r on r.audit_inspection_id=A.id  where A.created_at between '".$from_date."' AND '".$to_date."' and  count != 'NULL' and A.type_of_warehouse='".$typeOfWarehouse."' ".$checkerCond."");

        }else if($field=='3'){
            
            // $resultArr = DB::table('misdata')
            //             ->leftJoin('audit_inspections','misdata.warehouse_code','=','audit_inspections.warehouse_code')
            //             ->select('misdata.type_of_warehouse','misdata.name_of_warehouse','misdata.warehouse_code')
            //             ->where('misdata.created_at','>=',$from_date)
            //             ->where('misdata.created_at','<=',$to_date)
            //             //->where('audit_inspections.approved_status','=', 2)
            //             ->where('misdata.type_of_warehouse' ,$typeOfWarehouse)
            //             ->whereNull('audit_inspections.warehouse_code')
            //             ->get();


            $resultArr = DB::table('misdata')
                        ->select(
                            'misdata.type_of_warehouse',
                            'misdata.name_of_warehouse',
                            'misdata.warehouse_code'
                        )
                        ->where('misdata.created_at','>=',$from_date)
                        ->where('misdata.created_at','<=',$to_date)
                        ->whereNotExists( function ($query) use ($to_date, $from_date) {
                            $query->select(DB::raw(1))
                            ->from('audit_inspections')
                            ->whereRaw('audit_inspections.warehouse_code=misdata.warehouse_code')
                            //->whereBetween('audit_inspections.created_at', [$from, $to]);
                            ->where('audit_inspections.created_at','>=',$from_date)
                            ->where('audit_inspections.created_at','<=',$to_date);
                        })
                        ->where('misdata.type_of_warehouse' ,$typeOfWarehouse)
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('misdata.name_of_checker',$userName);
                                }
                                
                            })
                        ->get();            

        }else if($field=='5'){
            
            $resultArr = DB::table('audit_inspections')
                        ->select('type_of_warehouse','name_of_warehouse','warehouse_code')
                        ->where('created_at','>=',$from_date)
                        ->where('created_at','<=',$to_date)
                        ->where('approved_status','=', 2)
                        ->where('type_of_warehouse',$typeOfWarehouse)
                        ->whereRaw('DATEDIFF(date_of_guard_deployment_at_warehouse,warehouse_start_date)>=7')
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->get();

        }else if($field=='4' || $field=='6' || $field=='7' || $field=='8'){

            if($field=="4"){
                 $condition=["whole_warehouse_under_fumigation"=>"Yes"];
            }else if($field=="6"){
                $condition=["location_closed"=>"Yes"];
            }else if($field=="7"){
                $condition=["location_not_started_before_audit"=>"Yes"];
            }else if($field=="8"){
                $condition=["cm_available_during_visit"=>"No"];
            }   
            
             $resultArr = DB::table('audit_inspections')
                        ->select('type_of_warehouse','warehouse_code','name_of_warehouse')
                        ->where('created_at','>=',$from_date)
                        ->where('created_at','<=',$to_date)
                        //->where('approved_status','=', 2)
                        ->where($condition)
                        ->where('type_of_warehouse',$typeOfWarehouse)
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->get();

        }else if($field=='9'){

        
            $resultArr = DB::table('audit_inspections')
                        ->select('type_of_warehouse','name_of_warehouse','warehouse_code')
                        ->where('created_at','>=',$from_date)
                        ->where('created_at','<=',$to_date)
                        ->where('location_closed','=','No')
                        ->where('location_not_started_before_audit','=','No')
                        ->where('cm_available_during_visit','=','Yes')
                        ->where('whole_warehouse_under_fumigation','=','No')
                        ->where('type_of_warehouse' ,$typeOfWarehouse)
                        ->where('approved_status','=', 0)
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->get();

        }else if($field=='10'){

        
            $resultArr = DB::table('audit_inspections')
                        ->select('type_of_warehouse','name_of_warehouse','warehouse_code')
                        ->where('created_at','>=',$from_date)
                        ->where('created_at','<=',$to_date)
                        ->where('location_closed','=','No')
                        ->where('location_not_started_before_audit','=','No')
                        ->where('cm_available_during_visit','=','Yes')
                        ->where('whole_warehouse_under_fumigation','=','No')
                        ->where('type_of_warehouse' ,$typeOfWarehouse)
                        ->where('approved_status','=', 1)
                        ->where(function ($query) use ($role,$userName){
                                if($role == 'checker')
                                {
                                  $query->where('name_of_checker',$userName);
                                }
                                
                            })
                        ->get();

        }

       return view('admin/reports/clientwisedetail',compact('resultArr'));
}


public function auditfindingsdetail(Request $request){
        
    $resultArr = array();

        $typeOfWarehouse    = $request->get('type');
        $field      = $request->get('field');

        if($request->get('fdate')){
            $from_date= date("Y-m-d",strtotime($request->get('fdate')));
        }else{
            $from_date=date("Y-m-01"); 
        }
        /* TO DATE FILTER */
        if($request->get('tdate')){
            $to_date=date("Y-m-d",strtotime($request->get('tdate'))); 
        }else{
            $to_date = date("Y-m-t");
        }

        $role = Auth::user()->roles->pluck('name')->first();
        $userName = Auth::user()->name;

        if($request->get('name_of_checker')){
            $userName=$checker=$request->get('name_of_checker'); 
            $role = 'checker';
        }
        
        //print_r($request->toArray());die;
        
         /*    Field value 
            0 = "No Firefighting equipment available"
            1 = "Infestation observed"
            2 = "Flex banner of ARYA not found"
            3 = "Stack Cards not available"
            4 = "Cleanliness related"
            5 = "Stack Fallen"
            6 = "WH not Visited by Ops Team"
            7 = "Stock not Tallied"
            8 = "Stock Shortage"
          */

          $checkerCond = '';
            if($role=='checker'){
                $checkerCond = 'and A.name_of_checker="'.$userName.'"';
            }   

        if($field=='0'){

            $c=['approved_status','=', 2];  
            $c1 =['approved_status','=', 4];
             $resultArr = DB::table('audit_inspections')
                                ->select('type_of_warehouse','warehouse_code','name_of_warehouse')
                                ->where('created_at','>=',$from_date)
                                ->where('created_at','<=',$to_date)
                                //->where('approved_status','=', 2)
                                ->where(function($q) use ($c,$c1){
                                    $q->where([$c])->orWhere([$c1]);
                                })
                                ->where('fire_fighting_equipment_installed','No')
                                ->where('type_of_warehouse',$typeOfWarehouse)
                                ->where(function ($query) use ($role,$userName){
                                    if($role == 'checker')
                                    {
                                      $query->where('name_of_checker',$userName);
                                    }
                                    
                                })
                                ->get();  
                        
        }else if($field=='1' || $field =='3' || $field=='5'){

            switch($field){
            case '1': $condition = "stack_health_status='Infestation'";break;
            case '3': $condition = "borrower_name_matching='Stack Card Not Displayed'";break;
            case '5': $condition = "stack_status_2='Uncountable Fallen'";break;
            }
            

            if($typeOfWarehouse=='CM'){

                $resultArr = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,A.name_of_warehouse,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where ".$condition." group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from_date."' AND '".$to_date."' and  count != 'NULL' and A.type_of_warehouse='".$typeOfWarehouse."' ".$checkerCond."");

            }elseif ($typeOfWarehouse=='PWH') {

                $resultArr = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,A.name_of_warehouse,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where ".$condition." group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from_date."' AND '".$to_date."' and  count != 'NULL' and A.type_of_warehouse='".$typeOfWarehouse."' ".$checkerCond."");

            }elseif ($typeOfWarehouse=='PWH-CM') {

               $resultArr1 = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,A.name_of_warehouse,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where ".$condition." group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from_date."' AND '".$to_date."' and  count != 'NULL' and A.type_of_warehouse='".$typeOfWarehouse."' ".$checkerCond."");
       
                $resultArr2 = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,A.name_of_warehouse,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where ".$condition." group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from_date."' AND '".$to_date."' and  count != 'NULL' and A.type_of_warehouse='".$typeOfWarehouse."' ".$checkerCond."");

                
                $resultArrMerged = array_map(function ($ra1, $ra2) {
                    return array_merge((array)$ra1, (array)$ra2);
                }, $resultArr1, $resultArr2);

                // $resultArr = array_replace_recursive(
                //   array_combine(array_column($resultArr1, "id"), $resultArr1),
                //   array_combine(array_column($resultArr2, "id"), $resultArr2)
                // );

                 //echo "<pre>";
                // print_r($resultArr1);
                // print_r($resultArr2);
                // print_r($resultArrMerged);die;
                $resultArr = array();
                foreach ($resultArrMerged as $ram) {
                    $resultArr[] = (object)$ram;
                }

            }            

                        

        }else if($field=='2' || $field == '4'){

            switch($field){
            case '2': $condition = ['arya_flex_banner'=>'No'];break;
            case '4': $condition = ['cleanliness_of_warehouse'=>'Not Clean'];break;
            }
            
            $resultArr = DB::table('audit_inspections')
                        ->select('type_of_warehouse','warehouse_code','name_of_warehouse')
                        ->where('created_at','>=',$from_date)
                        ->where('created_at','<=',$to_date)
                        //->where('approved_status','=', 2)
                        ->where('type_of_warehouse',$typeOfWarehouse)
                        ->where($condition)
                        ->where(function ($query) use ($role,$userName){
                                    if($role == 'checker')
                                    {
                                      $query->where('name_of_checker',$userName);
                                    }
                                    
                                })
                        ->get();    

        }elseif($field=='6'){
            $c=['approved_status','=', 2];  
            $c1 =['approved_status','=', 4];    
            $resultArr = DB::table('audit_inspections')
                        ->select('type_of_warehouse','warehouse_code','name_of_warehouse')
                        ->where('created_at','>=',$from_date)
                        ->where('created_at','<=',$to_date)
                        //->where('approved_status','=', 2)
                        ->where(function($q) use ($c,$c1){
                        $q->where([$c])->orWhere([$c1]);
                        })
                        ->where('type_of_warehouse',$typeOfWarehouse)
                        ->whereRaw('DATEDIFF(actual_date_of_audit,date_of_last_visit_of_cluster_manager)>=30')
                        ->where(function ($query) use ($role,$userName){
                                    if($role == 'checker')
                                    {
                                      $query->where('name_of_checker',$userName);
                                    }
                                    
                                })
                        ->get();

        }elseif($field=='7' || $field=='8'){

            $c=['approved_status','=', 2];  
            $c1 =['approved_status','=', 4];
            switch($field){
            case '7': $condition = ['overall_system_stock_pwh','<','overall_entered_stock_pwh'];
                      $condition1 = ['overall_system_stock_cm','<','overall_entered_stock_cm'];  
                    break;
            case '8': $condition = ['overall_system_stock_pwh','>','overall_entered_stock_pwh'];
                      $condition1 = ['overall_system_stock_cm','>','overall_entered_stock_cm'];
                    break;
            }

            //DB::enableQueryLog();
            $resultArr = DB::table('audit_inspections')
                    ->select('type_of_warehouse','warehouse_code','name_of_warehouse',DB::raw('1 as insCount'),'overall_system_stock_pwh','overall_system_stock_cm','overall_entered_stock_pwh','overall_entered_stock_cm')
                    ->where('created_at','>=',$from_date)
                    ->where('created_at','<=',$to_date)
                    //->where('approved_status','=', 2)
                    ->where(function($q) use ($c,$c1){
                        $q->where([$c])->orWhere([$c1]);
                    })
                    ->where('type_of_warehouse',$typeOfWarehouse)
                    ->where(function($q) use ($condition,$condition1){
                        $q->whereColumn([$condition])->orWhereColumn([$condition1]);
                    })
                    ->where(function ($query) use ($role,$userName){
                                    if($role == 'checker')
                                    {
                                      $query->where('name_of_checker',$userName);
                                    }
                                    
                                })
                    ->get();
            //dd(DB::getQueryLog());        
            //echo "<pre>";
            //print_r($resultArr);die;        
        }


       return view('admin/reports/auditfindingsdetail',compact('resultArr'));
}
    


public function auditfindingsdishonoured(Request $request){
        if (! Gate::allows('reports_manage')) {
            return abort(401);
        }

         /* FROM DATE FILTER */
        if($request->get('from_date')){
            $from= date("Y-m-d",strtotime($request->get('from_date')));
        }else{
            $from=date("Y-m-01"); 
        }
        /* TO DATE FILTER */
        if($request->get('to_date')){
            $to=date("Y-m-d",strtotime($request->get('to_date'))); 
        }else{
            $to = date("Y-m-t");
        }

    //DB::enableQueryLog();    
    $noFffeAvailableWarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from)
                        ->where('audit_inspections.created_at','<=',$to)
                        ->where('audit_inspections.approved_status','=', 2)
                        ->where('fire_fighting_equipment_installed','No')
                        ->where('audit_escalations.dishonour_status',1)
                        ->where('audit_escalations.field_type','fire_fighting_equipment_installed')
                        ->groupBy('audit_inspections.type_of_warehouse')
                        ->get();  
    //dd(DB::getQueryLog());                    
    $noFffeAvailableWarehouseArr = array();         
        if (count($noFffeAvailableWarehouseCount) > 0) {
            $total = 0;
            foreach ($noFffeAvailableWarehouseCount as $pData) {
                $noFffeAvailableWarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $noFffeAvailableWarehouseArr['total'] = $total;
        }

                    
    

    $infestationWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where stack_health_status='Infestation' group by inspection_id) r on r.inspection_id=A.id left join audit_escalations e on e.audit_inspection_id=A.id where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' AND e.dishonour_status=1 AND e.field_type='stack_health_status'");


    $infestationCmWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where stack_health_status='Infestation' group by inspection_id) r on r.inspection_id=A.id left join audit_escalations e on e.audit_inspection_id=A.id  where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' AND e.dishonour_status=1 AND e.field_type='stack_health_status'");

    //echo "<pre>";
    //print_r($infestationWarehouseCount);
    //print_r($infestationCmWarehouseCount);                                        

    $infestationWarehouseArr = array();
        //Infestation In chamberwise_stock table                
        if (count($infestationWarehouseCount) > 0) {
            $pwhtotal = 0;
            $pwhcmtotal = 0;
            foreach ($infestationWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='PWH'){
                    $pwhtotal = $pwhtotal + $pData->insCount;
                    $infestationWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                        
                }elseif($pData->type_of_warehouse=='PWH-CM'){
                    $pwhcmtotal = $pwhcmtotal + $pData->insCount;
                    $infestationWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                    
                }
            }
            $infestationWarehouseArr['total'] = $pwhtotal+$pwhcmtotal;
        }

        //Infestation in clientwise_stock table             
        if (count($infestationCmWarehouseCount) > 0) {
            $cmtotal = 0;
            $pwhcmtotal1 = 0;
            foreach ($infestationCmWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='CM'){
                    $cmtotal = $cmtotal + $pData->insCount;
                    $infestationWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                    
                }elseif(($pData->type_of_warehouse=='PWH-CM') && (isset($infestationWarehouseArr['warehouse_code']) && $infestationWarehouseArr['warehouse_code'] == $pData->warehouse_code)){
                    $pwhcmtotal1 = $pwhcmtotal1 + $pData->insCount;
                    $infestationWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal1;
                    
                }
            }
            $infestationWarehouseArr['total'] = $infestationWarehouseArr['total']+$cmtotal+$pwhcmtotal1;
        }


      //  print_r($infestationWarehouseArr);die;
        

    $flexBannerOfAryaNotFoundWarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from)
                        ->where('audit_inspections.created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where('audit_inspections.arya_flex_banner','No')
                        ->where('audit_escalations.dishonour_status',1)
                        ->where('audit_escalations.field_type','arya_flex_banner')
                        ->groupBy('audit_inspections.type_of_warehouse')
                        ->get();    

    $flexBannerOfAryaNotFoundWarehouseArr = array();            
        if (count($flexBannerOfAryaNotFoundWarehouseCount) > 0) {
            $total = 0;
            foreach ($flexBannerOfAryaNotFoundWarehouseCount as $pData) {
                $flexBannerOfAryaNotFoundWarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $flexBannerOfAryaNotFoundWarehouseArr['total'] = $total;
        }


    $stackCardNotAvailableWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where borrower_name_matching='Stack Card Not Displayed' group by inspection_id) r on r.inspection_id=A.id left join audit_escalations e on e.audit_inspection_id=A.id where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' AND e.dishonour_status=1 AND e.field_type='borrower_name_matching'");

       
    $stackCardNotAvailableCmWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where borrower_name_matching='Stack Card Not Displayed' group by inspection_id) r on r.inspection_id=A.id left join audit_escalations e on e.audit_inspection_id=A.id where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' AND e.dishonour_status=1 AND e.field_type='borrower_name_matching'");

                        
    $stackCardNotAvailableWarehouseArr = array();           

        //Stack Card Not Displayed In chamberwise_stock table               
        if (count($stackCardNotAvailableWarehouseCount) > 0) {
            $pwhtotal = 0;
            $pwhcmtotal = 0;
            foreach ($stackCardNotAvailableWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='PWH'){
                    $pwhtotal = $pwhtotal + $pData->insCount;
                    $stackCardNotAvailableWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                        
                }elseif($pData->type_of_warehouse=='PWH-CM'){
                    $pwhcmtotal = $pwhcmtotal + $pData->insCount;
                    $stackCardNotAvailableWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                    
                }
            }
            $stackCardNotAvailableWarehouseArr['total'] = $pwhtotal+$pwhcmtotal;
        }

        //Stack Card Not Displayed in clientwise_stock table                
        if (count($stackCardNotAvailableCmWarehouseCount) > 0) {
            $cmtotal = 0;
            $pwhcmtotal1 = 0;
            foreach ($stackCardNotAvailableCmWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='CM'){
                    $cmtotal = $cmtotal + $pData->insCount;
                    $stackCardNotAvailableWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                    
                }elseif(($pData->type_of_warehouse=='PWH-CM') && (isset($stackCardNotAvailableWarehouseArr['warehouse_code']) && $stackCardNotAvailableWarehouseArr['warehouse_code'] == $pData->warehouse_code)){
                    $pwhcmtotal1 = $pwhcmtotal1 + $pData->insCount;
                    $stackCardNotAvailableWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal1;
                    
                }
            }
            $stackCardNotAvailableWarehouseArr['total'] = $stackCardNotAvailableWarehouseArr['total']+$cmtotal+$pwhcmtotal1;
        }
        
                            
    
    $cleanlinessRelatedWarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from)
                        ->where('audit_inspections.created_at','<=',$to)
                        //->where('approved_status','=', 2)
                        ->where('audit_inspections.cleanliness_of_warehouse','Not Clean')
                        ->where('audit_escalations.dishonour_status',1)
                        ->where('audit_escalations.field_type','cleanliness_of_warehouse')
                        ->groupBy('audit_inspections.type_of_warehouse')
                        ->get();   
                        
    $cleanlinessRelatedWarehouseArr = array();          
        if (count($cleanlinessRelatedWarehouseCount) > 0) {
            $total = 0;
            foreach ($cleanlinessRelatedWarehouseCount as $pData) {
                $cleanlinessRelatedWarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $cleanlinessRelatedWarehouseArr['total'] = $total;
        }

    $stackFallenWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where stack_status_2='Uncountable Fallen' group by inspection_id) r on r.inspection_id=A.id left join audit_escalations e on e.audit_inspection_id=A.id where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' AND e.dishonour_status=1 AND e.field_type='stack_status_2'");


       
    $stackFallenCmWarehouseCount = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where stack_status_2='Uncountable Fallen' group by inspection_id) r on r.inspection_id=A.id left join audit_escalations e on e.audit_inspection_id=A.id where A.created_at between '".$from."' AND '".$to."' and  count != 'NULL' AND e.dishonour_status=1 AND e.field_type='stack_status_2'");
     
   
    $stackFallenWarehouseArr = array();         
        //Stack Card Not Displayed In chamberwise_stock table               
        if (count($stackFallenWarehouseCount) > 0) {
            $pwhtotal = 0;
            $pwhcmtotal = 0;
            foreach ($stackFallenWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='PWH'){
                    $pwhtotal = $pwhtotal + $pData->insCount;
                    $stackFallenWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                        
                }elseif($pData->type_of_warehouse=='PWH-CM'){
                    $pwhcmtotal = $pwhcmtotal + $pData->insCount;
                    $stackFallenWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                    
                }
            }
            $stackFallenWarehouseArr['total'] = $pwhtotal+$pwhcmtotal;
        }

        //Stack Card Not Displayed in clientwise_stock table                
        if (count($stackFallenCmWarehouseCount) > 0) {
            $cmtotal = 0;
            $pwhcmtotal1 = 0;
            foreach ($stackFallenCmWarehouseCount as $pData) {
                if($pData->type_of_warehouse=='CM'){
                    $cmtotal = $cmtotal + $pData->insCount;
                    $stackFallenWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                    
                }elseif(($pData->type_of_warehouse=='PWH-CM') && (isset($stackFallenWarehouseArr['warehouse_code']) && $stackFallenWarehouseArr['warehouse_code'] == $pData->warehouse_code)){
                    $pwhcmtotal1 = $pwhcmtotal1 + $pData->insCount;
                    $stackFallenWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal1;
                    
                }
            }
            $stackFallenWarehouseArr['total'] = $stackFallenWarehouseArr['total']+$cmtotal+$pwhcmtotal1;
        }


    $c=['audit_inspections.approved_status','=', 2];  
    $c1 =['audit_inspections.approved_status','=', 4];    
    
    $notVisitedByOpsTeamWarehouseCount = DB::table('audit_inspections')
                        ->select('type_of_warehouse',DB::raw('count(*) as count'))
                        ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from)
                        ->where('audit_inspections.created_at','<=',$to)
                        //->where('audit_inspections.approved_status','=', 2)
                         ->where(function($q) use ($c,$c1){
                        $q->where([$c])->orWhere([$c1]);
                        })
                        ->whereRaw('DATEDIFF(audit_inspections.actual_date_of_audit,audit_inspections.date_of_last_visit_of_cluster_manager)>=30')
                        ->where('audit_escalations.dishonour_status',1)
                        ->where('audit_escalations.field_type','date_of_last_visit_of_cluster_manager')
                        ->groupBy('audit_inspections.type_of_warehouse')
                        ->get();
                        
    $notVisitedByOpsTeamWarehouseArr = array();         
        if (count($notVisitedByOpsTeamWarehouseCount) > 0) {
            $total = 0;
            foreach ($notVisitedByOpsTeamWarehouseCount as $pData) {
                $notVisitedByOpsTeamWarehouseArr[$pData->type_of_warehouse] = $pData->count;
                $total = $total + $pData->count;
            }
            $notVisitedByOpsTeamWarehouseArr['total'] = $total;
        }

    //DB::enableQueryLog();
    // $condition = ['audit_inspections.overall_system_stock_pwh','<','audit_inspections.overall_entered_stock_pwh'];
    // $condition1 =['audit_inspections.overall_system_stock_cm','<','audit_inspections.overall_entered_stock_cm'];   
    // $stockNotTalliedWarehouseCount = DB::table('audit_inspections')
    //                 ->select('type_of_warehouse',DB::raw('1 as insCount'),'overall_system_stock_pwh','overall_system_stock_cm','overall_entered_stock_pwh','overall_entered_stock_cm')
    //                 ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
    //                 ->where('audit_inspections.created_at','>=',$from)
    //                 ->where('audit_inspections.created_at','<=',$to)
    //                 ->where('audit_inspections.approved_status','=', 2)
    //                 //->whereColumn('overall_system_stock_pwh','<','overall_entered_stock_pwh')
    //                 //->orWhereColumn('overall_system_stock_cm','<','overall_entered_stock_cm')
    //                 ->where(function($q) use ($condition,$condition1){
    //                     $q->whereColumn([$condition])->orWhereColumn([$condition1]);
    //                 })
    //                 //->where('audit_escalations.dishonour_status',1)
    //                 //->groupBy('type_of_warehouse')
    //                 ->get();

    
    // //dd(DB::getQueryLog());        
    // //echo "<pre>";
    // //print_r($stockNotTalliedWarehouseCount);die;                

    // $stockNotTalliedWarehouseArr = array();         
    //         if (count($stockNotTalliedWarehouseCount) > 0) {
    //             $pwhtotal = 0;
    //             $cmtotal = 0;
    //             $pwhcmtotal = 0;
    //             foreach ($stockNotTalliedWarehouseCount as $pData) {
    //                 if($pData->type_of_warehouse=='PWH'){
    //                     $pwhtotal = $pwhtotal + $pData->insCount;
    //                     $stockNotTalliedWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                            
    //                 }elseif($pData->type_of_warehouse=='CM'){
    //                     $cmtotal = $cmtotal + $pData->insCount;
    //                     $stockNotTalliedWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                        
    //                 }elseif($pData->type_of_warehouse=='PWH-CM'){
    //                     $pwhcmtotal = $pwhcmtotal + $pData->insCount;
    //                     $stockNotTalliedWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                        
    //                 }
    //             }
    //             $stockNotTalliedWarehouseArr['total'] = $pwhtotal+$cmtotal+$pwhcmtotal;
    //         }                       

    

    // $cond = ['audit_inspections.overall_system_stock_pwh','>','audit_inspections.overall_entered_stock_pwh'];
    // $cond1 = ['audit_inspections.overall_system_stock_cm','>','audit_inspections.overall_entered_stock_cm'];        
    // $stockShortageWarehouseCount = DB::table('audit_inspections')
    //                 ->select('type_of_warehouse',DB::raw('1 as insCount'),'overall_system_stock_pwh','overall_system_stock_cm','overall_entered_stock_pwh','overall_entered_stock_cm')
    //                 ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
    //                 ->where('audit_inspections.created_at','>=',$from)
    //                 ->where('audit_inspections.created_at','<=',$to)
    //                 ->where('audit_inspections.approved_status','=', 2)
    //                 //->whereColumn('overall_system_stock_pwh','>','overall_entered_stock_pwh')
    //                 //->orWhereColumn('overall_system_stock_cm','>','overall_entered_stock_cm')
    //                 ->where(function($q) use ($cond,$cond1){
    //                     $q->whereColumn([$cond])->orWhereColumn([$cond1]);
    //                 })
    //                 //->where('audit_escalations.dishonour_status',1)
    //                 //->groupBy('type_of_warehouse')
    //                 ->get();

    // $stockShortageWarehouseArr = array();           
    //         if (count($stockShortageWarehouseCount) > 0) {
    //             $pwhtotal = 0;
    //             $cmtotal = 0;
    //             $pwhcmtotal = 0;
    //             foreach ($stockShortageWarehouseCount as $pData) {
    //                 if($pData->type_of_warehouse=='PWH'){
    //                     $pwhtotal = $pwhtotal + $pData->insCount;
    //                     $stockShortageWarehouseArr[$pData->type_of_warehouse] = $pwhtotal;
                            
    //                 }elseif($pData->type_of_warehouse=='CM'){
    //                     $cmtotal = $cmtotal + $pData->insCount;
    //                     $stockShortageWarehouseArr[$pData->type_of_warehouse] = $cmtotal;
                        
    //                 }elseif($pData->type_of_warehouse=='PWH-CM'){
    //                     $pwhcmtotal = $pwhcmtotal + $pData->insCount;
    //                     $stockShortageWarehouseArr[$pData->type_of_warehouse] = $pwhcmtotal;
                        
    //                 }
    //             }
    //             $stockShortageWarehouseArr['total'] = $pwhtotal+$cmtotal+$pwhcmtotal;
    //         }                       

    
    //echo "<pre>";                     
    //print_r($stockShortageWarehouseArr);die;


    $dataArray = array("No Firefighting equipment available","Infestation observed","Flex banner of ARYA not found","Stack Cards not available","Cleanliness related","Stack Fallen","WH not Visited by Ops Team"/*,"Stock not Tallied","Stock Shortage"*/);



    $finalArr = array();
        for ($i = 0; $i < count($dataArray); $i++) {
            
            /* DEFAULT 0 VALUE */
               $finalArr[$i]['CM'] = 0;
               $finalArr[$i]['PWH'] = 0;
               $finalArr[$i]['PWH-CM'] = 0;
               $finalArr[$i]['total'] = 0;
            /* END DEFAULT 0 VALUE */

            $finalArr[$i]['particular'] = $dataArray[$i];

            // Warehouse With No Firefighting Equipment Available Array
            if (count($noFffeAvailableWarehouseArr) > 0) {
                foreach ($noFffeAvailableWarehouseArr as $k => $data)
                    $finalArr[0][$k] = $data;
            }

            // Infestation observed Warehouse Array
            if (count($infestationWarehouseArr) > 0) {
                foreach ($infestationWarehouseArr as $k => $data)
                    $finalArr[1][$k] = $data;
            }

            // Warehouse With Flex banner of ARYA not found Array
            if (count($flexBannerOfAryaNotFoundWarehouseArr) > 0) {
                foreach ($flexBannerOfAryaNotFoundWarehouseArr as $k => $data)
                    $finalArr[2][$k] = $data;
            }

            // Stack Cards not available Warehouse Array
            if (count($stackCardNotAvailableWarehouseArr) > 0) {
                foreach ($stackCardNotAvailableWarehouseArr as $k => $data)
                    $finalArr[3][$k] = $data;
            }

            // Cleanliness related Warehouse Array
            if (count($cleanlinessRelatedWarehouseArr) > 0) {
                foreach ($cleanlinessRelatedWarehouseArr as $k => $data)
                    $finalArr[4][$k] = $data;
            }

            // Stack Fallen Warehouse Array
            if (count($stackFallenWarehouseArr) > 0) {
                foreach ($stackFallenWarehouseArr as $k => $data)
                    $finalArr[5][$k] = $data;
            }

            // WH not Visited by Ops Team Array
            if (count($notVisitedByOpsTeamWarehouseArr) > 0) {
                foreach ($notVisitedByOpsTeamWarehouseArr as $k => $data)
                    $finalArr[6][$k] = $data;
            }

            // // Stock not Tallied Warehouse Array
            // if (count($stockNotTalliedWarehouseArr) > 0) {
            //     foreach ($stockNotTalliedWarehouseArr as $k => $data)
            //         $finalArr[7][$k] = $data;
            // }

            // // Stock Shortage Warehouse Array
            // if (count($stockShortageWarehouseArr) > 0) {
            //     foreach ($stockShortageWarehouseArr as $k => $data)
            //         $finalArr[8][$k] = $data;
            // }
 
        }    

        //echo "<pre>";
        //print_r($finalArr);die;
        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        return view('admin/reports/auditfindingsdishonoured',compact('finalArr','from_date','to_date'));


    }



    public function auditfindingsdetaildishonoured(Request $request){
        
    $resultArr = array();

        $typeOfWarehouse    = $request->get('type');
        $field      = $request->get('field');

        if($request->get('fdate')){
            $from_date= date("Y-m-d",strtotime($request->get('fdate')));
        }else{
            $from_date=date("Y-m-01"); 
        }
        /* TO DATE FILTER */
        if($request->get('tdate')){
            $to_date=date("Y-m-d",strtotime($request->get('tdate'))); 
        }else{
            $to_date = date("Y-m-t");
        }
        
        //print_r($request->toArray());die;
        
         /*    Field value 
            0 = "No Firefighting equipment available"
            1 = "Infestation observed"
            2 = "Flex banner of ARYA not found"
            3 = "Stack Cards not available"
            4 = "Cleanliness related"
            5 = "Stack Fallen"
            6 = "WH not Visited by Ops Team"
            7 = "Stock not Tallied"
            8 = "Stock Shortage"
          */   

        if($field=='0'){

            //DB::enableQueryLog();    

             $resultArr = DB::table('audit_inspections')
                                ->select('type_of_warehouse','warehouse_code','name_of_warehouse')
                                ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                                ->where('audit_inspections.created_at','>=',$from_date)
                                ->where('audit_inspections.created_at','<=',$to_date)
                                ->where('audit_inspections.approved_status','=', 2)
                                ->where('audit_inspections.fire_fighting_equipment_installed','No')
                                ->where('audit_inspections.type_of_warehouse',$typeOfWarehouse)
                                ->where('audit_escalations.dishonour_status',1)
                                ->where('audit_escalations.field_type','fire_fighting_equipment_installed')
                                ->get(); 
            //dd(DB::getQueryLog());                     
                        
        }else if($field=='1' || $field =='3' || $field=='5'){

            switch($field){
            case '1': $condition = "r.stack_health_status='Infestation'";
                      $condition1= "e.field_type='stack_health_status'";
                    break;
            case '3': $condition = "borrower_name_matching='Stack Card Not Displayed'";
                      $condition1= "e.field_type='borrower_name_matching'"; 
                    break;
            case '5': $condition = "stack_status_2='Uncountable Fallen'";
                      $condition1= "e.field_type='stack_status_2'";  
                    break;
            }
            

            if($typeOfWarehouse=='CM'){

                $resultArr = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,A.name_of_warehouse,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where ".$condition." group by inspection_id) r on r.inspection_id=A.id left join audit_escalations e on e.audit_inspection_id=A.id where A.created_at between '".$from_date."' AND '".$to_date."' and  count != 'NULL' and A.type_of_warehouse='".$typeOfWarehouse."' AND e.dishonour_status=1 AND ".$condition1."");

            }elseif ($typeOfWarehouse=='PWH') {

                $resultArr = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,A.name_of_warehouse,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where ".$condition." group by inspection_id) r on r.inspection_id=A.id left join audit_escalations e on e.audit_inspection_id=A.id where A.created_at between '".$from_date."' AND '".$to_date."' and  count != 'NULL' and A.type_of_warehouse='".$typeOfWarehouse."' AND e.dishonour_status=1 AND ".$condition1."");

            }elseif ($typeOfWarehouse=='PWH-CM') {

               $resultArr1 = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,A.name_of_warehouse,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from chamberwise_stock where ".$condition." group by inspection_id) r on r.inspection_id=A.id left join audit_escalations e on e.audit_inspection_id=A.id where A.created_at between '".$from_date."' AND '".$to_date."' and  count != 'NULL' and A.type_of_warehouse='".$typeOfWarehouse."' AND e.dishonour_status=1 AND ".$condition1."");
       
                $resultArr2 = DB::select("SELECT A.id,A.type_of_warehouse,A.warehouse_code,A.name_of_warehouse,inspection_id,count,1 as insCount FROM `audit_inspections` A left join (select inspection_id, count(inspection_id) as count from clientwise_stock where ".$condition." group by inspection_id) r on r.inspection_id=A.id  where A.created_at between '".$from_date."' AND '".$to_date."' and  count != 'NULL' and A.type_of_warehouse='".$typeOfWarehouse."' AND e.dishonour_status=1 AND ".$condition1."");

                
                $resultArrMerged = array_map(function ($ra1, $ra2) {
                    return array_merge((array)$ra1, (array)$ra2);
                }, $resultArr1, $resultArr2);

                // $resultArr = array_replace_recursive(
                //   array_combine(array_column($resultArr1, "id"), $resultArr1),
                //   array_combine(array_column($resultArr2, "id"), $resultArr2)
                // );

                 //echo "<pre>";
                // print_r($resultArr1);
                // print_r($resultArr2);
                // print_r($resultArrMerged);die;
                $resultArr = array();
                foreach ($resultArrMerged as $ram) {
                    $resultArr[] = (object)$ram;
                }

            }            

                        

        }else if($field=='2' || $field == '4'){

            switch($field){
            case '2': $condition = ['audit_inspections.arya_flex_banner'=>'No'];
                      $condition1 = ['audit_escalations.field_type'=>'arya_flex_banner'];  
                    break;
            case '4': $condition = ['audit_inspections.cleanliness_of_warehouse'=>'Not Clean'];
                      $condition1 = ['audit_escalations.field_type'=>'cleanliness_of_warehouse'];
                    break;
            }
            
            $resultArr = DB::table('audit_inspections')
                        ->select('type_of_warehouse','warehouse_code','name_of_warehouse')
                        ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from_date)
                        ->where('audit_inspections.created_at','<=',$to_date)
                        //->where('audit_inspections.approved_status','=', 2)
                        ->where('audit_inspections.type_of_warehouse',$typeOfWarehouse)
                        ->where($condition)
                        ->where($condition1)
                        ->where('audit_escalations.dishonour_status',1)
                        ->get();    

        }elseif($field=='6'){

            $c=['audit_inspections.approved_status','=', 2];  
            $c1 =['audit_inspections.approved_status','=', 4];    


            $resultArr = DB::table('audit_inspections')
                        ->select('type_of_warehouse','warehouse_code','name_of_warehouse')
                        ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from_date)
                        ->where('audit_inspections.created_at','<=',$to_date)
                        //->where('audit_inspections.approved_status','=', 2)
                         ->where(function($q) use ($c,$c1){
                        $q->where([$c])->orWhere([$c1]);
                        })
                        ->where('audit_inspections.type_of_warehouse',$typeOfWarehouse)
                        ->whereRaw('DATEDIFF(audit_inspections.actual_date_of_audit,audit_inspections.date_of_last_visit_of_cluster_manager)>=30')
                        ->where('audit_escalations.dishonour_status',1)
                        ->where('audit_escalations.field_type','date_of_last_visit_of_cluster_manager')
                        ->get();

         }//elseif($field=='7' || $field=='8'){

        //     switch($field){
        //     case '7': $condition = ['audit_inspections.overall_system_stock_pwh','<','audit_inspections.overall_entered_stock_pwh'];
        //               $condition1 = ['audit_inspections.overall_system_stock_cm','<','audit_inspections.overall_entered_stock_cm'];  
        //             break;
        //     case '8': $condition = ['audit_inspections.overall_system_stock_pwh','>','audit_inspections.overall_entered_stock_pwh'];
        //               $condition1 = ['audit_inspections.overall_system_stock_cm','>','audit_inspections.overall_entered_stock_cm'];
        //             break;
        //     }

        //     //DB::enableQueryLog();
        //     $resultArr = DB::table('audit_inspections')
        //             ->select('type_of_warehouse','warehouse_code','name_of_warehouse',DB::raw('1 as insCount'),'overall_system_stock_pwh','overall_system_stock_cm','overall_entered_stock_pwh','overall_entered_stock_cm')
        //             ->leftJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
        //             ->where('audit_inspections.created_at','>=',$from_date)
        //             ->where('audit_inspections.created_at','<=',$to_date)
        //             ->where('audit_inspections.approved_status','=', 2)
        //             ->where('audit_inspections.type_of_warehouse',$typeOfWarehouse)
        //             ->where(function($q) use ($condition,$condition1){
        //                 $q->whereColumn([$condition])->orWhereColumn([$condition1]);
        //             })
        //             ->where('audit_escalations.dishonour_status',1)
        //             ->get();
        //     //dd(DB::getQueryLog());        
        //     //echo "<pre>";
        //     //print_r($resultArr);die;        
        // }


       return view('admin/reports/auditfindingsdetaildishonoured',compact('resultArr'));
}

public function warehousewisemismatchreport(Request $request){
        if (! Gate::allows('reports_manage')) {
            return abort(401);
        }

         /* FROM DATE FILTER */
        if($request->get('from_date')){
            $from= date("Y-m-d",strtotime($request->get('from_date')));
        }else{
            $from=date("Y-m-01");
            //$from=date("2020-12-01"); 
        }
        /* TO DATE FILTER */
        if($request->get('to_date')){
            $to=date("Y-m-d",strtotime($request->get('to_date'))); 
        }else{
            $to = date("Y-m-t");
            //$to = date("2020-12-31");
        }

    
    $condition = ['audit_escalations.escalation_value','<','chamberwise_stock.current_stock_bags'];    
    $stockShortageWarehouseCount = DB::table('audit_escalations')
                        ->select('audit_inspections.id','audit_inspections.warehouse_code','audit_inspections.name_of_warehouse','audit_inspections.created_at',DB::raw('count(*) as shortageCount'))
                        
                        ->join("chamberwise_stock",function($join){
                            $join->on('chamberwise_stock.inspection_id','=','audit_escalations.audit_inspection_id')
                                ->on('chamberwise_stock.stack_number','=','audit_escalations.stack_no')
                                ->on('chamberwise_stock.client_name','=','audit_escalations.client_name');
                        })
                        ->leftJoin('audit_inspections','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from)
                        ->where('audit_inspections.created_at','<=',$to)
                        ->whereColumn([$condition])
                        ->where('audit_inspections.approved_status','=', 2)
                        ->where('audit_escalations.field_type','=', 'stack_status_1')
                        //->whereIn('audit_inspections.type_of_warehouse', ['PWH','PWH-CM'])
                        ->groupBy('audit_escalations.audit_inspection_id')
                        ->get()->toArray();                      

       
    
    //DB::enableQueryLog();    
    $condition = ['audit_escalations.escalation_value','>','chamberwise_stock.current_stock_bags'];    
    $stockNotTalliedWarehouseCount = DB::table('audit_escalations')
                        ->select('audit_inspections.id','audit_inspections.warehouse_code','audit_inspections.name_of_warehouse','audit_inspections.created_at',DB::raw('count(*) as notTalliedCount'))
                        
                        ->join("chamberwise_stock",function($join){
                            $join->on('chamberwise_stock.inspection_id','=','audit_escalations.audit_inspection_id')
                                ->on('chamberwise_stock.stack_number','=','audit_escalations.stack_no')
                                ->on('chamberwise_stock.client_name','=','audit_escalations.client_name');
                        })
                        ->leftJoin('audit_inspections','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from)
                        ->where('audit_inspections.created_at','<=',$to)
                        ->whereColumn([$condition])
                        ->where('audit_inspections.approved_status','=', 2)
                        ->where('audit_escalations.field_type','=', 'stack_status_1')
                        //->whereIn('audit_inspections.type_of_warehouse', ['PWH','PWH-CM'])
                        ->groupBy('audit_escalations.audit_inspection_id')
                        ->get()->toArray();                      
    //dd(DB::getQueryLog()); 
    
    
    $stockShortageWarehouseCount = json_decode(json_encode($stockShortageWarehouseCount), true); 

    $stockNotTalliedWarehouseCount = json_decode(json_encode($stockNotTalliedWarehouseCount), true);

    $times1 = count($stockShortageWarehouseCount);
    $times2 = count($stockNotTalliedWarehouseCount);

    $finalArr = array();

    if($times1>=$times2){

        $arr2 = array_column($stockNotTalliedWarehouseCount, "id");

        foreach($stockShortageWarehouseCount as $arr){
            $key = array_search($arr['id'], $arr2);
            if($key ===false){
                $finalArr[] =  $arr;
            }else{
                $finalArr[] =  array_merge($arr,$stockNotTalliedWarehouseCount[$key]);    
            }
            
            
        }

    }else{

        $arr2 = array_column($stockShortageWarehouseCount, "id");

        foreach($stockNotTalliedWarehouseCount as $arr){
            $key = array_search($arr['id'], $arr2);
            if($key ===false){
                $finalArr[] = $arr;     
            }
                $finalArr[] =  array_merge($arr,$stockShortageWarehouseCount[$key]);
        }

    }

    //echo "<pre>";
    //print_r($stockShortageWarehouseCount);
    //print_r($stockNotTalliedWarehouseCount);
    //print_r($finalArr);die;
   
    $from_date = $request->get('from_date');
    $to_date = $request->get('to_date');
    return view('admin/reports/warehousewisemismatchreport',compact('finalArr','from_date','to_date'));


    }

    public function warehousewisemismatchreportdetail(Request $request){
        
    $resultArr = array();

        $type               = $request->get('type');
        $inspectionId       = $request->get('insId');

        if($request->get('fdate')){
            $from_date= date("Y-m-d",strtotime($request->get('fdate')));
        }else{
            $from_date=date("Y-m-01"); 
        }
        /* TO DATE FILTER */
        if($request->get('tdate')){
            $to_date=date("Y-m-d",strtotime($request->get('tdate'))); 
        }else{
            $to_date = date("Y-m-t");
        }
        
        //print_r($request->toArray());die;
        
         /* Type value 
            SC = "Stock Shortage"
            NT = "Stock Not Tallied"
          */   

        if($type=='SC'){

            //DB::enableQueryLog();    

             $condition = ['audit_escalations.escalation_value','<','chamberwise_stock.current_stock_bags'];    
            $resultArr = DB::table('audit_escalations')
                        ->select('audit_inspections.id','audit_inspections.warehouse_code','audit_inspections.name_of_warehouse','audit_inspections.created_at','audit_escalations.stack_no','audit_escalations.escalation_value','chamberwise_stock.current_stock_bags')
                        
                        ->join("chamberwise_stock",function($join){
                            $join->on('chamberwise_stock.inspection_id','=','audit_escalations.audit_inspection_id')
                                ->on('chamberwise_stock.stack_number','=','audit_escalations.stack_no')
                                ->on('chamberwise_stock.client_name','=','audit_escalations.client_name');
                        })
                        ->leftJoin('audit_inspections','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from_date)
                        ->where('audit_inspections.created_at','<=',$to_date)
                        ->whereColumn([$condition])
                        ->where('audit_inspections.approved_status','=', 2)
                        ->where('audit_escalations.field_type','=', 'stack_status_1')
                        ->where('audit_escalations.audit_inspection_id', $inspectionId)
                        ->get(); 
            //dd(DB::getQueryLog());
        //echo "<pre>";                
        //print_r($resultArr);die;                            

        }elseif($type=='NT'){

            $condition = ['audit_escalations.escalation_value','>','chamberwise_stock.current_stock_bags'];    
            $resultArr = DB::table('audit_escalations')
                        ->select('audit_inspections.id','audit_inspections.warehouse_code','audit_inspections.name_of_warehouse','audit_inspections.created_at','audit_escalations.stack_no','audit_escalations.escalation_value','chamberwise_stock.current_stock_bags')
                        
                        ->join("chamberwise_stock",function($join){
                            $join->on('chamberwise_stock.inspection_id','=','audit_escalations.audit_inspection_id')
                                ->on('chamberwise_stock.stack_number','=','audit_escalations.stack_no')
                                ->on('chamberwise_stock.client_name','=','audit_escalations.client_name');
                        })
                        ->leftJoin('audit_inspections','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                        ->where('audit_inspections.created_at','>=',$from_date)
                        ->where('audit_inspections.created_at','<=',$to_date)
                        ->whereColumn([$condition])
                        ->where('audit_inspections.approved_status','=', 2)
                        ->where('audit_escalations.field_type','=', 'stack_status_1')
                        ->where('audit_escalations.audit_inspection_id', $inspectionId)
                        ->get();

         }


       return view('admin/reports/warehousewisemismatchreportdetail',compact('resultArr'));
    }





}
