<?php

namespace App\Http\Controllers\Admin;

use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use App\Location;
use DB;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('states_manage')) {
            return abort(401);
        }

        $locations = DB::table('locations')->leftJoin('states', 'locations.state_id', '=', 'states.id')
            ->select(['locations.id','locations.location_name as location', 'states.state'])
            ->where('locations.status', '=', 1)
            ->orderBy('states.updated_at', 'desc')
            ->get();

        return view('admin.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('states_manage')) {
            return abort(401);
        }
        $states =State::all();
        return view('admin.locations.create',compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        if (! Gate::allows('states_manage')) {
            return abort(401);
        }
        $location = Location::create($request->all());
        
        return redirect()->route('admin.locations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('states_manage')) {
            return abort(401);
        }
        $states = State::all();
        $location = DB::table('locations')->leftJoin('states', 'locations.state_id', '=', 'states.id')
            ->select(['locations.id','locations.location_name as location', 'states.state','locations.state_id'])
            ->where('locations.status', '=', 1)
            ->where('locations.id', '=', $id)
            ->get()[0];
            //print_r($location->location);die;

        return view('admin.locations.edit', compact('states','location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('states_manage')) {
            return abort(401);
        }
        $location = Location::findOrFail($id);
        $location->update($request->all());
        
        return redirect()->route('admin.locations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('states_manage')) {
            return abort(401);
        }
        $location = Location::findOrFail($id);
        $location->delete();

        return redirect()->route('admin.locations.index');
    }
}
