<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use App\User;
use DB;
trait HelperTrait
{
	public function getUserDetails($ID,$column){
    	$user = User::find($ID); 
	    if($user)
	    return $user->$column;
	}

	public function getUserIdByName($name){
    	$user = User::where('name', $name)->first(); 
	    if($user)
	    return $user->id;
	}

	public function getRepliedEscalations($auditInspectionId){
    	$replyCount = DB::table('audit_escalations')
    				->select(DB::raw('count(id) as rc'))
    				->where('audit_inspection_id',$auditInspectionId)
    				->where('status','2')
    				->first(); 
	    if(count($replyCount)>0){
	    	return $replyCount->rc;
	    }else{
	    	return false;
	    }
	}

	public function getLevel1Escalations($auditInspectionId){
    	$replyCount = DB::table('audit_escalations')
    				->select(DB::raw('count(id) as rc'))
    				->where('audit_inspection_id',$auditInspectionId)
    				->where('status','4')
    				->first(); 
	    if(count($replyCount)>0){
	    	return $replyCount->rc;
	    }else{
	    	return false;
	    }
	}

	public function getNotRepliedEscalations($auditInspectionId){
    	$replyCount = DB::table('audit_escalations')
    				->select(DB::raw('count(id) as rc'))
    				->where('audit_inspection_id',$auditInspectionId)
    				->where('status','0')
    				->first(); 
	    if(count($replyCount)>0){
	    	return $replyCount->rc;
	    }else{
	    	return false;
	    }
	}

}