<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\AuditInspection;
use Auth;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\FieldMapper;
use App\User;
use Mail;
use App\Http\Controllers\Traits\HelperTrait;
use Illuminate\Support\Facades\Storage;

class AuditInspectionController extends Controller
{
    use HelperTrait;
    
	public function index(Request $request){
		if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }

        /* FROM DATE FILTER */
        if($request->get('from_date')){
            $from= date("Y-m-d",strtotime($request->get('from_date')));
        }else{
            $from=date("Y-m-01"); 
        }
        /* TO DATE FILTER */
        if($request->get('to_date')){
            $to=date("Y-m-d",strtotime($request->get('to_date'))); 
        }else{
            $to = date("Y-m-t");
        }

        $role = Auth::user()->roles->pluck('name')->first();
        $userName = Auth::user()->name;

        $level1Users = User::whereHas("roles", function($q){ $q->where("name", "level 1"); })->where("parent_id",Auth::user()->id)->get();

        $amArr = [];
        foreach ($level1Users as $user) {
            $amArr[]=$user->name;
        }

        $condition = array();
        $amCondition = array();
    
        if($role == 'maker'){
        	$condition = [
        		'status'			=>		1,
        		'auditor_name'		=>		$userName
        	];  
        }elseif($role == 'checker'){
            $condition = [
                'status'            =>      1,
                'name_of_checker'   =>      $userName,
                ['approved_status', '!=',   0],
            ];
        }elseif($role == 'cluster head'){
            $condition = [
                'status'            =>      1,
                'name_of_last_visit_cluster_manager'    =>      $userName,
                ['approved_status', '!=',   0],
            ];
        }elseif($role == 'level 1'){
        	$condition = [
        		'status'			=>		1,
        		'name_of_last_visit_area_manager'		=>		$userName,
        		['approved_status',	'!=',	0],
        	];
        }elseif($role == 'level 2'){
            $condition = [
                'status'            =>      1,
                ['approved_status', '!=',   0],
            ];

            $amcondition = [
                'name_of_last_visit_area_manager'       =>      $amArr,
            ];
        }

        if(!empty($amCondition)){
            $auditinspections = AuditInspection::where($condition)
                            ->whereIn($amCondition)
                            ->where('created_at','>=',$from)
                            ->where('created_at','<=',date('Y-m-d', strtotime($to . ' +1 day')))
                            ->orderBy('id','desc')
                            ->get();
        }else{
        //DB::enableQueryLog();
   		$auditinspections = AuditInspection::where($condition)
                            ->where(function ($query) use ($role,$userName){
                            if($role == 'checker')
                                $query->where('name_of_checker',$userName)->orWhere('name_of_checker',NULL);
                                
                            })
                            ->where('created_at','>=',$from)
                            ->where('created_at','<=',date('Y-m-d', strtotime($to . ' +1 day')))
                            ->orderBy('id','desc')
                            ->get();
        //dd(DB::getQueryLog());                    
        }                      
        
       //echo "<pre>";                         
       //print_r($auditinspections->toArray());die;
        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');                    
    	return view('auditinspection/index',compact('auditinspections','from_date','to_date'));
    }

    public function add($id){
    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
        
        $misdata = DB::table('misdata')->select('*')->find($id);
        $level1 = User::whereHas("roles", function($q){ $q->where("name", "level 1"); })->get();
        $from=date("Y-m-01");
        $to = date("Y-m-t");

        $lastInsDateObj= DB::table('audit_inspections')
                            ->select('actual_date_of_audit')
                            ->where('warehouse_code', $misdata->warehouse_code)
                            ->where('created_at','>=',$from)
                            ->where('created_at','<=',$to)
                            //->where('location_closed','=','No')
                            //->where('location_not_started_before_audit','=','No')
                            //->where('cm_available_during_visit','=','Yes')
                            //->where('whole_warehouse_under_fumigation','=','No')
                            ->orderBy('id', 'desc')
                            //->orderBy('created_at','desc')
                            ->skip(1)
                            ->first();

        if(isset($lastInsDateObj)){                    
            $date1 = strtotime($lastInsDateObj->actual_date_of_audit);
        }else{
            $date1 = strtotime(date("Y-m-d"));
        }
        $date2 = strtotime(date("Y-m-d"));
        $days = $this->diffdate($date1,$date2);
        //print_r($days);die;
        if(($days>1) || ($days==0)){
            
        if($misdata->type_of_warehouse=='PWH'){
            $warehouseIdsObj = DB::connection('mysql2')->table('warehouse')->select('id')->where(["warehouse_code"=>$misdata->warehouse_code])->get();

            $whIds=[];
            foreach ($warehouseIdsObj as $id) {
                $whIds[]=$id->id;
            }
            //print_r($whIds);
            //DB::connection('mysql2')->enableQueryLog();
            $currentStock = DB::connection('mysql2')->table('dtr_grain')->select('warehouse.warehouse_code','client.client_name','client.id as clientId','dtr_grain.stk_no','dtr_grain.gdn_no',DB::raw('round(SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_grain.cargo_gross_wt,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_grain.cargo_gross_wt,0))),3) as current_stock_qty'),DB::raw('SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_grain.no_of_bags,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_grain.no_of_bags,0))) as current_stock_bags'),DB::raw('MAX(dtr_grain.created_at) as max_created_at'))
            	->join('dtr_audit','dtr_grain.dtr_audit_id','=','dtr_audit.id')
            	->leftJoin('warehouse','dtr_grain.warehouse_id','=','warehouse.id')
            	->leftJoin('client','dtr_grain.client_id','=','client.id')
            	->where('dtr_audit.approved','2')
            	->where('warehouse.active','1')
                ->where('client.active','1')
                ->whereIn('dtr_audit.warehouse_id', $whIds)
            	->groupBy('dtr_audit.warehouse_id','dtr_grain.client_id','dtr_grain.gdn_no','dtr_grain.stk_no')
                ->orderBy('client.client_name','asc')
            	->get();

                 $new = array();
                foreach ($currentStock as $cs) {
                    if(($cs->current_stock_bags>5) && ($cs->current_stock_qty>5)){
                        $new[] = $cs;
                    }
                }

                $currentStock = $new;
               
                //dd($currentStock);      
                //dd(DB::connection('mysql2')->getQueryLog());

                if(count($currentStock)==0){
                
                $currentStockCotton = DB::connection('mysql2')->table('dtr_cotton')->select('warehouse.warehouse_code','client.client_name','client.id as clientId',DB::raw('0 as stk_no'),DB::raw('0 as gdn_no'),DB::raw('round(SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_cotton.qty,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_cotton.qty,0))),3) as current_stock_qty'),DB::raw('SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_cotton.qty,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_cotton.qty,0))) as current_stock_bags'),DB::raw('MAX(dtr_cotton.created_at) as max_created_at'))
                ->join('dtr_audit','dtr_cotton.dtr_audit_id','=','dtr_audit.id')
                ->leftJoin('warehouse','dtr_cotton.warehouse_id','=','warehouse.id')
                ->leftJoin('client','dtr_cotton.client_id','=','client.id')
                ->where('dtr_audit.approved','2')
                ->where('warehouse.active','1')
                ->where('client.active','1')
                ->whereIn('dtr_audit.warehouse_id', $whIds)
                ->groupBy('dtr_audit.warehouse_id','dtr_cotton.client_id')
                ->get();

                //echo "<pre>";
                //print_r($currentStockCotton);die;

                $new = array();
                foreach ($currentStockCotton as $cs) {
                    if(($cs->current_stock_bags>0) && ($cs->current_stock_qty>0)){
                        $new[] = $cs;
                    }
                }

                $currentStockCotton = $new;

                return view('auditinspection/add',compact('misdata','currentStockCotton','level1'));                
            }
               
        	return view('auditinspection/add',compact('misdata','currentStock','level1'));
        }else if($misdata->type_of_warehouse=='CM'){

                $warehouseIdsObj = DB::connection('mysql3')->table('tbl_warehouses')->select('id')->where(["warehouse_code"=>$misdata->warehouse_code])->get();

                $whIds=[];
                foreach ($warehouseIdsObj as $id) {
                    $whIds[]=$id->id;
                }

                //print_r($whIds);die;
                $currentStockCm = DB::connection('mysql3')->table('tbl_srs as BaseTbl')->select("S.name as state","Loc.name as location", "BaseTbl.id", "BaseTbl.storage_receipt_no",  "B.borrowerName as borrowerName_cm", "W.name as warehouse","W.warehouse_code", "C.client_name as client_name_cm","C.id as clientId_cm", "Com.commodity", "CV.commodity_variety", "BaseTbl.total_bags", "BaseTbl.net_weight", "BaseTbl.release_bag", "BaseTbl.release_qty",DB::raw("SUM(BaseTbl.closing_bag) as closing_bag"), DB::raw("SUM(BaseTbl.closing_qty) as closing_qty"), "BaseTbl.market_rate", "BaseTbl.value_of_commodity", "BaseTbl.createdDtm")
                    ->leftJoin('tbl_warehouses as W','BaseTbl.warehouse_id','=','W.id')
                    ->leftJoin('tbl_clients as C','BaseTbl.client_id','=','C.id')
                    ->leftJoin("tbl_borrowers as B","BaseTbl.borrower_id",'=','B.id')
                    ->leftJoin("tbl_states as S","BaseTbl.state_id","=","S.id")
                    ->leftJoin("tbl_states as Loc","BaseTbl.location_id","=","Loc.id")
                    ->leftJoin("tbl_commodity as Com","BaseTbl.commodity_id","=","Com.id")
                    ->leftJoin("tbl_commodity_variety as CV","BaseTbl.commodity_variety","=","CV.id")
                    ->where('BaseTbl.isDeleted',0)
                    ->where('BaseTbl.approved',2)
                    ->where('BaseTbl.closing_bag',">",0)
                    ->groupBy(['B.id','C.id'])
                    ->whereIn('BaseTbl.warehouse_id',$whIds)
                    ->orderBy("BaseTbl.id","DESC")
                    ->get();

                //dd($currentStockCm);      
                return view('auditinspection/add',compact('misdata','currentStockCm','level1'));

        }else if($misdata->type_of_warehouse=='PWH-CM'){
            //Code to get PWH stock
            $warehouseIdsObj = DB::connection('mysql2')->table('warehouse')->select('id')->where(["warehouse_code"=>$misdata->warehouse_code])->get();

            $whIds=[];
            foreach ($warehouseIdsObj as $id) {
                $whIds[]=$id->id;
            }
            //print_r($whIds);die;
            //DB::connection('mysql2')->enableQueryLog();
            $currentStock = DB::connection('mysql2')->table('dtr_grain')->select('warehouse.warehouse_code','client.client_name','client.id as clientId','dtr_grain.stk_no','dtr_grain.gdn_no',DB::raw('round(SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_grain.cargo_gross_wt,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_grain.cargo_gross_wt,0))),3) as current_stock_qty'),DB::raw('SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_grain.no_of_bags,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_grain.no_of_bags,0))) as current_stock_bags'),DB::raw('MAX(dtr_grain.created_at) as max_created_at'))
                ->join('dtr_audit','dtr_grain.dtr_audit_id','=','dtr_audit.id')
                ->leftJoin('warehouse','dtr_grain.warehouse_id','=','warehouse.id')
                ->leftJoin('client','dtr_grain.client_id','=','client.id')
                ->where('dtr_audit.approved','2')
                ->where('warehouse.active','1')
                ->where('client.active','1')
                ->whereIn('dtr_audit.warehouse_id', $whIds)
                ->groupBy('dtr_audit.warehouse_id','dtr_grain.client_id','dtr_grain.gdn_no','dtr_grain.stk_no')
                //->orderBy('CAST(dtr_grain.stk_no AS INTEGER)','asc')
                ->orderBy('client.client_name','asc')
                ->get();


                $new = array();
                foreach ($currentStock as $cs) {
                    if(($cs->current_stock_bags>5) && ($cs->current_stock_qty>5)){
                        $new[] = $cs;
                    }
                }

                $currentStock = $new;

                //dd($currentStock);

                if(count($currentStock)==0){
                
                $currentStockCotton = DB::connection('mysql2')->table('dtr_cotton')->select('warehouse.warehouse_code','client.client_name','client.id as clientId',DB::raw('0 as stk_no'),DB::raw('0 as gdn_no'),DB::raw('round(SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_cotton.qty,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_cotton.qty,0))),3) as current_stock_qty'),DB::raw('SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_cotton.qty,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_cotton.qty,0))) as current_stock_bags'),DB::raw('MAX(dtr_cotton.created_at) as max_created_at'))
                ->join('dtr_audit','dtr_cotton.dtr_audit_id','=','dtr_audit.id')
                ->leftJoin('warehouse','dtr_cotton.warehouse_id','=','warehouse.id')
                ->leftJoin('client','dtr_cotton.client_id','=','client.id')
                ->where('dtr_audit.approved','2')
                ->where('warehouse.active','1')
                ->where('client.active','1')
                ->whereIn('dtr_audit.warehouse_id', $whIds)
                ->groupBy('dtr_audit.warehouse_id','dtr_cotton.client_id')
                ->get();

                //echo "<pre>";
                //print_r($currentStockCotton);die;

                $new = array();
                foreach ($currentStockCotton as $cs) {
                    if(($cs->current_stock_bags>0) && ($cs->current_stock_qty>0)){
                        $new[] = $cs;
                    }
                }

                $currentStockCotton = $new;
                
            }

            //Code for CM Stock
            $warehouseIdsObjCm = DB::connection('mysql3')->table('tbl_warehouses')->select('id')->where(["warehouse_code"=>$misdata->warehouse_code])->get();

                $whIdsCm=[];
                foreach ($warehouseIdsObjCm as $id) {
                    $whIdsCm[]=$id->id;
                }

                //print_r($whIds);die;
                $currentStockCm = DB::connection('mysql3')->table('tbl_srs as BaseTbl')->select("S.name as state","Loc.name as location", "BaseTbl.id", "BaseTbl.storage_receipt_no",  "B.borrowerName as borrowerName_cm", "W.name as warehouse","W.warehouse_code", "C.client_name as client_name_cm","C.id as clientId_cm", "Com.commodity", "CV.commodity_variety", "BaseTbl.total_bags", "BaseTbl.net_weight", "BaseTbl.release_bag", "BaseTbl.release_qty",DB::raw("SUM(BaseTbl.closing_bag) as closing_bag"), DB::raw("SUM(BaseTbl.closing_qty) as closing_qty"), "BaseTbl.market_rate", "BaseTbl.value_of_commodity", "BaseTbl.createdDtm")
                    ->leftJoin('tbl_warehouses as W','BaseTbl.warehouse_id','=','W.id')
                    ->leftJoin('tbl_clients as C','BaseTbl.client_id','=','C.id')
                    ->leftJoin("tbl_borrowers as B","BaseTbl.borrower_id",'=','B.id')
                    ->leftJoin("tbl_states as S","BaseTbl.state_id","=","S.id")
                    ->leftJoin("tbl_states as Loc","BaseTbl.location_id","=","Loc.id")
                    ->leftJoin("tbl_commodity as Com","BaseTbl.commodity_id","=","Com.id")
                    ->leftJoin("tbl_commodity_variety as CV","BaseTbl.commodity_variety","=","CV.id")
                    ->where('BaseTbl.isDeleted',0)
                    ->where('BaseTbl.approved',2)
                    ->where('BaseTbl.closing_bag',">",0)
                    ->groupBy('B.id','C.id')
                    ->whereIn('BaseTbl.warehouse_id',$whIdsCm)
                    ->orderBy("BaseTbl.id","DESC")
                    ->get();
                
                return view('auditinspection/add',compact('misdata','currentStock','currentStockCm','currentStockCotton','level1'));    

        }

        }else{
            return redirect()->route('auditinspection.index')->with('success', 'You cannot audit any warehouse within 7 days of previous audit!');            
        }
    }

    public function save(Request $request){

    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }

        //dd($request);

        if($request->file('selfie_of_auditor_with_closed_warehouse_gate')){
            $file = $request->file('selfie_of_auditor_with_closed_warehouse_gate');
            $selfieName=$file->getClientOriginalName();
            $dT= date('d-m-Y-H-m-s');
            $imagePath = public_path('/storage/inspectionImages/selfieOfAuditor/');
            $file->move($imagePath,$dT."_".$selfieName);
            
        }

        if ($request->get('location_closed')=="Yes" || $request->get('location_not_started_before_audit')=="Yes" || $request->get('cm_available_during_visit')=="No" || $request->get('whole_warehouse_under_fumigation')=="Yes") {
           $data = [
          "auditor_id" => $request->get('auditor_id'),
          "name_of_checker" => $request->get('name_of_checker'),  
          "location_closed" => $request->get('location_closed'),
          "location_not_started_before_audit" => $request->get('location_not_started_before_audit'),
          "cm_not_available_remark" => $request->get('cm_not_available_remark'), 
          "selfie_of_auditor_with_closed_warehouse_gate"  =>      isset($selfieName) ? $dT."_".$selfieName : '', 
          "cm_available_during_visit" => $request->get('cm_available_during_visit'),
          "whole_warehouse_under_fumigation" => $request->get('whole_warehouse_under_fumigation'),  
          "who_has_the_control_on_lock_and_key" => $request->get('who_has_the_control_on_lock_and_key'),
          "auditor_name" => $request->get('auditor_name'),
          "warehouse_code" => $request->get('warehouse_code'),
          "state" => $request->get('state'),
          "name_of_warehouse" => $request->get('name_of_warehouse'),
          "location_name" => $request->get('location_name'),
          "warehouse_start_date" => date('Y-m-d', strtotime($request->get('warehouse_start_date'))),
          "type_of_warehouse" => $request->get('type_of_warehouse'),
          "unrecorded_stock" => $request->get('unrecorded_stock'),
          "type_of_structure" => $request->get('type_of_structure'),
          "last_transaction_date" => date('Y-m-d', strtotime($request->get('last_transaction_date'))),
          "last_transaction_date_system" => $request->get('last_transaction_date_system'),
          "name_of_last_visit_area_manager" => $request->get('name_of_last_visit_area_manager'),    
          "name_of_last_visit_cluster_manager" => $request->get('name_of_last_visit_cluster_manager'),
          
        ];

        if ($request->get('cm_available_during_visit')=="No" || $request->get('whole_warehouse_under_fumigation')=="Yes") {
          $data["reschedule_date"] = date('Y-m-d', strtotime($request->get('reschedule_date')));
        }

      
        $id = AuditInspection::insertGetId($data);
        $misdata_id = $request->get('misdata_id');//misdata id comming from view
        $misdata = DB::table('misdata')->where('id',$misdata_id)->update(['audit_inspection_id'=>$id]);

           return redirect()->route('auditinspection.index')->with('success', 'Data Inserted Successfully!');
        }

        if(isset($request['client_name'])){
        foreach($request['client_name'] as $mykey => $myvalue) {
            $myname = 'swipe_borrower_name_'.$mykey;
            if($request[$myname] == '')
            {
                $request[$myname] = $myvalue;
            }
         }
        }
       //dd($request);


        $validator = Validator::make($request->all(),[
                    "location_closed" => 'required',
                    "location_not_started_before_audit" => 'required', 
                    "cm_available_during_visit" => 'required',
                    "whole_warehouse_under_fumigation" => 'required',  
                    "who_has_the_control_on_lock_and_key" => 'required',
                    "auditor_name" => 'required',
                    "warehouse_code" => 'required',
                    "state" => 'required',
                    "name_of_warehouse" => 'required',
                    "location_name" => 'required',
                    "warehouse_start_date" => 'required',
                    "type_of_warehouse" => 'required',
                    //"last_transaction_date"=>'required',
                    
          ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        
    	$data = [
          "auditor_id" => $request->get('auditor_id'),  
          "name_of_checker" => $request->get('name_of_checker'),
          "location_closed" => $request->get('location_closed'),
          "location_not_started_before_audit" => $request->get('location_not_started_before_audit'),
          "cm_not_available_remark" => $request->get('cm_not_available_remark'), 
          "selfie_of_auditor_with_closed_warehouse_gate"  =>      isset($selfieName) ? $dT."_".$selfieName : '', 
          "cm_available_during_visit" => $request->get('cm_available_during_visit'),
          "whole_warehouse_under_fumigation" => $request->get('whole_warehouse_under_fumigation'),  
          "who_has_the_control_on_lock_and_key" => $request->get('who_has_the_control_on_lock_and_key'),
          "auditor_name" => $request->get('auditor_name'),
    	  "warehouse_code" => $request->get('warehouse_code'),
	      "state" => $request->get('state'),
	      "name_of_warehouse" => $request->get('name_of_warehouse'),
	      "location_name" => $request->get('location_name'),
	      "warehouse_start_date" => date('Y-m-d', strtotime($request->get('warehouse_start_date'))),
	      "type_of_warehouse" => $request->get('type_of_warehouse'),
          "unrecorded_stock" => $request->get('unrecorded_stock'),
          "type_of_structure" => $request->get('type_of_structure'),
	      "last_transaction_date" => date('Y-m-d', strtotime($request->get('last_transaction_date'))),
          "last_transaction_date_system" => $request->get('last_transaction_date_system'),
          "name_of_last_visit_area_manager" => $request->get('name_of_last_visit_area_manager'),    
          "name_of_last_visit_cluster_manager" => $request->get('name_of_last_visit_cluster_manager'),
    	];
      
    	$id = AuditInspection::insertGetId($data);
        // Data Insertion in clientwise_stock table
            $client_name_cm = $request->get('client_name_cm');
            if($id && isset($client_name_cm)){
            foreach ($client_name_cm as $key=>$cn) {
                $subData = [
                    "inspection_id"         =>      $id,
                    "client_name"           =>      $request->get('client_name_cm')[$key],
                    "client_id"             =>      $request->get('client_id_cm')[$key],
                    "borrower_name"          =>     $request->get('borrowerName_cm')[$key],
                    "closing_bag"           =>      $request->get('closing_bag')[$key],
                    "closing_qty"           =>      $request->get('closing_qty')[$key],
                    "borrower_name_matching" =>      $request->get('borrower_name_matching_cm')[$key],
                    "bank_name_matching"    =>      $request->get('bank_name_matching_cm')[$key],
                    "stack_status_1"        =>      $request->get('stack_status_1_cm')[$key],
                    "stack_status_2"        =>      $request->get('stack_status_2_cm')[$key],
                    "stack_health_status"   =>      $request->get('stack_health_status_cm')[$key],
                    "number_of_bags"        =>      $request->get('number_of_bags_cm')[$key],
                ];

                $insertId =    DB::table('clientwise_stock')->insertGetId($subData);
             }
            }     
            
        //Insertion of data in clientwise_stock table ends here

        //Insertion of data in chamberwise_stock and health_status table starts here  
        $chamber_number = $request->get('chamber_number');
        if($id && isset($chamber_number)){
            
            foreach ($chamber_number as $key=>$ch) {
                $chamNo =  $request->get('chamber_number')[$key];
                    $subData = [
                        "inspection_id"         =>      $id,
                        "chamber_number"        =>      $request->get('chamber_number')[$key],
                        "stack_number"          =>      $request->get('stack_number')[$key],
                        "client_id"           =>      $request->get('client_id')[$key],
                        "client_name"           =>      $request->get('client_name')[$key],
                        "current_stock_bags"    =>      $request->get('current_stock_bags')[$key],
                        "current_stock_qty"     =>      $request->get('current_stock_qty')[$key],
                        "max_created_at"     =>      $request->get('max_created_at')[$key],
                        "borrower_name_matching" =>      $request->get('borrower_name_matching')[$key],
                        "bank_name_matching"    =>      $request->get('bank_name_matching')[$key],
                        "stack_status_1"        =>      $request->get('stack_status_1')[$key],
                        "stack_status_2"        =>      $request->get('stack_status_2')[$key],
                        "stack_health_status"   =>      $request->get('stack_health_status')[$key],
                        "number_of_bags"        =>      $request->get('number_of_bags')[$key],
                    ];

                $inserId =    DB::table('chamberwise_stock')->insertGetId($subData);
                if($inserId){

                    $borrowerName = $request['swipe_borrower_name_'.$key];
                    
                    $stackNo = json_encode($request['stackNo_'.$key]);
                    $noOfBlock = json_encode($request['noOfBlock_'.$key]);
                    $danda = json_encode($request['danda_'.$key]);
                    $patti = json_encode($request['patti_'.$key]);
                    $d = json_encode($request['d_'.$key]);
                    $height = json_encode($request['height_'.$key]);
                    $bagsF = json_encode($request['bagsF_'.$key]);
                    $bagsG = json_encode($request['bagsG_'.$key]);
                    $total = json_encode($request['total_'.$key]);

                    $dataArr = [
                        'inspection_id'         =>      $id,
                        'chamberwise_stock_id'  =>      $inserId,
                        'chamber_number'        =>      $chamNo,
                        'borrower_name'         =>      $borrowerName,
                        'stack_no'              =>      $stackNo,
                        'no_of_block'           =>      $noOfBlock,
                        'danda'                 =>      $danda,
                        'patti'                 =>      $patti,
                        'danda_plus_patti'      =>      $d,
                        'height'                =>      $height,
                        'plus_bags'             =>      $bagsF,
                        'minus_bags'            =>      $bagsG,
                        'total'                 =>      $total

                    ];

                    //print_r($dataArr);die;
                    DB::table('health_status')->insert($dataArr);
               } 
            }


           
        }
        //Insertion of data in chamberwise_stock and health_status table ends here
        //die;
    	$misdata_id = $request->get('misdata_id');//misdata id comming from view
    	$misdata = DB::table('misdata')->where('id',$misdata_id)->update(['audit_inspection_id'=>$id]);
    	return redirect()->route('auditinspection.addmore',[$id])->with('success','Data Inserted Successfully.');	
    }

    public function addmore($id){
    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
        
        //DB::enableQueryLog();
        $misdata = DB::table('misdata')->select('*')->where('audit_inspection_id',$id)->first();
        //dd(DB::getQueryLog());
        //print_r($misdata);die;
    	return view('auditinspection/addmore',compact('id','misdata'));
    }

    public function savemore(Request $request,$id){
    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
        //dd($request);

         $inputPictures = $request->file('expired_ffe_image');

         $pictureArr = array();
         $picture ='';
         $expiryDate = '';
         if($request->hasFile('expired_ffe_image')){ 
            foreach ($inputPictures as $item){
              $expImage = $item->getClientOriginalName(); 
              $name = time() . '_' .$expImage;           
              $item->move(public_path('/storage/inspectionImages/ffeExpiredImages/'.$id.''), $name);  
              $pictureArr[] = $name;                         
            }
            $expiryDate = implode("|", $request->expiry_date);
            $picture = implode("|", $pictureArr); //Image separated by comma
         }else{
            $expiryDate='';
            $picture = '';
         }

        // dd($expiryDate);
        $this->validate($request,[
                    "fire_fighting_equipment_installed" => 'required',
                    "dunnage_material_used" => 'required',
                    "arya_flex_banner" => 'required',
                    "live_electricity" => 'required',
                    "proper_ventilation_in_the_warehouse" => 'required',
                    "plinth_height_is_less_than_1_feet" => 'required',
                    "lock_and_key_compromise" => 'required',
                    "any_physical_damage_to_warehouse_structure" => 'required',
                    "are_all_the_registers_available" => 'required',
                    "are_all_the_registers_updated" => 'required',
                    "all_svs_ro_and_acknowledgments_available" => 'required',
                    "date_of_guard_deployment_at_warehouse" => 'required',
                    "photo_of_visiter_register_attached" => 'required',
                    "stack_plan_displayed" => 'required',
                    "stack_plan_match_with_physical_stack" => 'required',

            
          ]);

        if($request->file('structure_image')){
            $file = $request->file('structure_image');
            $structureImg=$file->getClientOriginalName();
            $dT= date('d-m-Y-H-m-s');
            $imagePath = public_path('/storage/inspectionImages/damageStructure/'.$id.'');
            $file->move($imagePath,$dT."_".$structureImg);
            
        }

    	$images = array();
    	if($files=$request->file('warehouse_images')){
	        foreach($files as $file){
	            $name=$file->getClientOriginalName();
	            $dateTime= date('d-m-Y-H-m-s');
	            $imagePath = public_path('/storage/inspectionImages/'.$id.'');
	            $file->move($imagePath,$dateTime."_".$name);
	            $images[]=$name;
	        }
	    }
	    //dd($images);	
    	$data = [
            "fire_fighting_equipment_installed" => $request->get('fire_fighting_equipment_installed'),
            "number_of_ffe" => $request->get('number_of_ffe'),
            "number_of_ffe_expired" => $request->get('number_of_ffe_expired'),
            "expiry_date" => $expiryDate,
            "expired_ffe_image" => $picture,
          "dunnage_material_used" => $request->get('dunnage_material_used'),
          "arya_flex_banner" => $request->get('arya_flex_banner'),
          "live_electricity" => $request->get('live_electricity'),
          "proper_ventilation_in_the_warehouse" => $request->get('proper_ventilation_in_the_warehouse'),
          "plinth_height_is_less_than_1_feet" => $request->get('plinth_height_is_less_than_1_feet'),
    		"lock_and_key_compromise" => $request->get('lock_and_key_compromise'),
	      	"any_physical_damage_to_warehouse_structure" => $request->get('any_physical_damage_to_warehouse_structure'),
            "detail_of_physical_damage_to_warehouse_structure" => $request->get('detail_of_physical_damage_to_warehouse_structure'),
            "structure_image"  =>      isset($structureImg) ? $dT."_".$structureImg : '',
	     	"mention_damage_detail" => $request->get('mention_damage_detail'),
	       	"actual_date_of_audit" => date('Y-m-d', strtotime($request->get('actual_date_of_audit'))),
	       	"reason_of_delay" => $request->get('reason_of_delay'),
	       	"date_of_last_visit_of_area_manager" => date('Y-m-d', strtotime($request->get('date_of_last_visit_of_area_manager'))),
	       	"date_of_last_visit_of_cluster_manager" => date('Y-m-d', strtotime($request->get('date_of_last_visit_of_cluster_manager'))),
	       	"fumigation_record_available_at_warehouse" => $request->get('fumigation_record_available_at_warehouse'),
            "are_all_the_registers_available" => $request->get('are_all_the_registers_available'),
            "are_all_the_registers_updated" => $request->get('are_all_the_registers_updated'),
	       	"all_svs_ro_and_acknowledgments_available" => $request->get('all_svs_ro_and_acknowledgments_available'),
	       	"name_of_the_cm" => $request->get('name_of_the_cm'),
	       	"date_of_guard_deployment_at_warehouse" => date('Y-m-d', strtotime($request->get('date_of_guard_deployment_at_warehouse'))),
	       	"security_day_guard_attendance" => $request->get('security_day_guard_attendance'),
	       	"security_day_guard_available_during_the_visit" => $request->get('security_day_guard_available_during_the_visit'),
	       	"security_night_guard_attendance" => $request->get('security_night_guard_attendance'),
	       	"security_guard_attendance_registers_available" => $request->get('security_guard_attendance_registers_available'),
            "security_guard_attendance_registers_updated" => $request->get('security_guard_attendance_registers_updated'),
	       	"cleanliness_of_warehouse" => $request->get('cleanliness_of_warehouse'),
	       	"pending_queries_resolved" => $request->get('pending_queries_resolved'),
	       	"photo_of_visiter_register_attached" => $request->get('photo_of_visiter_register_attached'),
            "stack_plan_displayed" => $request->get('stack_plan_displayed'),
            "stack_plan_match_with_physical_stack" => $request->get('stack_plan_match_with_physical_stack'),
            "name_of_the_ops_team" => $request->get('name_of_the_ops_team'),
	       	"any_other_observation_or_remark" => $request->get('any_other_observation_or_remark'),
            "name_of_register_not_available" => $request->get('name_of_register_not_available'),
            "name_of_register_not_updated" => $request->get('name_of_register_not_updated'),
            "observation_remark" => $request->get('observation_remark'),
	       	"image_1"					=> 		isset($images[0]) ? $dateTime."_".$images[0] : '',
	       	"image_2"					=> 		isset($images[1]) ? $dateTime."_".$images[1] : '',
	       	"image_3"					=> 		isset($images[2]) ? $dateTime."_".$images[2] : '',
	       	"image_4"					=> 		isset($images[3]) ? $dateTime."_".$images[3] : '',
	       	"image_5"					=> 		isset($images[4]) ? $dateTime."_".$images[4] : '',				
    	];
    	$ai = AuditInspection::findOrFail($id);
        $ai->update($data);
    	
    	return redirect()->route('auditinspection.index')->with('success', 'Data Inserted Successfully!');
    }

    public function edit($id){
    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }

        $currentStock = DB::table('chamberwise_stock')->where('inspection_id',$id)->get(); 
        //dd($currentStock); 
        $healthStatus = DB::table('health_status')->where('inspection_id',$id)->get();

        $currentStockCm = DB::table('clientwise_stock')->where('inspection_id',$id)->get();

        $level1 = User::whereHas("roles", function($q){ $q->where("name", "level 1"); })->get();

        //echo "<pre>";
        //print_r($healthStatus);die;
        if(count($healthStatus) > 0){
        if($healthStatus[0]->stack_no!='null'){
        $healthStatusArr = array();
        foreach ($healthStatus as $row) {
            
            $chamberwise_stock_id = $row->chamberwise_stock_id;
            $cham_no = $row->chamber_number;
            $stack_no = json_decode($row->stack_no);
            $borrower_name = $row->borrower_name;
            $no_of_block = json_decode($row->no_of_block);
            $danda = json_decode($row->danda);
            $patti = json_decode($row->patti);
            $danda_plus_patti = json_decode($row->danda_plus_patti);
            $height = json_decode($row->height);
            $plus_bags = json_decode($row->plus_bags);
            $minus_bags = json_decode($row->minus_bags);
            $total = json_decode($row->total);

            foreach ($stack_no as $key => $value) {
                $data = [
                'chamberwise_stock_id'=>    $chamberwise_stock_id,
                'cham_no'           =>      $cham_no,
                'stack_no'          =>      $stack_no[$key],
                'borrower_name'     =>      $borrower_name,
                'no_of_block'       =>      $no_of_block[$key],
                'danda'             =>      $danda[$key],
                'patti'             =>      $patti[$key],
                'danda_plus_patti'  =>      $danda_plus_patti[$key],
                'height'            =>      $height[$key],
                'plus_bags'         =>      $plus_bags[$key],
                'minus_bags'        =>      $minus_bags[$key],
                'total'             =>      $total[$key]
            ];
            $healthStatusArr[] = $data;
            }   
        }
        //echo "<pre>";
        //print_r($healthStatusArr);die;
        }
        }


    	$auditinspection = AuditInspection::find($id);
    	return view('auditinspection/add',	compact('auditinspection','currentStock','healthStatusArr','currentStockCm','level1'));
    }

    public function update(Request $request, $id){
    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
        //dd($request);
        $this->validate($request,[
             'location_closed'=>'required',
             'location_not_started_before_audit'=>'required'
          ]);


        if ($request->get('location_closed')=="Yes"|| $request->get('location_not_started_before_audit')=="Yes" || $request->get('cm_available_during_visit')=="No") {
           return redirect()->route('auditinspection.index')->with('success', 'Data Updated Successfully!');
        }


        if($request->file('selfie_of_auditor_with_closed_warehouse_gate')){
            $file = $request->file('selfie_of_auditor_with_closed_warehouse_gate');
            $selfieName=$file->getClientOriginalName();
            $dT= date('d-m-Y-H-m-s');
            $imagePath = public_path('/storage/inspectionImages/selfieOfAuditor/'.$id.'');
            $file->move($imagePath,$dT."_".$selfieName);
            
        }

        $auditorSelfie = AuditInspection::select('selfie_of_auditor_with_closed_warehouse_gate')->where('id',$id)->first();

        $auditorImage = isset($selfieName) ? $dT."_".$selfieName : (isset($auditorSelfie->selfie_of_auditor_with_closed_warehouse_gate) ? $auditorSelfie->selfie_of_auditor_with_closed_warehouse_gate : '');

        if($auditorSelfie->selfie_of_auditor_with_closed_warehouse_gate !='' && isset($selfieName)){
            unlink(public_path('/storage/inspectionImages/selfieOfAuditor/'.$id.'/').$auditorSelfie->selfie_of_auditor_with_closed_warehouse_gate);
        }


    	$data = [
          "auditor_id" => $request->get('auditor_id'), 
          "name_of_checker" => $request->get('name_of_checker'), 
          "location_closed" => $request->get('location_closed'),
          "location_not_started_before_audit" => $request->get('location_not_started_before_audit'),
          "cm_available_during_visit" => $request->get('cm_available_during_visit'),
          "cm_not_available_remark" => $request->get('cm_not_available_remark'), 
          "selfie_of_auditor_with_closed_warehouse_gate"  =>      $auditorImage,  
          "who_has_the_control_on_lock_and_key" => $request->get('who_has_the_control_on_lock_and_key'),
          "auditor_name" => $request->get('auditor_name'),
          "whole_warehouse_under_fumigation" => $request->get('whole_warehouse_under_fumigation'),
          "warehouse_code" => $request->get('warehouse_code'),
          "state" => $request->get('state'),
          "name_of_warehouse" => $request->get('name_of_warehouse'),
          "location_name" => $request->get('location_name'),
          "warehouse_start_date" => date('Y-m-d', strtotime($request->get('warehouse_start_date'))),
          "type_of_warehouse" => $request->get('type_of_warehouse'),
          "unrecorded_stock" => $request->get('unrecorded_stock'),
          "type_of_structure" => $request->get('type_of_structure'),
          "last_transaction_date" => date('Y-m-d', strtotime($request->get('last_transaction_date'))),
          "name_of_last_visit_area_manager" => $request->get('name_of_last_visit_area_manager'),    
          "name_of_last_visit_cluster_manager" => $request->get('name_of_last_visit_cluster_manager'),
          
        ];

        //dd($request);

    	$ai = AuditInspection::findOrFail($id);
        $ai = $ai->update($data);

        
        // Data Updation in clientwise_stock table
            $client_name_cm = $request->get('client_name_cm');
            if(($ai) && (isset($client_name_cm))){

            foreach ($client_name_cm as $key=>$cn) {
                $client_id_cm = $request->get('client_id_cm')[$key];
                $borrowerName_cm = $request->get('borrowerName_cm')[$key];
                $subData = [
                    "inspection_id"         =>      $id,
                    "client_name"           =>      $request->get('client_name_cm')[$key],
                    "client_id"             =>      $request->get('client_id_cm')[$key],
                    "borrower_name"          =>     $request->get('borrowerName_cm')[$key],
                    "closing_bag"           =>      $request->get('closing_bag')[$key],
                    "closing_qty"           =>      $request->get('closing_qty')[$key],
                    "borrower_name_matching" =>      $request->get('borrower_name_matching_cm')[$key],
                    "bank_name_matching"    =>      $request->get('bank_name_matching_cm')[$key],
                    "stack_status_1"        =>      $request->get('stack_status_1_cm')[$key],
                    "stack_status_2"        =>      $request->get('stack_status_2_cm')[$key],
                    "stack_health_status"   =>      $request->get('stack_health_status_cm')[$key],
                    "number_of_bags"        =>      $request->get('number_of_bags_cm')[$key],
                ];

                DB::table('clientwise_stock')->where(['inspection_id'=>$id,'client_id'=>$client_id_cm,'borrower_name'=>$borrowerName_cm])->update($subData);
             }
            }     
            
        //Updation of data in clientwise_stock table ends here
        //dd($request);    
        $chamber_number = $request->get('chamber_number');        
        if(($ai) && (isset($chamber_number))){    
            //$data = DB::table('chamberwise_stock')->where('inspection_id',$id);
            
            foreach ($chamber_number as $key=>$row) {
                    $chamNo = $request->get('chamber_number')[$key];
                    $stkNo = $request->get('stack_number')[$key];
                    $cName  =$request->get('client_name')[$key];
                    $subData = [
                        "borrower_name_matching"=>      $request->get('borrower_name_matching')[$key],
                        "bank_name_matching"    =>      $request->get('bank_name_matching')[$key],
                        "stack_status_1"        =>      $request->get('stack_status_1')[$key],
                        "stack_status_2"        =>      $request->get('stack_status_2')[$key],
                        "stack_health_status"   =>      $request->get('stack_health_status')[$key],
                        "number_of_bags"        =>      $request->get('number_of_bags')[$key],
                    ];

                //DB::enableQueryLog();    
                $affectedRows = DB::table('chamberwise_stock')->where(['inspection_id'=>$id,'chamber_number'=>$chamNo,'stack_number'=>$stkNo,'client_name'=>$cName])->update($subData);
                //dd(DB::getQueryLog());
                //echo $affectedRows;die;

                //if($affectedRows){
                    // $chamberwiseStockId = $request['chamberwise_stock_id_'.$key];     
                    // $borrowerName = $request['borrower_name_'.$key];
                    // $stackNo = json_encode($request['stackNo_'.$key]);
                    // $noOfBlock = json_encode($request['noOfBlock_'.$key]);
                    // $danda = json_encode($request['danda_'.$key]);
                    // $patti = json_encode($request['patti_'.$key]);
                    // $d = json_encode($request['d_'.$key]);
                    // $height = json_encode($request['height_'.$key]);
                    // $bagsF = json_encode($request['bagsF_'.$key]);
                    // $bagsG = json_encode($request['bagsG_'.$key]);
                    // $total = json_encode($request['total_'.$key]);

                    // $dataArr = [
                    //     'stack_no'              =>      $stackNo,
                    //     'no_of_block'           =>      $noOfBlock,
                    //     'danda'                 =>      $danda,
                    //     'patti'                 =>      $patti,
                    //     'danda_plus_patti'      =>      $d,
                    //     'height'                =>      $height,
                    //     'plus_bags'             =>      $bagsF,
                    //     'minus_bags'            =>      $bagsG,
                    //     'total'                 =>      $total

                    // ];
                    // // echo "<pre>";
                    // // echo $chamberwiseStockId;
                    // // print_r($dataArr);
                    // DB::table('health_status')->where(['inspection_id'=>$id,'borrower_name'=>$borrowerName,'chamberwise_stock_id'=>$chamberwiseStockId])->update($dataArr);

           // }

        }     
        //die;

        }

        return redirect()->route('auditinspection.editmore',[$id])->with('success', 'Data Updated Successfully!');;
    }

    public function editmore($id){
    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
    	$auditinspection = AuditInspection::find($id);
        $level1 = User::whereHas("roles", function($q){ $q->where("name", "level 1"); })->get();

        $misdata = DB::table('misdata')->select('*')->where('audit_inspection_id',$id)->first();
        if(empty($misdata)){
            echo "Data insertion error due to network. Please contact to Rakesh Sir.";die;
        }
    	return view('auditinspection/addmore',	compact('auditinspection','level1','misdata'));
    }

    public function updatemore(Request $request, $id){
    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
        //dd($request);
         
         $expFfe = AuditInspection::select('expired_ffe_image')->where('id',$id)->first();

         $imagesFromDbArr = explode("|", $expFfe->expired_ffe_image);
         
         $pictureArr = array();
         
         if($request->hasFile('expired_ffe_image')){
            $inputPictures = $request->file('expired_ffe_image'); 
            foreach ($imagesFromDbArr as $key=>$item){

                if(isset($inputPictures[$key])){
                     $inputExpImage =  $inputPictures[$key]; 
                      $expImage = $inputExpImage->getClientOriginalName(); 
                      $name = time() . '_' .$expImage;           
                      $inputExpImage->move(public_path('/storage/inspectionImages/ffeExpiredImages/'.$id.''), $name);  
                      //unlink(public_path('/storage/inspectionImages/ffeExpiredImages/'.$id.'/').$item);
                      $pictureArr[] = $name;
                  }else{
                        $pictureArr[] = $item;
                  }                         
            }
            $expiryDate = implode("|", $request->expiry_date);
            $picture = implode("|", $pictureArr); //Image separated by comma
         }else{
            $expiryDate= implode("|", $request->expiry_date);
            $picture = $expFfe->expired_ffe_image;
         }

         //dd($pictureArr);

        if($request->file('structure_image')){
            $file = $request->file('structure_image');
            $structImg=$file->getClientOriginalName();
            $dT= date('d-m-Y-H-m-s');
            $imagePath = public_path('/storage/inspectionImages/damageStructure/'.$id.'');
            $file->move($imagePath,$dT."_".$structImg);
            
        }

        $structureImage = AuditInspection::select('structure_image')->where('id',$id)->first();

        $structureImg = isset($structImg) ? $dT."_".$structImg : (isset($structureImage->structure_image) ? $structureImage->structure_image : '');

        if($structureImage->structure_image !='' && isset($structImg)){
            unlink(public_path('/storage/inspectionImages/damageStructure/'.$id.'/').$structureImage->structure_image);
        }

    	$images = array();
    	if($files=$request->file('warehouse_images')){
	        foreach($files as $key=>$file){
	            $name=$file->getClientOriginalName();
	            $dateTime= date('d-m-Y-H-m-s');
	            $imagePath = public_path('/storage/inspectionImages/'.$id.'');
	            $file->move($imagePath,$dateTime."_".$name);
	            $images[$key]=$name;
	        }
	    }
	    //dd($images);

	    $savedImages = AuditInspection::select('image_1','image_2','image_3','image_4','image_5')->where('id',$id)->first();


	    $image_1 = isset($images[0]) ? $dateTime."_".$images[0] : (isset($savedImages->image_1) ? $savedImages->image_1 : '');
	    $image_2 = isset($images[1]) ? $dateTime."_".$images[1] : (isset($savedImages->image_2) ? $savedImages->image_2 : '');
	    $image_3 = isset($images[2]) ? $dateTime."_".$images[2] : (isset($savedImages->image_3) ? $savedImages->image_3 : '');
	    $image_4 = isset($images[3]) ? $dateTime."_".$images[3] : (isset($savedImages->image_4) ? $savedImages->image_4 : '');
	    $image_5 = isset($images[4]) ? $dateTime."_".$images[4] : (isset($savedImages->image_5) ? $savedImages->image_5 : '');

	    if($savedImages->image_1 !='' && isset($images[0])){
	    	unlink(public_path('/storage/inspectionImages/'.$id.'/').$savedImages->image_1);
	    }

	    if($savedImages->image_2 !='' && isset($images[1])){
	    	unlink(public_path('/storage/inspectionImages/'.$id.'/').$savedImages->image_2);
	    }

	    if($savedImages->image_3 !='' && isset($images[2])){
	    	unlink(public_path('/storage/inspectionImages/'.$id.'/').$savedImages->image_3);
	    }

	    if($savedImages->image_4 !='' && isset($images[3])){
	    	unlink(public_path('/storage/inspectionImages/'.$id.'/').$savedImages->image_4);
	    }

	    if($savedImages->image_5 !='' && isset($images[4])){
	    	unlink(public_path('/storage/inspectionImages/'.$id.'/').$savedImages->image_5);
	    }
	    
    	$data = [
            "fire_fighting_equipment_installed" => $request->get('fire_fighting_equipment_installed'),
            "number_of_ffe" => $request->get('number_of_ffe'),
            "number_of_ffe_expired" => $request->get('number_of_ffe_expired'),
            "expiry_date" => $expiryDate,
            "expired_ffe_image" => $picture,
          "dunnage_material_used" => $request->get('dunnage_material_used'),
          "arya_flex_banner" => $request->get('arya_flex_banner'),
          "live_electricity" => $request->get('live_electricity'),
          "proper_ventilation_in_the_warehouse" => $request->get('proper_ventilation_in_the_warehouse'),
          "plinth_height_is_less_than_1_feet" => $request->get('plinth_height_is_less_than_1_feet'),
    		"lock_and_key_compromise" => $request->get('lock_and_key_compromise'),
	      	"any_physical_damage_to_warehouse_structure" => $request->get('any_physical_damage_to_warehouse_structure'),
	     	"mention_damage_detail" => $request->get('mention_damage_detail'),
	       	"actual_date_of_audit" => date('Y-m-d', strtotime($request->get('actual_date_of_audit'))),
	       	"reason_of_delay" => $request->get('reason_of_delay'),
	       	"date_of_last_visit_of_area_manager" => date('Y-m-d', strtotime($request->get('date_of_last_visit_of_area_manager'))),
	       	"date_of_last_visit_of_cluster_manager" => date('Y-m-d', strtotime($request->get('date_of_last_visit_of_cluster_manager'))),
	       	"fumigation_record_available_at_warehouse" => $request->get('fumigation_record_available_at_warehouse'),
            "are_all_the_registers_available" => $request->get('are_all_the_registers_available'),
            "are_all_the_registers_updated" => $request->get('are_all_the_registers_updated'),
	       	"all_svs_ro_and_acknowledgments_available" => $request->get('all_svs_ro_and_acknowledgments_available'),
	       	"name_of_the_cm" => $request->get('name_of_the_cm'),
	       	"date_of_guard_deployment_at_warehouse" => date('Y-m-d', strtotime($request->get('date_of_guard_deployment_at_warehouse'))),
	       	"security_day_guard_attendance" => $request->get('security_day_guard_attendance'),
	       	"security_day_guard_available_during_the_visit" => $request->get('security_day_guard_available_during_the_visit'),
	       	"security_night_guard_attendance" => $request->get('security_night_guard_attendance'),
	       	"security_guard_attendance_registers_available" => $request->get('security_guard_attendance_registers_available'),
            "security_guard_attendance_registers_updated" => $request->get('security_guard_attendance_registers_updated'),
	       	"cleanliness_of_warehouse" => $request->get('cleanliness_of_warehouse'),
	       	"pending_queries_resolved" => $request->get('pending_queries_resolved'),
	       	"photo_of_visiter_register_attached" => $request->get('photo_of_visiter_register_attached'),
            "stack_plan_displayed" => $request->get('stack_plan_displayed'),
            "stack_plan_match_with_physical_stack" => $request->get('stack_plan_match_with_physical_stack'),
            "name_of_the_ops_team" => $request->get('name_of_the_ops_team'),
	       	"any_other_observation_or_remark" => $request->get('any_other_observation_or_remark'),
            "name_of_register_not_available" => $request->get('name_of_register_not_available'),
            "name_of_register_not_updated" => $request->get('name_of_register_not_updated'),
             "observation_remark" => $request->get('observation_remark'),
	       	"image_1"					=> 		$image_1,
	       	"image_2"					=> 		$image_2,
	       	"image_3"					=> 		$image_3,
	       	"image_4"					=> 		$image_4,
	       	"image_5"					=> 		$image_5,				
    	];
    	 
    	$ai = AuditInspection::findOrFail($id);
        $ai->update($data);

        return redirect()->route('auditinspection.index')->with('success', 'Data Updated Successfully!');;
    }

    public function delete($id){
    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
        $ai = AuditInspection::findOrFail($id);
        $ai->update(['status'=>0]);

        return redirect()->route('auditinspection.index');
    }

    public function view($id){
    	if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }

   		$auditinspection = AuditInspection::find($id);

        $chamberwiseStock = DB::table('chamberwise_stock')->where('inspection_id',$id)->get();

        $healthStatus = DB::table('health_status')->where('inspection_id', $id)->get();

        foreach ($chamberwiseStock as $key => $value) {
            $chamberwiseStock[$key]->countable_number_of_bags=null;
        }

        //print_r($healthStatus);die;

        if(count($healthStatus) > 0){
        if($healthStatus[0]->stack_no!='null'){    
            $healthStatusArr = array();
        foreach ($healthStatus as $index=>$row) {
            
            $chamberwise_stock_id = json_decode($row->chamberwise_stock_id);
            $stack_no = json_decode($row->stack_no);
            $borrower_name = $row->borrower_name;
            $no_of_block = json_decode($row->no_of_block);
            $danda = json_decode($row->danda);
            $patti = json_decode($row->patti);
            $danda_plus_patti = json_decode($row->danda_plus_patti);
            $height = json_decode($row->height);
            $plus_bags = json_decode($row->plus_bags);
            $minus_bags = json_decode($row->minus_bags);
            $total = json_decode($row->total);

            $countableNoOfBags = 0;

            foreach ($stack_no as $key => $value) {
                $data = [
                'chamberwise_stock_id' =>  $chamberwise_stock_id,  
                'stack_no'          =>      $stack_no[$key],
                'borrower_name'     =>      $borrower_name,
                'no_of_block'       =>      $no_of_block[$key],
                'danda'             =>      $danda[$key],
                'patti'             =>      $patti[$key],
                'danda_plus_patti'  =>      $danda_plus_patti[$key],
                'height'            =>      $height[$key],
                'plus_bags'         =>      $plus_bags[$key],
                'minus_bags'        =>      $minus_bags[$key],
                'total'             =>      $total[$key],
                'match_status'      =>      $row->match_status,
            ];
                $countableNoOfBags += $total[$key];

            $healthStatusArr[] = $data;
            }   

            if(empty($chamberwiseStock[$index]->number_of_bags)){
                $chamberwiseStock[$index]->countable_number_of_bags=$countableNoOfBags;
            }else{
                $chamberwiseStock[$index]->countable_number_of_bags=null;
            }
        }
        }

        }

        //echo "<pre>";
        //print_r($chamberwiseStock);die;


        $clientwiseStock = DB::table('clientwise_stock')->where('inspection_id',$id)->get();
        //dd($clientwiseStock);

        $escalationsObj = DB::table('audit_escalations')->select('audit_escalations.id','audit_escalations.stack_no','audit_escalations.bank_name','audit_escalations.client_name','audit_escalations.description','audit_escalations.field_type','audit_escalations.escalation_value','audit_escalations.created_at','audit_escalations.status','audit_escalations.dishonour_status','users.name')
        ->leftJoin('users','audit_escalations.checker_id','=','users.id')
        ->where('audit_inspection_id',$id)
        ->orderBy('stack_no','asc')
        ->get();
        //echo "<pre>";
   		//print_r($escalationsObj->toArray());
        $escalations = array();
        if(count($escalationsObj)){
            foreach ($escalationsObj as  $esc) {
                //$esc = (array)$esc;
                $auditLogArr = DB::table('audit_logs')
                               ->select('audit_logs.comment','audit_logs.till_date','audit_logs.created_at','users.name')
                               ->leftJoin('users','audit_logs.level_id','=','users.id') 
                               ->where('audit_logs.audit_escalation_id',$esc->id)
                               ->get()->toArray();

                if (count($auditLogArr) > 0) {
                    $esc->auditReply = $auditLogArr;
                }   

                $escalations[] = $esc;                
            }
        }
        //echo "<pre>";
        //print_r($escalations);die;


    	return view('auditinspection/view',compact('auditinspection','chamberwiseStock','escalations','healthStatusArr','clientwiseStock'));
    }

    public function listing(){
		if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
        
		$userName = Auth::user()->name;
        $from=date("Y-m-01");
        $to = date("Y-m-t");
   		$assignedaudit= DB::table('misdata')
                        ->where(['maker_name'=>$userName,'audit_inspection_id'=>NULL])
                        ->where('created_at','>=',$from)
                        ->where('created_at','<=',$to)
                        ->orderBy('id','desc')
                        ->get();
   	
    	return view('auditinspection/list',compact('assignedaudit'));
    }

    public function assign(Request $request){
		if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
        $id = $request->get('id');
		//$role = Auth::user()->roles->pluck('name')->first());
   		$auditinspection = AuditInspection::find($id);
    	$affectedRows = $auditinspection->update(['approved_status' => 1]);
    	if($affectedRows>0){
    		echo json_encode(array('value' => 'Success'));
            die;
    	}else{
    		echo json_encode(array('value' => 'Error'));
            die;
    	}
    }

    public function approve(Request $request){
        if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }
        $id = $request->get('id');
        $role = Auth::user()->roles->pluck('name')->first();
        // if($role=='checker'){
        //     $approved_status = 2;
        // }elseif($role == 'level 1'){
        //     $approved_status = 4;
        // }
        $model = AuditInspection::find($id);
        $chamberwiseStock = DB::table('chamberwise_stock')->where('inspection_id',$id)->get();

        $clientwiseStock = DB::table('clientwise_stock')->where('inspection_id',$id)->get();

         //echo "<pre>";
         //print_r($clientwiseStock->toArray());die;
        
        //Code to make Dishonour Query 
        //SELECT * FROM mytable ORDER BY id DESC LIMIT 1 
        $lastInsObjId= DB::table('audit_inspections')
                                    ->select('id')
                                    ->where('warehouse_code', $model->warehouse_code)
                                    //->where('approved_status','!=',0)
                                    ->where('location_closed','=','No')
                                    ->where('location_not_started_before_audit','=','No')
                                    ->where('cm_available_during_visit','=','Yes')
                                    ->where('whole_warehouse_under_fumigation','No')
                                    ->orderBy('id', 'desc')
                                    //->orderBy('created_at','desc')
                                    ->skip(1)
                                    ->first();

    if(!empty($lastInsObjId)){                            
            $healthStatusCurrent = DB::table('health_status')
                                    ->select('chamberwise_stock.chamber_number','chamberwise_stock.stack_number','chamberwise_stock.stack_status_1','chamberwise_stock.client_name','health_status.chamberwise_stock_id','health_status.no_of_block','health_status.danda','health_status.patti','health_status.height','health_status.total')
                                    ->join('chamberwise_stock','chamberwise_stock.id','=','health_status.chamberwise_stock_id')
                                    ->where('health_status.inspection_id',$id)
                                    ->get();                            

            $healthStatusPrevious = DB::table('health_status')
                                    ->select('chamberwise_stock.chamber_number','chamberwise_stock.stack_number','chamberwise_stock.stack_status_1','chamberwise_stock.client_name','health_status.no_of_block','health_status.danda','health_status.patti','health_status.height','health_status.total')
                                    ->join('chamberwise_stock','chamberwise_stock.id','=','health_status.chamberwise_stock_id')
                                    ->where('health_status.inspection_id',$lastInsObjId->id)
                                    ->get();

        //echo "<pre>";
        //print_r($healthStatusCurrent);
        //print_r($healthStatusPrevious);die;
        //$pvStatusArr = array();
            if(!empty($healthStatusCurrent) && !empty($healthStatusPrevious)){
            foreach ($healthStatusCurrent as $key => $hsc) {

                foreach ($healthStatusPrevious as $key => $hsp) {
                if(($hsc->stack_number==$hsp->stack_number) && ($hsc->chamber_number==$hsp->chamber_number) && ($hsc->client_name==$hsp->client_name) && ($hsc->stack_status_1=='Countable') && ($hsp->stack_status_1=='Countable') && ($hsp->stack_number > 0 ) && (isset($hsc->max_created_at)) && (isset($hsp->max_created_at)) ){
                    if($hsc->max_created_at == $hsp->max_created_at){
                    $noOfBlockCurrent = json_decode($hsc->no_of_block, TRUE);
                    $noOfBlockPrevious = json_decode($hsp->no_of_block, TRUE);
                    $block_array = array_diff($noOfBlockCurrent,$noOfBlockPrevious);

                    $dandaCurrent = json_decode($hsc->danda, TRUE);
                    $dandaPrevious = json_decode($hsp->danda, TRUE);
                    $danda_array = array_diff($dandaCurrent,$dandaPrevious);

                    $pattiCurrent = json_decode($hsc->patti, TRUE);
                    $pattiPrevious = json_decode($hsp->patti, TRUE);
                    $patti_array = array_diff($pattiCurrent,$pattiPrevious);

                    $heightCurrent = json_decode($hsc->height, TRUE);
                    $heightPrevious = json_decode($hsp->height, TRUE);
                    $height_array = array_diff($heightCurrent,$heightPrevious);

                    
                    if(empty($block_array) && empty($danda_array) && empty($patti_array) && empty($height_array)){
                        // if($hsc->stack_number==297){ 
                        //     echo "<pre>";
                        //     print_r($height_array);
                        //     print_r($heightCurrent);
                        //     print_r($heightPrevious);       
                        //     echo "pv is same";die;
                        // }
                        //$pvStatusArr[] = 1;
                        $updateStatus = DB::table('health_status')->where(['inspection_id'=>$id,'chamberwise_stock_id'=>$hsc->chamberwise_stock_id])->update(['match_status'=>1]);
                    }else{
                        // if($hsc->stack_number==297){
                        //     //print_r($height_array[0]);
                        //     echo "<pre>";
                        //     print_r($height_array);
                        //     print_r($heightCurrent);
                        //     print_r($heightPrevious);
                        //     echo "pv is not same";die;
                        // }
                        //$pvStatusArr[] = 0;
                        $updateStatus = DB::table('health_status')->where(['inspection_id'=>$id,'chamberwise_stock_id'=>$hsc->chamberwise_stock_id])->update(['match_status'=>0]);
                    }
                    }
                }
                
            }

            }




            }    
        }                            


                                    
        $fieldsWithQuery = FieldMapper::where('query',1)->get();
        $fieldsWithOneTimeQuery = FieldMapper::select('data_field')->where('one_time_query',1)->get()->toArray();

        $fieldsWithOneTimeQueryArr = array();

        foreach ($fieldsWithOneTimeQuery as $fwotq) {
            $fieldsWithOneTimeQueryArr[] =  $fwotq['data_field'];
        }
        
        //echo "<pre>";
        //print_r($fieldsWithOneTimeQueryArr);die;

        $escalationFieldArr = array();
        $escalationValueArr = array();
        $escalationStackNoArr  = array();

        $escalationFieldArr1 = array();
        $escalationValueArr1 = array();
        $escalationStackNoArr1  = array();
        $escalationClientNameArr1  = array();
        $stackNo = '';
        $lastEscalationFieldStatus = array();
        $lastEscalationFieldStatus1 = array();

        //Arrays to save CM Escalation Fields
        $escalationFieldCmArr = array();
        $escalationValueCmArr = array();
        $escalationBorrowerNameCmArr  = array();
        $escalationClientNameCmArr  = array();
        $lastEscalationFieldStatusCm = array();
        //print_r($fieldsWithQuery);die;

        $overall_system_stock_pwh=0;
        $overall_entered_stock_pwh=0;

        $overall_system_stock_cm=0;
        $overall_entered_stock_cm=0;

        if (count($fieldsWithQuery) > 0) {

        //Escalation queries for field values available in audit_inspection table
            foreach ($fieldsWithQuery as $field) {


                if(empty($lastInsObjId)){

                    if (array_key_exists($field->data_field,$model->toArray())) {
                        $val = $field->data_field;
                        
                        if ($field->data_value != "") {
                            if (strtolower($model->$val) == strtolower($field->data_value)) {
                                
                                //$escalationStackNoArr['stack_no'][] = '';
                                
                                $lastEscalationFieldStatus[$val] = 0;
                                $escalationFieldArr[$val] = $field->label;
                                $escalationValueArr[$val] = $model->$val;
                            
                            }   

                        }

                        if(strtolower($field->data_field)=='date_of_guard_deployment_at_warehouse'){
                            if($field->data_value == ""){
                                $date1 = strtotime($model->$val);
                                $date2 = strtotime($model->warehouse_start_date);
                                $days = $this->diffdate($date2,$date1);
                                //echo $days;die;
                                if($days>7){
                                    $lastEscalationFieldStatus[$val] = 0;
                                    $escalationFieldArr[$val] = $field->label;
                                    $escalationValueArr[$val] = $model->$val;        
                                }

                            }
                        }

                        if(strtolower($field->data_field)=='date_of_last_visit_of_cluster_manager'){
                            if($field->data_value == ""){
                                $date1 = strtotime($model->$val);
                                $date2 = strtotime($model->actual_date_of_audit);
                                $days = $this->diffdate($date1,$date2);
                                if($days>30){
                                    $lastEscalationFieldStatus[$val] = 0;
                                    $escalationFieldArr[$val] = $field->label;
                                    $escalationValueArr[$val] = $model->$val;        
                                }

                            }
                        }

                        if(strtolower($field->data_field)=='observation_remark'){
                           if($model->$val!=''){
                                $lastEscalationFieldStatus[$val] = 0;
                                $escalationFieldArr[$val] = $field->label;
                                $escalationValueArr[$val] = $model->$val;        
                            }   
                        }  

                        if(strtolower($field->data_field)=='last_transaction_date'){
                            if($field->data_value == ""){
                                $date1 = strtotime($model->$val);
                                $date2 = strtotime($model->last_transaction_date_system);
                                $days = $this->diffdate($date2,$date1);
                                //echo $days;die;
                                if($days>6){
                                    $lastEscalationFieldStatus[$val] = 0;
                                    $escalationFieldArr[$val] = $field->label;
                                    $escalationValueArr[$val] = $model->$val;        
                                }

                            }
                        }  



                   } 
                }else{
                    if(in_array($field->data_field, $fieldsWithOneTimeQueryArr)==false){
                        if (array_key_exists($field->data_field,$model->toArray())) {
                        $val = $field->data_field;

                            if ($field->data_value != "") {
                                if (strtolower($model->$val) == strtolower($field->data_value)) {
                        
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                        ->select('field_type','id')  
                                        ->where('audit_inspection_id',$lastInsObjId->id)
                                        ->where('field_type',$field->data_field)
                                        ->first();

                                if(!empty($lastEscalationFieldObj)){        
                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            //->where('field_type',$field->data_field)
                                            ->first();
                                
                                }

                                if(isset($lastEscalationReplyObj)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));

                                }   
                                    
                                                
                                    if((!empty($lastEscalationFieldObj)) && (!empty($lastEscalationReplyObj))){
                                        if($date2 > $date1){
                                            $lastEscalationFieldStatus[$val] = 1;
                                        }else{
                                          $lastEscalationFieldStatus[$val] = 0;  
                                        } 
                                    }else{
                                        $lastEscalationFieldStatus[$val] = 0;
                                    }

                                $escalationFieldArr[$val] = $field->label;
                                $escalationValueArr[$val] = $model->$val;
                               }

                           }

                           if(strtolower($field->data_field)=='date_of_last_visit_of_cluster_manager'){
                            if($model->$val != ""){
                                $date1 = strtotime($model->$val);
                                $date2 = strtotime($model->actual_date_of_audit);
                                $days = $this->diffdate($date1,$date2);
                                if($days>30){
                                    $lastEscalationFieldObj = DB::table('audit_escalations')
                                        ->select('field_type','id')  
                                        ->where('audit_inspection_id',$lastInsObjId->id)
                                        ->where('field_type',$field->data_field)
                                        ->first();

                                    if(!empty($lastEscalationFieldObj)){        
                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            //->where('field_type',$field->data_field)
                                            ->first();
                                
                                }

                                if(isset($lastEscalationReplyObj)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));

                                }   
                                    
                                                
                                    if((!empty($lastEscalationFieldObj)) && (!empty($lastEscalationReplyObj))){
                                        if($date2 > $date1){
                                            $lastEscalationFieldStatus[$val] = 1;
                                        }else{
                                          $lastEscalationFieldStatus[$val] = 0;  
                                        } 
                                    }else{
                                        $lastEscalationFieldStatus[$val] = 0;
                                    }
                                        
                                    
                                    $escalationFieldArr[$val] = $field->label;
                                    $escalationValueArr[$val] = $model->$val;        
                                }

                            }
                        } 

                        if(strtolower($field->data_field)=='observation_remark'){
                           if($model->$val!=''){
                                $lastEscalationFieldStatus[$val] = 0;
                                $escalationFieldArr[$val] = $field->label;
                                $escalationValueArr[$val] = $model->$val;        
                            }   
                        }

                        if(strtolower($field->data_field)=='last_transaction_date'){

                            if($model->$val != ""){

                                $date1 = strtotime($model->$val);
                                $date2 = strtotime($model->last_transaction_date_system);
                                $days = $this->diffdate($date2,$date1);
                                //echo $days;die;
                                if($days>6){
                                    $lastEscalationFieldObj = DB::table('audit_escalations')
                                        ->select('field_type','id')  
                                        ->where('audit_inspection_id',$lastInsObjId->id)
                                        ->where('field_type',$field->data_field)
                                        ->first();

                                    if(!empty($lastEscalationFieldObj)){        
                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            //->where('field_type',$field->data_field)
                                            ->first();
                                
                                }

                                if(isset($lastEscalationReplyObj)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));

                                }   
                                    
                                                
                                    if((!empty($lastEscalationFieldObj)) && (!empty($lastEscalationReplyObj))){
                                        if($date2 > $date1){
                                            $lastEscalationFieldStatus[$val] = 1;
                                        }else{
                                          $lastEscalationFieldStatus[$val] = 0;  
                                        } 
                                    }else{
                                        $lastEscalationFieldStatus[$val] = 0;
                                    }
                                        
                                    
                                    $escalationFieldArr[$val] = $field->label;
                                    $escalationValueArr[$val] = $model->$val;        
                                }

                            }
                        }

                           

                       }     

                    }
                } 

            }

           //Escalation queries for fields whose data is present in chamberwise_stock table
            foreach ($fieldsWithQuery as $field) {

                foreach($chamberwiseStock as $model){
                    $stackNo = $model->stack_number;
                    $client = $model->client_name;
                    $approxNoOfBags = $model->number_of_bags;
                    $currentStockBags = $model->current_stock_bags;
                    $chamberwiseStockId = $model->id;
                    if (array_key_exists($field->data_field,$model)) {
                        $val = $field->data_field;
                        
                        if(strtolower($field->data_field)=='borrower_name_matching'){
                            if ($model->$val == 'Stack Card Displayed and Not Match' || $model->$val == 'Stack Card Not Displayed') {
                                if(!empty($lastInsObjId)){
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('stack_no',$stackNo)
                                    ->where('client_name',$client)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                                //print_r($lastEscalationFieldObj);
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                                    //echo $date1."--".$date2;die;
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatus1[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatus1[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatus1[$val][] = 0;
                                }  

                            }else{
                                $lastEscalationFieldStatus1[$val][] = 0;   
                            }

                                $escalationClientNameArr1['client_name'][] = $client;
                                $escalationStackNoArr1['stack_no'][] = $stackNo;    
                                $escalationFieldArr1[$val][] = $field->label;
                                $escalationValueArr1[$val][] = $model->$val;

                            }   
                        } 

                        if(strtolower($field->data_field)=='bank_name_matching'){
                            if ($model->$val == 'No') {
                                if(!empty($lastInsObjId)){
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('stack_no',$stackNo)
                                    ->where('client_name',$client)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                               
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                                    
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatus1[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatus1[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatus1[$val][] = 0;
                                }
                            }else{
                                $lastEscalationFieldStatus1[$val][] = 0;   
                            }

                                $escalationClientNameArr1['client_name'][] = $client;
                                $escalationStackNoArr1['stack_no'][] = $stackNo;
                                $escalationFieldArr1[$val][] = $field->label;
                                $escalationValueArr1[$val][] = $model->$val;
                            }   
                        }

                        if(strtolower($field->data_field)=='stack_status_2'){
                            if ($model->$val == 'Uncountable Fallen' || $model->$val == 'Uncountable Under Fumigation' || $model->$val == 'Uncountable Wall Touch' || $model->$val == 'Uncountable Inward/Outward Running' || $model->$val == 'Countable Top Side And Wall Touch 4 Side' || $model->$val == 'Countable Wall Touch 2 Side' || $model->$val == 'Countable Wall Touch 3 Side'
                            || $model->$val == 'Countable Fallen' ) {
                                if(!empty($lastInsObjId)){
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('stack_no',$stackNo)
                                    ->where('client_name',$client)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                        
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatus1[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatus1[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatus1[$val][] = 0;
                                }
                            }else{
                                $lastEscalationFieldStatus1[$val][] = 0;   
                            }

                                $escalationClientNameArr1['client_name'][] = $client;
                                $escalationStackNoArr1['stack_no'][] = $stackNo;
                                $escalationFieldArr1[$val][] = $field->label;
                                $escalationValueArr1[$val][] = $model->$val;
                            }   
                        }

                        if(strtolower($field->data_field)=='stack_health_status'){
                            if ($model->$val == 'Infestation' || $model->$val == 'Atta Formation' ) {
                                if(!empty($lastInsObjId)){
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('stack_no',$stackNo)
                                    ->where('client_name',$client)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                        
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatus1[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatus1[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatus1[$val][] = 0;
                                }
                            }else{
                                $lastEscalationFieldStatus1[$val][] = 0;   
                            }   

                                $escalationClientNameArr1['client_name'][] = $client;
                                $escalationStackNoArr1['stack_no'][] = $stackNo;
                                $escalationFieldArr1[$val][] = $field->label;
                                $escalationValueArr1[$val][] = $model->$val;
                            }   
                        }


                    if(strtolower($field->data_field)=='stack_status_1'){

                            if($stackNo != '0'){
                                //For grain escalations 
                            if ($model->$val == 'Countable') {
                                $healthStatus = DB::table('health_status')->select('total')->where('chamberwise_stock_id',$chamberwiseStockId)->first();

                                $jsonTotal = 0;

                                $healthStatusTotalArr = json_decode($healthStatus->total);
                                foreach ($healthStatusTotalArr as $value) {
                                    $jsonTotal += $value;       
                                }
                                //To save overall system stock
                                $overall_system_stock_pwh += $currentStockBags;
                                $overall_entered_stock_pwh += $jsonTotal;

                                if($jsonTotal != $currentStockBags){
                                    if(!empty($lastInsObjId)){
                                    $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('stack_no',$stackNo)
                                    ->where('client_name',$client)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                                //print_r($lastEscalationReplyObj);die;
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                                   
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatus1[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatus1[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatus1[$val][] = 0;
                                }
                            }else{
                                $lastEscalationFieldStatus1[$val][] = 0;   
                            }

                                    $escalationClientNameArr1['client_name'][] = $client;
                                    $escalationStackNoArr1['stack_no'][] = $stackNo;
                                    $escalationFieldArr1[$val][] = 'Total bags';
                                    $escalationValueArr1[$val][] = $jsonTotal;   
                                }
                                    
                            }else if($model->$val == 'Uncountable'){
                                //To save overall system stock
                                $overall_system_stock_pwh += $currentStockBags;
                                $overall_entered_stock_pwh += $approxNoOfBags;

                                if($approxNoOfBags !=$currentStockBags){
                                    if(!empty($lastInsObjId)){
                                    $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('stack_no',$stackNo)
                                    ->where('client_name',$client)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                        
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                            
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatus1[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatus1[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatus1[$val][] = 0;
                                }
                            }else{
                                $lastEscalationFieldStatus1[$val][] = 0;   
                            }
                                    $escalationClientNameArr1['client_name'][] = $client;
                                    $escalationStackNoArr1['stack_no'][] = $stackNo;
                                    $escalationFieldArr1[$val][] = 'Total bags';
                                    $escalationValueArr1[$val][] = $approxNoOfBags;
                                }
                            } 

                            }else{
                            //For cotton escalations
                                $overall_system_stock_pwh += $currentStockBags;
                                $overall_entered_stock_pwh += $approxNoOfBags;

                                if($approxNoOfBags !=$currentStockBags){
                                    if(!empty($lastInsObjId)){
                                    $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('stack_no',$stackNo)
                                    ->where('client_name',$client)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                        
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                            
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatus1[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatus1[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatus1[$val][] = 0;
                                }
                            }else{
                                $lastEscalationFieldStatus1[$val][] = 0;   
                            }
                                    $escalationClientNameArr1['client_name'][] = $client;
                                    $escalationStackNoArr1['stack_no'][] = $stackNo;
                                    $escalationFieldArr1[$val][] = 'Total bags';
                                    $escalationValueArr1[$val][] = $approxNoOfBags;
                                }
                        }   
                        }                            


                    }

                }    
            }
            //print_r($escalationClientNameArr1);die;
            //echo "test";die;

            //Escalation queries for fields whose data is present in clientwise_stock table
            foreach ($fieldsWithQuery as $field) {

                foreach($clientwiseStock as $model){
                    $borrowerName = $model->borrower_name;
                    $client = $model->client_name;
                    $closingBag = $model->closing_bag;
                    $closing_qty = $model->closing_qty;
        
                    if (array_key_exists($field->data_field,$model)) {
                        $val = $field->data_field;
                        
                        if(strtolower($field->data_field)=='borrower_name_matching'){
                            if ($model->$val == 'Stack Card Displayed and Not Match' || $model->$val == 'Stack Card Not Displayed') {
                                if(!empty($lastInsObjId)){
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('bank_name',$client)
                                    ->where('client_name',$borrowerName)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                                
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                                    
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatusCm[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatusCm[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatusCm[$val][] = 0;
                                }
                            }else{
                                $lastEscalationFieldStatusCm[$val][] = 0;   
                            }

                                $escalationClientNameCmArr['bank_name'][] = $client;
                                $escalationBorrowerNameArr['client_name'][] = $borrowerName;    
                                $escalationFieldCmArr[$val][] = $field->label;
                                $escalationValueCmArr[$val][] = $model->$val;

                            }   
                        } 

                        if(strtolower($field->data_field)=='bank_name_matching'){
                            if ($model->$val == 'Pledge Card Not Displayed') {
                                if(!empty($lastInsObjId)){
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('bank_name',$client)
                                    ->where('client_name',$borrowerName)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                                
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));

                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatusCm[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatusCm[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatusCm[$val][] = 0;
                                }
                            }else{
                                $lastEscalationFieldStatusCm[$val][] = 0;   
                            }

                                $escalationClientNameCmArr['bank_name'][] = $client;
                                $escalationBorrowerNameArr['client_name'][] = $borrowerName;    
                                $escalationFieldCmArr[$val][] = $field->label;
                                $escalationValueCmArr[$val][] = $model->$val;
                            }   
                        }

                        if(strtolower($field->data_field)=='stack_status_2'){
                            if ($model->$val == 'Uncountable Fallen' || $model->$val == 'Uncountable Under Fumigation' || $model->$val == 'Uncountable Wall Touch' || $model->$val == 'Uncountable Inward/Outward Running' || $model->$val == 'Countable Top Side And Wall Touch 4 Side' || $model->$val == 'Countable Wall Touch 2 Side' || $model->$val == 'Countable Wall Touch 3 Side'
                            || $model->$val == 'Countable Fallen' ) {
                                if(!empty($lastInsObjId)){
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('bank_name',$client)
                                    ->where('client_name',$borrowerName)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                        
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                        
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatusCm[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatusCm[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatusCm[$val][] = 0;
                                }
                            }else{
                                $lastEscalationFieldStatusCm[$val][] = 0;   
                            }

                                $escalationClientNameCmArr['bank_name'][] = $client;
                                $escalationBorrowerNameArr['client_name'][] = $borrowerName;    
                                $escalationFieldCmArr[$val][] = $field->label;
                                $escalationValueCmArr[$val][] = $model->$val;
                            }   
                        }

                        if(strtolower($field->data_field)=='stack_health_status'){
                            if ($model->$val == 'Infestation' || $model->$val == 'Atta Formation' ) {
                                if(!empty($lastInsObjId)){
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('bank_name',$client)
                                    ->where('client_name',$borrowerName)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                        
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
                                
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatusCm[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatusCm[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatusCm[$val][] = 0;
                                }
                              }else{
                                $lastEscalationFieldStatusCm[$val][] = 0;   
                            }      

                                $escalationClientNameCmArr['bank_name'][] = $client;
                                $escalationBorrowerNameArr['client_name'][] = $borrowerName;    
                                $escalationFieldCmArr[$val][] = $field->label;
                                $escalationValueCmArr[$val][] = $model->$val;
                            }   
                        }


                        if(strtolower($field->data_field)=='number_of_bags'){
                            $overall_system_stock_cm +=$closingBag;
                            $overall_entered_stock_cm +=$model->$val;

                            if ($model->$val != $closingBag) {
                            if(!empty($lastInsObjId)){
                                $lastEscalationFieldObj = DB::table('audit_escalations')
                                    ->select('field_type','id')  
                                    ->where('audit_inspection_id',$lastInsObjId->id)
                                    ->where('field_type',$field->data_field)
                                    ->where('bank_name',$client)
                                    ->where('client_name',$borrowerName)
                                    ->first();

                                $lastEscalationReplyObj=(object)array();    
                                if(count($lastEscalationFieldObj)>0){

                                    $lastEscalationReplyObj = DB::table('audit_logs')
                                            ->select('till_date')  
                                            ->where('audit_inspection_id',$lastInsObjId->id)
                                            ->where('audit_escalation_id',$lastEscalationFieldObj->id)
                                            ->first();            
                                }  
                                
                                if(!empty($lastEscalationReplyObj->till_date)){
                                    $date1 = strtotime($lastEscalationReplyObj->till_date);
                                    $date2 = strtotime(date('Y-m-d'));
        
                                    if($date2 >= $date1){
                                        $lastEscalationFieldStatusCm[$val][] = 1;
                                    }else{
                                      $lastEscalationFieldStatusCm[$val][] = 0;  
                                    }
                                    
                                }else{
                                    $lastEscalationFieldStatusCm[$val][] = 0;
                                }
                              }else{
                                $lastEscalationFieldStatusCm[$val][] = 0;   
                            }      

                                $escalationClientNameCmArr['bank_name'][] = $client;
                                $escalationBorrowerNameArr['client_name'][] = $borrowerName;    
                                $escalationFieldCmArr[$val][] = $field->label;
                                $escalationValueCmArr[$val][] = $model->$val;
                            
                        }
                    }        


                    }

                }    
            }


            
        }
        //echo "test";die;
        //echo "<pre>";
        //print_r($escalationClientNameCmArr);
        //print_r($escalationBorrowerNameArr);
        //print_r($escalationFieldCmArr);
        //print_r($escalationValueCmArr);die;

        
        //Updating overall system stock and overall entered stock in audit_inspections table
        $auditinspection = AuditInspection::find($id);
        $overall_entered_stock_pwh = $overall_entered_stock_pwh + $auditinspection->unrecorded_stock; 

        $affectedRows = $auditinspection->update(['overall_system_stock_pwh' => $overall_system_stock_pwh,'overall_entered_stock_pwh'=>$overall_entered_stock_pwh,'overall_system_stock_cm' => $overall_system_stock_cm,'overall_entered_stock_cm'=>$overall_entered_stock_cm]);        
        //die;

        //$auditinspection = AuditInspection::find($id);
        //$affectedRows = $auditinspection->update(['approved_status' => 2]);

        $escalationData = array();

        $checker_id = Auth::user()->id;

        //$level1 = User::whereHas("roles", function($q){ $q->where("name", "level 1"); })->first();

        $areaManagerName = DB::table('audit_inspections')
                            ->select('name_of_last_visit_area_manager')
                            ->where('id',$id)
                            ->first();

        $level1 = User::where("name",$areaManagerName->name_of_last_visit_area_manager)->first();                    

       //print_r($level1->email);die;

        
        if (count($escalationFieldArr) > 0) {
    
            foreach ($escalationFieldArr as $key => $value) {
                $escalationData['audit_inspection_id'] = $id;
                $escalationData['stack_no'] = NULL;
                $escalationData['client_name'] = NULL;
                $escalationData['field_type'] = $key;
                $escalationData['description'] = $value;
                $escalationData['escalation_value'] = $escalationValueArr[$key];
                $escalationData['checker_id'] = $checker_id;
                $escalationData['level1_id'] = $level1->id;
                $escalationData['status'] = '0';
                $escalationData['dishonour_status'] = $lastEscalationFieldStatus[$key];
                $escalationData['parent_id'] = 0;
                $escalationData['created_at'] = date("Y-m-d H:i:s");

                DB::table('audit_escalations')->insert($escalationData);
            }


        }

        if (count($escalationFieldArr1) > 0) {
            $j=0;
            foreach ($escalationFieldArr1 as $index=>$efa) {
                $i=0;
                foreach ($efa as $key => $value) {
                    
                    $escalationData['audit_inspection_id'] = $id;
                    $escalationData['stack_no'] = $escalationStackNoArr1['stack_no'][$j];
                    $escalationData['client_name'] = $escalationClientNameArr1['client_name'][$j];
                    $escalationData['field_type'] = $index;
                    $escalationData['description'] = $value;
                    $escalationData['escalation_value'] = $escalationValueArr1[$index][$i];
                    $escalationData['checker_id'] = $checker_id;
                    $escalationData['level1_id'] = $level1->id;
                    $escalationData['status'] = '0';
                    $escalationData['dishonour_status'] = $lastEscalationFieldStatus1[$index][$i];
                    $escalationData['parent_id'] = 0;
                    $escalationData['created_at'] = date("Y-m-d H:i:s");

                    DB::table('audit_escalations')->insert($escalationData);
                     $i++;
                     $j++;
                }
                
                             
            }

        }

        //Escalations for CM Part
        if (count($escalationFieldCmArr) > 0) {
           foreach ($escalationFieldCmArr as $index=>$efa) {
                $i=0;
                foreach ($efa as $key => $value) {
                    //echo "<pre>";
                    //print_r($index);
                    //print_r($value);
                    //print_r($escalationBorrowerNameArr);die;
                    $escalationData['audit_inspection_id'] = $id;
                    $escalationData['bank_name'] = $escalationClientNameCmArr['bank_name'][$i];
                    $escalationData['client_name'] = $escalationBorrowerNameArr['client_name'][$i]; 
                    $escalationData['stack_no'] = NULL;
                    $escalationData['type'] = 'CM';
                    $escalationData['field_type'] = $index;
                    $escalationData['description'] = $value;
                    $escalationData['escalation_value'] = $escalationValueCmArr[$index][$i];
                    $escalationData['checker_id'] = $checker_id;
                    $escalationData['level1_id'] = $level1->id;
                    $escalationData['status'] = '0';
                    $escalationData['dishonour_status'] = $lastEscalationFieldStatusCm[$index][$i];
                    $escalationData['parent_id'] = 0;
                    $escalationData['created_at'] = date("Y-m-d H:i:s");

                    DB::table('audit_escalations')->insert($escalationData);
                     $i++;
                }                
                             
            }

        }
        //die;
        

        
        
        $totalEsacaltions = DB::table('audit_escalations')->where('audit_inspection_id',$id)->get();

        $auditinspection = AuditInspection::find($id);
        if(!$totalEsacaltions->isEmpty()){
            $affectedRows = $auditinspection->update(['approved_status' => 2]);
        //$email = getUserDetails($totalEsacaltions[0]->level1_id,'email');
        //$name = getUserDetails($totalEsacaltions[0]->level1_id,'name');

        $email = $this->getUserDetails($totalEsacaltions[0]->level1_id,'email');
        $name  = $this->getUserDetails($totalEsacaltions[0]->level1_id,'name');

        $warehouse_detail = AuditInspection::where('id',$totalEsacaltions[0]->audit_inspection_id)->first();
        // dd($name);
        $maildata = ['Name' => $name,'totalEsacaltions'=>$totalEsacaltions,'warehouse_detail'=>$warehouse_detail];


         //$email = 'khushiddin@aryacma.co.in';


        $aa =  Mail::send('mail',$maildata,function($message) use($maildata,$email) {

             $message->from('info@aryacma.co.in');
            $message->to($email);
            $message->subject("Internal Audit");
         
         });
        }else{
            $affectedRows = $auditinspection->update(['approved_status' => 4]);
        }



        if($affectedRows>0){
            echo json_encode(array('value' => 'Success'));
            die;
        }else{
            echo json_encode(array('value' => 'Error'));
            die;
        }
    }

    

    public function reject(Request $request){
        if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }

        $id = $request->get('id');
        $role = Auth::user()->roles->pluck('name')->first();
        
        $auditinspection = AuditInspection::find($id);
        $affectedRows = $auditinspection->update(['approved_status' => 3]);
        if($affectedRows>0){

            echo json_encode(array('value' => 'Success'));
            die;
        }else{
            echo json_encode(array('value' => 'Error'));
            die;
        }
    }

    public function addcomment(Request $request){

       $audit_inspection_id = $request->get('audit_inspection_id');
       $audit_escalation_id = $request->get('audit_escalation_id');
       $comment = $request->get('comment');
       $till_date = $request->get('till_date');
       $till_date = ($till_date !='') ? date('Y-m-d', strtotime($request->get('till_date'))):NULL;
       $level_id = Auth::user()->id;

       $data = [
            'audit_inspection_id'       =>      $audit_inspection_id,
            'audit_escalation_id'       =>      $audit_escalation_id,
            'comment'                   =>      $comment,
            'level_id'                  =>      $level_id,
            'till_date'                 =>      $till_date
       ];

       $insert_id = DB::table('audit_logs')->insertGetId($data);
       if(isset($insert_id)){
        $role = Auth::user()->roles->pluck('name')->first();
       $escData = array();
        if (!in_array($role, array('level 2', 'level 3', 'level 4'))) {
            if ($role == 'checker') {
                $escData['status'] = '3';
            }
            if ($role == 'level 1') {
                $escData['status'] = '2';
            }
            if ($role == 'cluster head') {
                $escData['status'] = '4';
            } 
        }

        //print_r($escData);die;

        $affectedRows = DB::table('audit_escalations')->where('id',$audit_escalation_id)->update($escData);

        if($affectedRows>0){
            echo json_encode(array('value' => 'Success'));
            die;
        }else{
            echo json_encode(array('value' => 'Error'));
            die;
        }
           
       }
       


    }


    public function check(Request $request) {
        $userId = Auth::user()->id;
            //print_r($request->toArray());die;
            $postData = $request;
            $model = DB::table('audit_escalations')->where('id',$postData->id)->first();
            
        
            $postData = $postData->toArray();
            
            unset($postData['_token']);
            //unset($postData['escalation_id']);
            $role = Auth::user()->roles->pluck('name')->first();
       
            if ($role == 'checker') {
                $postData['updated_at'] = date("Y-m-d H:i:s");
                $postData['status'] = '1';
            }else{
                $postData['updated_at'] = date("Y-m-d H:i:s");
                $postData['status'] = '2';
            }
             
            //print_r($postData);die;

            $affectedRows = DB::table('audit_escalations')->where('id',$postData['id'])->update($postData);
            
                if ($affectedRows > 0) {
                    
                    $finalQueryArr = DB::table('audit_escalations')
                    ->where('status','!=','1')
                    ->where('audit_inspection_id',$model->audit_inspection_id)
                    ->get()
                    ->toArray();

                    if (count($finalQueryArr) == '0') {
                        $inspModel = AuditInspection::where('id',$model->audit_inspection_id)->first();
                        $inspData['approved_status'] = 4;
    
                        $inspModel->update($inspData);
                        
                    } 
                    /* END */
                    echo json_encode(array('value' => 'Success'));
                    die;
                } else {
                    echo json_encode(array('value' => 'Error'));
                    die;
                }
            
        
    }


    public function escalationfield(Request $request) {
       //print_r($request->toArray());die;
        $userId = Auth::user()->id;
    
        $escId = $request->get('id');

        $client = $request->get('client');

        $auditEscalation = DB::table('audit_escalations')->select('audit_escalations.audit_inspection_id','field_mappers.label','field_mappers.data_field','field_mappers.data_type')
        ->join('field_mappers','audit_escalations.field_type','=','field_mappers.data_field')
        ->where('audit_escalations.id',$escId)
        ->get()
        ->toArray();

        //print_r($auditEscalation);die;
             
    
            $fieldDataArray = array();
            if (count($auditEscalation) > 0) {
                foreach ($auditEscalation as $auditEsc) {
                    
                    $fieldDataArray['field_type'] = $auditEsc->data_type;

                    $columns = array($auditEsc->data_field);

                    $columnNames = ['stack_status_1','stack_status_2','stack_health_status','borrower_name_matching','bank_name_matching'];
                    $columnNames1 = ['total'];
                    
                    $auditInspection=array();
                    if(!in_array($auditEsc->data_field, $columnNames)){
                    $auditInspection = AuditInspection::select($columns)
                                    ->where("id",$auditEsc->audit_inspection_id)
                                    ->get()->toArray();
                    }else{                
                    $chamberwiseStock = DB::table('chamberwise_stock')->select($columns)
                                    ->where(["inspection_id"=>$auditEsc->audit_inspection_id,'client_name'=>$client])
                                    ->get()->toArray();
                    }     
                    //print_r($chamberwiseStock);die;           
                    if (count($auditInspection) > 0) {
                        foreach ($auditInspection as $k=>$ai) {
                                print_r($ai);die;
                                $fieldDataArray[0]['label'] = $auditEsc->label;
                                $fieldDataArray[0]['field_id'] = $auditEsc->data_field;
                                $fieldDataArray[0]['field_value'] = $ai[$auditEsc->data_field];
                            }
                     
                    }else if (count($chamberwiseStock) > 0) {
                        foreach ($chamberwiseStock as $i=>$cs) {
                                //$dataField = $auditEsc->data_field;
                                $cs = (array)$cs;           
                                $fieldDataArray[0]['label'] = $auditEsc->label;
                                $fieldDataArray[0]['field_id'] = $auditEsc->data_field;
                                $fieldDataArray[0]['field_value'] = $cs[$auditEsc->data_field];
                            }
                     
                    }
                        echo json_encode($fieldDataArray);
                    exit;
                }
            } else {
                echo json_encode(array('value' => 'Error'));
                exit;
            }

    }

    function diffdate($startDate,$endDate){
        $timeDiff = $endDate-$startDate;
        $numberDays = round($timeDiff / 86400);
        return $numberDays;
    }

//Save pv using ajax
    public function savepv(Request $request) {
       //print_r($request->toArray());die;
    
        $chamberwiseStockId = $request->get('chamberwise_stock_id');
             
        $stackNo = json_encode($request['stackNo']);
        
        $noOfBlock = json_encode($request['noOfBlock']);
        $danda = json_encode($request['danda']);
        $patti = json_encode($request['patti']);
        $d = json_encode($request['d']);
        $height = json_encode($request['height']);
        $bagsF = json_encode($request['bagsF']);
        $bagsG = json_encode($request['bagsG']);
        $total = json_encode($request['total']);

        $dataArr = [
            'stack_no'              =>      $stackNo,
            'no_of_block'           =>      $noOfBlock,
            'danda'                 =>      $danda,
            'patti'                 =>      $patti,
            'danda_plus_patti'      =>      $d,
            'height'                =>      $height,
            'plus_bags'             =>      $bagsF,
            'minus_bags'            =>      $bagsG,
            'total'                 =>      $total

        ];
        //echo "<pre>";
        //echo $chamberwiseStockId;
        //print_r($dataArr);die;
        $affected = DB::table('health_status')->where(['chamberwise_stock_id'=>$chamberwiseStockId])->update($dataArr);

            if ($affected) {
                echo json_encode(array('value' => 'Success'));
                exit;
            } else {
                echo json_encode(array('value' => 'Error'));
                exit;
            }

    }


    public function uploadauditvideo($id){
        if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }        
        
        return view('auditinspection/uploadauditvideo',compact('id'));
    }

    public function saveauditvideo(Request $request){
        if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }        
        
        $files = request()->file('audit_videos');
        $inspectionId=$request->get('inspection_id');
        $videoPathArr = array();
        foreach ($files as $key => $file) {
            $videoName = 'ia.aryacma.co.in2/Videos/'.$inspectionId.'/'.$file->getClientOriginalName();

            Storage::disk('s3')->put($videoName, file_get_contents($file));
            Storage::disk('s3')->setVisibility($videoName, 'public');

            $url = Storage::disk('s3')->url($videoName);
            $videoPathArr[] = $url;

        }
        //print_r($videoPathArr);die;

        $dataArr = [
            'inspection_id'     =>      $inspectionId,
            "video_1"           =>      isset($videoPathArr[0]) ? $videoPathArr[0] : '',
            "video_2"           =>      isset($videoPathArr[1]) ? $videoPathArr[1] : '',
            "video_3"           =>      isset($videoPathArr[2]) ? $videoPathArr[2] : '',
            "video_4"           =>      isset($videoPathArr[3]) ? $videoPathArr[3] : '',
            "video_5"           =>      isset($videoPathArr[4]) ? $videoPathArr[4] : '',
        ];
        
        $insertId = DB::table('audit_videos')->insertGetId($dataArr);

        if($insertId){
            $auditinspection = AuditInspection::find($inspectionId);
            $affectedRows = $auditinspection->update(['video_flag' => 1]);
        }    

        return redirect()->route('auditinspection.index');
    }

    public function editauditvideo($id){
        if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }

        $auditvideos = DB::table('audit_videos')->where('inspection_id',$id)->first();   
        
        return view('auditinspection/uploadauditvideo',compact('auditvideos','id'));
    }

    public function updateauditvideo(Request $request,$id){
        if (! Gate::allows('inspection_manage')) {
            return abort(401);
        }

        $files = request()->file('audit_videos');
        $inspectionId = $request->get('inspection_id');
        //print_r($files);die;
        
        $videoPathArr = array();
        foreach ($files as $key => $file) {
            $videoName = 'ia.aryacma.co.in2/Videos/'.$inspectionId.'/'.$file->getClientOriginalName();

            Storage::disk('s3')->put($videoName, file_get_contents($file));
            Storage::disk('s3')->setVisibility($videoName, 'public');

            $url = Storage::disk('s3')->url($videoName);
            $videoPathArr[$key] = $url;

        }


        $savedVideos = DB::table('audit_videos')->where('id',$id)->first();



        $video_1 = isset($videoPathArr[0]) ? $videoPathArr[0] : (isset($savedVideos->video_1) ? $savedVideos->video_1 : '');
        $video_2 = isset($videoPathArr[1]) ? $videoPathArr[1] : (isset($savedVideos->video_2) ? $savedVideos->video_2 : '');
        $video_3 = isset($videoPathArr[2]) ? $videoPathArr[2] : (isset($savedVideos->video_3) ? $savedVideos->video_3 : '');
        $video_4 = isset($videoPathArr[3]) ? $videoPathArr[3] : (isset($savedVideos->video_4) ? $savedVideos->video_4 : '');
        $video_5 = isset($videoPathArr[4]) ? $videoPathArr[4] : (isset($savedVideos->video_5) ? $savedVideos->video_5 : '');

        //print_r($videoPathArr);die;

        $dataArr = [
            "video_1"           =>      $video_1,
            "video_2"           =>      $video_2,
            "video_3"           =>      $video_3,
            "video_4"           =>      $video_4,
            "video_5"           =>      $video_5,
        ];

        $status = DB::table('audit_videos')->where('id',$id)->update($dataArr);
        return redirect()->route('auditinspection.index');

    }

    public function clusterheads(Request $request)
    { 
        $clusterHeads = User::whereHas("roles", function($q){ $q->where("name", "cluster head"); })->where('parent_id',$request->get('amId'))->get();

        //print_r($clusterHeads);die;
        
        return response()->json($clusterHeads, 200);
        
    }


            


    
}
