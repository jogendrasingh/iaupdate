<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;

class AjaxController extends Controller
{
   public function state()
    { 
        $states = State::select('id','state')->get();
        return response()->json($states, 200);
        
    }
}
