<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use App\AuditInspection;

class ImportExcelController extends Controller
{
    public function index(){
        if (! Gate::allows('mis_manage')) {
            return abort(401);
        }

    	$misdata = DB::table('misexceldetails')
    	->select('id','imported_count','skipped_count','mis_excel_name','created_at')
        ->orderBy('id','desc')
    	->get();
    	return view('importexcel/index',compact('misdata'));
    }

    public function uploadmis(){
        if (! Gate::allows('mis_manage')) {
            return abort(401);
        }
    	return view('importexcel/upload_mis');
    }

    public function savemis(Request $request){
        if (! Gate::allows('mis_manage')) {
            return abort(401);
        }
    	$this->validate($request, [
		      'misexcel'  => 'required'
		     ]);
    		$extensions = array("xls","xlsx","csv");
    		$ext = $request->file('misexcel')->getClientOriginalExtension();

		    if(in_array($ext,$extensions)){

		    $path = $request->file('misexcel')->getRealPath();

		    $data = Excel::load($path)->get();

		    
		    //echo "<pre>";
		    //print_r($data);die;

		     if($data->count() > 0)
		     {
		        $excel = $request->file('misexcel');
		        $name = $excel->getClientOriginalName();
		        $nameWithoutExtension = explode(".", $name)[0];
		        $dateTime= date('d-m-Y-H-m-s');
		        $destinationPath = public_path('/storage/misexcel/');
		        $excel->move($destinationPath, $dateTime."_".$name);

		        $excel_details =[
		        	'mis_excel_name'		=>  $dateTime."_".$name
		        ];

		        $insert_id = DB::table('misexceldetails')->insertGetId($excel_details);

		        $response = $this->processdata($insert_id);
		        //dd($response);
		        
				$skippedPath = public_path('/storage/misexcel/skipped');
				$importedPath = public_path('/storage/misexcel/imported');

		        if(is_array($response)){
                    $importedCount = count($response['completed']);
                    $skippedCount  = count($response['skipped']);
                    $reason        = $response['reason'];
                    
                    $updateData['imported_count']  = $importedCount;
                    $updateData['skipped_count']   = $skippedCount;
                    //dd($updateData);
                    //$post['reason']         = $reason;
                    DB::table('misexceldetails')->where('id',$insert_id)->update($updateData);

                    /* EXCEL WRITE */
                    if(count($response['skipped'])>0){    
                    	$skippedData = $response['skipped'];
                    	Excel::create($dateTime."_".$nameWithoutExtension, function($excel) use ($skippedData) {
				            $excel->sheet('sheet1', function($sheet) use ($skippedData)
				            {
				                $sheet->fromArray($skippedData);
				            });
				        })->store($ext, $skippedPath);


                    }
                    if(count($response['completed'])>0){    
                        $importedData = $response['completed'];
                    	Excel::create($dateTime."_".$nameWithoutExtension, function($excel) use ($importedData) {
				            $excel->sheet('sheet1', function($sheet) use ($importedData)
				            {
				                $sheet->fromArray($importedData);
				            });
				        })->store($ext, $importedPath);
                    }
                

               }
		      
		      
		     }
		 	}else{
		 		return back()->with('error', 'Please select valid file.');	
		 	}
		     
		    return back()->with('success', 'Excel Data Imported successfully.');
    		//return redirect()->route('importexcel.savemis');
    }

	public function viewmisdata($id){
        if (! Gate::allows('mis_manage')) {
            return abort(401);
        }
    	$misdatadetails = DB::table('misdata')->select('*')->where('mis_excel_id','=',$id)->get();
    	return view('importexcel/viewmisdata',compact('misdatadetails'));
    }

    public function editmisdata($id){
		if (! Gate::allows('mis_manage')) {
            return abort(401);
        }
    	$misdatadetails = DB::table('misdata')->select('*')->find($id);
    	//dd($misdatadetails);
    	return view('importexcel/editmisdata',compact('misdatadetails'));
    }


    public function saveupdatedmisdata(Request $request, $id){
        if (! Gate::allows('mis_manage')) {
            return abort(401);
        }
   		$mis_excel_id = $request->get('mis_excel_id');
   		$updateddata  = $request->except('_token');
    	DB::table('misdata')->where('id',$id)->update($updateddata);
    	return redirect()->route('importexcel.viewmisdata',[$mis_excel_id]);
    }


public function processdata($excelId){		
	$path = public_path('/storage/misexcel/');
	$excelFile = DB::table('misexceldetails')->find($excelId);
	$excelFileLocated = $path.$excelFile->mis_excel_name;
	$excelData = Excel::load($excelFileLocated)->get();
	$excelDataArr = $excelData->toArray();
	
	$skipped =	array();
	$imported = array();

	$done = 0;
	$i = 0;
	$msg = "";

	$totalRecords = count($excelDataArr);
	//dd($totalRecords);
	$completed =array();
    $skipped = array();

	foreach ($excelDataArr as $row) {
		 $skip = "0"; 
		 $score=0;
         $error = "";
         $saveValue = array();

         foreach ($row as $key => $value) {
         	//error messages
         	if($value == '' && $key!='lock_key_control'){
         		$skip = 1;
                $error .= $key . " Cannot be blank, ";
         	}else if($key == "warehouse_start_date") {
                    if($value!=''){    
                    $retvalue = $this->validateDate($value);
                    if($retvalue==false){
                        $skip = 1;
                        $error .= '=>'.$key . " date format is not valid, ";
                       	echo $value." - ".$error."<br>";  
                    }
                }
            }

            //score calculation
            if($key == "commodity_value_per_mt"){
                	if($value!='' && $value<50000){ $score += 0; $a=0;}
                	else{ $score += 5; $a=5;}
                }
                if($key=='takeover_case_agencies'){
                    if($value=='Yes'){ $score += 10; $b=10;}
                    else if($value=='No'){ $score += 0; $b=0;}
                }
                if($key=='client_type'){
                    if($value=='ICICI'){ $score += -5; $c=-5;}
                    else if($value=='Other'){ $score += 3; $c=3;}
                    else if($value=='PWH Corp'){ $score += 0; $c=0;}
                    else if($value=='PWH SME'){ $score += 3; $c=3;}
                    else if($value=='Retail'){ $score += 5; $c=5;}
                    else{ $score +=0; $c=0;}
                }if($key=='aum_in_warehouse'){
                    if($value<2){ $score += -3; $d=-3;}
                    else if($value>=2 && $value<4){ $score += 0; $d=0;}
                    else if($value>=4 && $value<6){ $score += 5; $d=5;}
                    else if($value>=6){ $score += 10; $d=10;}
                }
                if($key=='covered_last_month'){
                    if($value=='Yes'){ $score += -5;  $e=-5;}
                    else if($value=='No'){ $score += 5; $e=5;}
                }
                if($key=='price_volatility'){
                    if($value=='Volatile Commodity'){ $score += 5; $f=5;}
                    else if($value=='Stable Commodity'){ $score += 0; $f=0;}
                }
                if($key=='intermittent_variance'){
                    if($value=='Manually'){ $score += 5; $g=5;}
                    else{ $score += 0; $g=0;}
                 }

                 if($key=='name_of_warehouse'){
                    $value = utf8_encode($value);
                }
                
                $saveValue[$key] = $value;
        
         	}

        $score = $a+$b+$c+$d+$e+$f+$g;
        //echo $score;die;

        if ($skip != 1) {
            /* SAVE DATA */
            if (count($saveValue) > 0) {
       			$saveValue['mis_excel_id'] = $excelId;
                $saveValue['score'] = $score;
                DB::table('misdata')->insert($saveValue);

                $completed[] =  $saveValue;
                $msg = "completed";

                }
        	} else {
                $saveValue['reason'] = $error;    
                $skipped[] = $saveValue;
                $msg = $error;
        }
    }  

    $response = array('completed' => $completed, 'skipped' => $skipped, 'reason' => $msg);
 
    return $response;


}

//return true if date is in 'Y-m-d' format else false
function validateDate($date)
{
	$date = date('Y-m-d',strtotime($date));
    if (false === strtotime($date)) { 
        return false;
    } 
    list($year, $month, $day) = explode('-', $date); 
    return checkdate($month, $day, $year);

}


function dtrdownload(){

    $currentStock = DB::connection('mysql2')->table('dtr_grain')->select("dtr_grain.state_id","dtr_grain.warehouse_id","warehouse.warehouse_code", DB::raw('round(SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_grain.cargo_gross_wt,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_grain.cargo_gross_wt,0))),3) as qty'),DB::raw('SUM(COALESCE(IF(dtr_audit.form_type="inward",dtr_grain.no_of_bags,0)))-SUM(COALESCE(IF(dtr_audit.form_type="outward",dtr_grain.no_of_bags,0))) as bags'),"dtr_grain.commodity","warehouse.name","client.entity_name","category.category_name as state","sub.category_name as location","client.id as client_id","dtr_commodity_name.commodity_name","warehouse.whType")
        ->join('dtr_audit','dtr_grain.dtr_audit_id','=','dtr_audit.id')
        ->leftJoin('warehouse','dtr_grain.warehouse_id','=','warehouse.id')
        ->leftJoin('client','dtr_grain.client_id','=','client.id')
        ->leftJoin("category","category.id",'=','dtr_grain.state_id')
        ->leftJoin("category as sub","sub.id","=","warehouse.address")
        ->leftJoin("dtr_commodity_name","dtr_grain.commodity","=","dtr_commodity_name.id")
        ->where('dtr_audit.approved','2')
        ->where('warehouse.active','1')
        ->groupBy('dtr_grain.client_id', 'dtr_grain.warehouse_id')
        //->having("qty>1 AND bags>1")
        ->orderBy("dtr_grain.state_id","dtr_grain.warehouse_id")
        ->get()->toArray();

       $fcurrentStock = array();
        
        if(count($currentStock)>0){
            foreach($currentStock as $k=>$val){
                $fcurrentStock[$k]['warehouse_code'] = $val->warehouse_code;
                $fcurrentStock[$k]['name'] = $val->name;
                $fcurrentStock[$k]['client_id'] = $val->client_id;
                $fcurrentStock[$k]['entity_name'] = $val->entity_name;
                $fcurrentStock[$k]['state'] = $val->state;
                $fcurrentStock[$k]['location'] = $val->location;
                $fcurrentStock[$k]['commodity'] = $val->commodity_name;
                $fcurrentStock[$k]['qty'] = $val->qty;
                $fcurrentStock[$k]['bags'] = $val->bags;
            }
        }
      Excel::create('fcurrentStock', function($excel) use($fcurrentStock) {
          $excel->sheet('ExportFile', function($sheet) use($fcurrentStock) {
              $sheet->fromArray($fcurrentStock);
          });
      })->export('xls');    


}

function cmdownload(){

        $currentStock = DB::connection('mysql3')->table('tbl_srs as BaseTbl')->select("S.name as state","Loc.name as location", "BaseTbl.id", "BaseTbl.storage_receipt_no",  "B.borrowerName", "W.name as warehouse","W.warehouse_code", "C.client_name", "Com.commodity", "CV.commodity_variety", "BaseTbl.total_bags", "BaseTbl.net_weight", "BaseTbl.release_bag", "BaseTbl.release_qty","BaseTbl.closing_bag", "BaseTbl.closing_qty", "BaseTbl.market_rate", "BaseTbl.value_of_commodity", "BaseTbl.createdDtm", "B.borrower_code")
        ->leftJoin('tbl_warehouses as W','BaseTbl.warehouse_id','=','W.id')
        ->leftJoin('tbl_clients as C','BaseTbl.client_id','=','C.id')
        ->leftJoin("tbl_borrowers as B","BaseTbl.borrower_id",'=','B.id')
        ->leftJoin("tbl_states as S","BaseTbl.state_id","=","S.id")
        ->leftJoin("tbl_states as Loc","BaseTbl.location_id","=","Loc.id")
        ->leftJoin("tbl_commodity as Com","BaseTbl.commodity_id","=","Com.id")
        ->leftJoin("tbl_commodity_variety as CV","BaseTbl.commodity_variety","=","CV.id")
        ->where('BaseTbl.isDeleted',0)
        ->where('BaseTbl.approved',2)
        ->where('BaseTbl.closing_bag',">",0)
        ->orderBy("BaseTbl.id","DESC")
        ->get()->toArray();

        $fcurrentStock = array();
        
        if(count($currentStock)>0){
            foreach($currentStock as $k=>$val){
                $fcurrentStock[$k]['state'] =       $val->state;
                $fcurrentStock[$k]['location'] =    $val->location;
                $fcurrentStock[$k]['warehouse'] =   $val->warehouse;
                $fcurrentStock[$k]['warehouse_code'] = $val->warehouse_code;
                $fcurrentStock[$k]['client_name'] = $val->client_name;
                $fcurrentStock[$k]['borrowerName'] = $val->borrowerName;
                $fcurrentStock[$k]['borrowerName'] = $val->borrowerName;
                $fcurrentStock[$k]['commodity'] = $val->commodity;
                $fcurrentStock[$k]['commodity_variety'] = $val->commodity_variety;
                $fcurrentStock[$k]['sr'] = $val->storage_receipt_no;
                $fcurrentStock[$k]['total_bags'] = $val->total_bags;
                $fcurrentStock[$k]['net_wt'] = $val->net_weight;
                $fcurrentStock[$k]['closing_bag'] = $val->closing_bag;
                $fcurrentStock[$k]['closing_qty'] = $val->closing_qty;
                $fcurrentStock[$k]['market_rate'] = $val->market_rate;
                $fcurrentStock[$k]['create Date'] = date('d-M-Y',strtotime($val->createdDtm));
                $fcurrentStock[$k]['Client Code'] = $val->borrower_code;
            }
        }
      Excel::create('fcurrentStock', function($excel) use($fcurrentStock) {
          $excel->sheet('ExportFile', function($sheet) use($fcurrentStock) {
              $sheet->fromArray($fcurrentStock);
          });
      })->export('xls');  

        


}


public function viewallassignedwarehouses(){
        if (! Gate::allows('mis_manage')) {
            return abort(401);
        }

        $from=date("Y-m-01"); 
        $to = date("Y-m-t");
        
        $misdatadetails = DB::table('misdata')
                            ->select('*')
                            ->where('created_at','>=',$from)
                            ->where('created_at','<=',date('Y-m-d', strtotime($to . ' +1 day')))
                            ->get();
        return view('importexcel/viewallassignedwarehouses',compact('misdatadetails'));
    }


public function updateusingcsv(){      
    $path = public_path('/storage/csvfile/'.'clusterHead.csv');

    $csvData = Excel::load($path)->get();
    $csvDataArr = $csvData->toArray();
    
    echo "<pre>";
    print_r($csvDataArr);die;
    $i=0;
    foreach ($csvDataArr as $row) {
        $auditinspection = AuditInspection::find($row['id']);
        $affectedRows = $auditinspection->update(['name_of_last_visit_cluster_manager' => $row['cluster_head']]);

        if($affectedRows){
            $i++;
        }
    }  
    echo $i;
    exit;

}



}
