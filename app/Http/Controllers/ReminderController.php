<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\AuditInspection;
use Auth;
use App\User;
use Mail;
use App\Http\Controllers\Traits\HelperTrait;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
class ReminderController extends Controller
{
	//use HelperTrait;
    use HasRoles;
	public function sendescalations(){
		$currentDate = date('Y-m-d');
        $auditEscalationArr = DB::table('audit_inspections')
        					->select('audit_inspections.id','audit_inspections.state','audit_inspections.warehouse_code','audit_inspections.name_of_warehouse',DB::raw('count(audit_escalations.id) as TotalEscalation'),DB::raw('max(date(audit_inspections.updated_at)) as createDate'),'audit_inspections.name_of_last_visit_area_manager as level1_name','audit_inspections.type_of_warehouse','audit_escalations.level1_id')
                            ->rightJoin('audit_escalations','audit_escalations.audit_inspection_id','=','audit_inspections.id')
                            //->leftJoin('users','users.id','=','audit_escalations.level1_id')
                            ->where('audit_inspections.approved_status',2)
                            //->whereIn('audit_escalations.status',['0','3'])
                            ->where('audit_escalations.status','0')
                            ->groupBy('audit_inspections.type_of_warehouse','audit_inspections.warehouse_code')
                            ->orderBy('audit_inspections.id','desc')
     	                    //->whereRaw('DATEDIFF("'.date("Y-m-d").'",created_at)>2')
                            ->get()
                            ->toArray();
        //echo "<pre>";                    
        //print_r($auditEscalationArr);die;

                            

        if(count($auditEscalationArr)>0){
        $counter = 0;
            foreach($auditEscalationArr as $levelData){

                
                if($levelData->TotalEscalation > 0){
                    $level1Date = date('Y-m-d', strtotime($levelData->createDate . ' +1 Weekday'));

                    $level2Date = date('Y-m-d', strtotime($levelData->createDate . ' +2 Weekday'));
                    
                    $level3Date = date('Y-m-d', strtotime($levelData->createDate . ' +4 Weekday'));
                    
                    $level4Date = date('Y-m-d', strtotime($levelData->createDate . ' +9 Weekday'));

                    
                    
                    if($level1Date == $currentDate){
                        
                        $userArr = User::whereHas("roles", function($q){ $q->where("name", "level 1"); })->where("id",$levelData->level1_id)->first();


                        $escalationArray = DB::table('audit_escalations')
                                        ->select('bank_name','client_name','stack_no','description')
                                        ->where('status','0')
                                        ->where('audit_inspection_id',$levelData->id)
                                        ->get()
                                        ->toArray();

                        //echo "<pre>";                    
                        //print_r($userAr);die;  
                        
                    }else if($level2Date == $currentDate){
                        
                        $level1User = User::whereHas("roles", function($q){ $q->where("name", "level 1"); })->where("id",$levelData->level1_id)->first();

                        $userArr = User::whereHas("roles", function($q){ $q->where("name", "level 2"); })->where('id',$level1User->parent_id)->get()->toArray();

                        $escalationArray = DB::table('audit_escalations')
                                        ->select('bank_name','client_name','stack_no','description')
                                        ->where('status','0')
                                        ->where('audit_inspection_id',$levelData->id)
                                        ->get()
                                        ->toArray();

                        //echo "<pre>";                    
                        //print_r($userAr);die;  
                        
                    }else if($level3Date == $currentDate){
                        
                        $userArr = User::whereHas("roles", function($q){ $q->where("name", "level 3"); })->get()->toArray();

                        $escalationArray = DB::table('audit_escalations')
                                        ->select('bank_name','client_name','stack_no','description')
                                        ->where('status','0')
                                        ->where('audit_inspection_id',$levelData->id)
                                        ->get()
                                        ->toArray();    
                        
                    }else if($level4Date == $currentDate){
                        
                        $userArr = User::whereHas("roles", function($q){ $q->where("name", "level 4"); })->get()->toArray(); 

                        $escalationArray = DB::table('audit_escalations')
                                        ->select('audit_escalations.bank_name','audit_escalations.client_name','audit_escalations.stack_no','audit_escalations.description')
                                        ->join('field_mappers','audit_escalations.field_type','=','field_mappers.data_field')
                                        ->where('audit_escalations.status','0')
                                        ->where('audit_escalations.audit_inspection_id',$levelData->id)
                                        ->where('field_mappers.level_4',1)
                                        ->get()
                                        ->toArray();   
                        
                    }
                   
                    
                    if(!empty($userArr) && count($userArr)>0 && count($escalationArray)>0){
                        
                        /* GET ESCALATION DETAILS */
                            //$msg2 = "";
                            
                            


                        foreach($userArr as $user){

                            $maildata = ['escalationArray'=>$escalationArray]; 
                            //print_r($maildata); exit;
                            $email = $user['email'];
                            //$email = "khushiddin@aryacma.in";
                            $subject = "Reminder Email  - Revert on open queries Type - ".$levelData->type_of_warehouse. ", Warehouse - ".$levelData->name_of_warehouse.", Code - ".$levelData->warehouse_code;
                            $aa =  Mail::send('escalationmail',$maildata,function($message) use($maildata,$email,$subject) {

                                $message->from('info@aryacma.co.in');
                                $message->to($email)->subject($subject);
                             
                             });

                                
                        } 
                        $counter++;

                    }
                }    
            }
            echo $counter;
        }                   

        

	 }

	


}
