<?php
Route::get('/', function () { return redirect('/admin/home'); });

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);

    Route::resource('states', 'Admin\StateController');
    Route::post('states_mass_destroy', ['uses' => 'Admin\StateController@massDestroy', 'as' => 'states.mass_destroy']);

    Route::resource('locations', 'Admin\LocationController');
    Route::post('loations_mass_destroy', ['uses' => 'Admin\LocationController@massDestroy', 'as' => 'locations.mass_destroy']);
});

Route::group(['middleware' => ['auth'], 'prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('/home', 'HomeController@dashboard');
    
});


Route::post('/state','AjaxController@state')->name('state');

//MIS Routes
Route::get('/importexcel/index','ImportExcelController@index')->name('importexcel.index');
Route::get('/importexcel/uploadmis','ImportExcelController@uploadmis')->name('importexcel.uploadmis');
Route::post('/importexcel/savemis','ImportExcelController@savemis')->name('importexcel.savemis');
Route::get('/importexcel/viewmisdata/{id}','ImportExcelController@viewmisdata')->name('importexcel.viewmisdata');
Route::get('/importexcel/editmisdata/{id}','ImportExcelController@editmisdata')->name('importexcel.editmisdata');
Route::post('/importexcel/saveupdatedmisdata/{id}','ImportExcelController@saveupdatedmisdata')->name('importexcel.saveupdatedmisdata');

Route::get('/importexcel/dtrdownload','ImportExcelController@dtrdownload')->name('importexcel.dtrdownload');
Route::get('/importexcel/cmdownload','ImportExcelController@cmdownload')->name('importexcel.cmdownload');
Route::get('/importexcel/viewallassignedwarehouses','ImportExcelController@viewallassignedwarehouses')->name('importexcel.viewallassignedwarehouses');



//Audit Inspection Routes
Route::get('/auditinspection/index','AuditInspectionController@index')->name('auditinspection.index');
Route::get('/auditinspection/add/{id}','AuditInspectionController@add')->name('auditinspection.add');
Route::get('/auditinspection/addmore/{id}','AuditInspectionController@addmore')->name('auditinspection.addmore');
Route::post('/auditinspection/save','AuditInspectionController@save')->name('auditinspection.save');
Route::post('/auditinspection/savemore/{id}','AuditInspectionController@savemore')->name('auditinspection.savemore');
Route::post('/auditinspection/edit/{id}','AuditInspectionController@edit')->name('auditinspection.edit');
Route::delete('/auditinspection/delete/{id}','AuditInspectionController@delete')->name('auditinspection.delete');
Route::get('/auditinspection/edit/{id}','AuditInspectionController@edit')->name('auditinspection.edit');
Route::put('/auditinspection/update/{id}','AuditInspectionController@update')->name('auditinspection.update');
Route::get('/auditinspection/editmore/{id}','AuditInspectionController@editmore')->name('auditinspection.editmore');
Route::put('/auditinspection/updatemore/{id}','AuditInspectionController@updatemore')->name('auditinspection.updatemore');

Route::get('/auditinspection/view/{id}','AuditInspectionController@view')->name('auditinspection.view');

Route::get('/auditinspection/list','AuditInspectionController@listing')->name('auditinspection.list');

Route::post('/auditinspection/assign','AuditInspectionController@assign')->name('auditinspection.assign');

Route::post('/auditinspection/assign','AuditInspectionController@assign')->name('auditinspection.assign');

Route::post('/auditinspection/approve','AuditInspectionController@approve')->name('auditinspection.approve');

Route::post('/auditinspection/reject','AuditInspectionController@reject')->name('auditinspection.reject');

Route::post('/auditinspection/addcomment','AuditInspectionController@addcomment')->name('auditinspection.addcomment');

Route::post('/auditinspection/check','AuditInspectionController@check')->name('auditinspection.check');

Route::get('/auditinspection/escalationfield','AuditInspectionController@escalationfield')->name('auditinspection.escalationfield');

Route::get('/auditinspection/getuser','AuditInspectionController@getuser')->name('auditinspection.getuser');

Route::post('/auditinspection/savepv','AuditInspectionController@savepv')->name('auditinspection.savepv');

Route::get('/auditinspection/uploadauditvideo/{id}','AuditInspectionController@uploadauditvideo')->name('auditinspection.uploadauditvideo');

Route::post('/auditinspection/saveauditvideo','AuditInspectionController@saveauditvideo')->name('auditinspection.saveauditvideo');

Route::get('/auditinspection/editauditvideo/{id}','AuditInspectionController@editauditvideo')->name('auditinspection.editauditvideo');

Route::put('/auditinspection/updateauditvideo/{id}','AuditInspectionController@updateauditvideo')->name('auditinspection.updateauditvideo');

Route::post('/auditinspection/clusterheads','AuditInspectionController@clusterheads')->name('auditinspection.clusterheads');


//Routes for reports
Route::group(['middleware' => ['auth'], 'prefix' => 'reports', 'as' => 'reports.'], function () {

    Route::get('plannedwarehouses', ['uses'=>'Admin\ReportController@plannedwarehouses','as' => 'plannedwarehouses']);

    Route::get('auditedwarehouses', ['uses'=>'Admin\ReportController@auditedwarehouses','as' => 'auditedwarehouses']);

    Route::get('clientwiseplannedwarehouses', ['uses'=>'Admin\ReportController@clientwiseplannedwarehouses','as' => 'clientwiseplannedwarehouses']);

    Route::get('auditfindings', ['uses'=>'Admin\ReportController@auditfindings','as' => 'auditfindings']);

    Route::get('clientwisedetail', ['uses'=>'Admin\ReportController@clientwisedetail','as' => 'clientwisedetail']);

    Route::get('auditfindingsdetail', ['uses'=>'Admin\ReportController@auditfindingsdetail','as' => 'auditfindingsdetail']);

    Route::get('auditfindingsdishonoured', ['uses'=>'Admin\ReportController@auditfindingsdishonoured','as' => 'auditfindingsdishonoured']);

    Route::get('auditfindingsdetaildishonoured', ['uses'=>'Admin\ReportController@auditfindingsdetaildishonoured','as' => 'auditfindingsdetaildishonoured']);

    Route::get('warehousewisemismatchreport', ['uses'=>'Admin\ReportController@warehousewisemismatchreport','as' => 'warehousewisemismatchreport']);

    Route::get('warehousewisemismatchreportdetail', ['uses'=>'Admin\ReportController@warehousewisemismatchreportdetail','as' => 'warehousewisemismatchreportdetail']);    
    
});

Route::get('/reminder/sendescalations','ReminderController@sendescalations')->name('auditinspection.sendescalations');


//use App\User;
Route::get("/test",function(){
	//echo "test";
	//echo route('auth.login');
	//echo url('login');
	 //abort(401);
	echo "<pre>";
	print_r(App\User::all()->toArray());
	//print_r(App\User::all()->pluck('name','name'));
	//dd(App\User::all());
})->middleware('Logger');

Route::get("/upload",function(){

    return view('upload');

})->name('upload');

Route::post("/save",function(){
    //dd(request()->file('image'));
    // $result = request()->file('image')->store(
    //     'aryapro',
    //     's3'
    // );
    $file = request()->file('image');
    //print_r($file);die;
    $imageName = 'aryapro/'.$file->getClientOriginalName();

    Storage::disk('s3')->put($imageName, file_get_contents($file));
    Storage::disk('s3')->setVisibility($imageName, 'public');

    $url = Storage::disk('s3')->url($imageName);
    //print_r($url);die;
    return back();
})->name('save');

Route::get('send','MailController@send');

Route::get("/internetnotify",function(){

    return view('internetnotify');

})->name('internetnotify');

Route::get('updateusingcsv','ImportExcelController@updateusingcsv');





















