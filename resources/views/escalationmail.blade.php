Dear Sir,
Please revert on following open queries:
<h2>Escalation List</h2>
<table border="1" cellspacing="0" style="margin-left: 20px;margin-right: 20px;">
    <tbody>
        <tr>
            <td align="center" valign="top">
                <p>
                    S.No.
                </p>
            </td>
            <td align="center" valign="top">
                <p>
                    Escalation
                </p>
            </td>
            <td align="center" valign="top">
                <p>
                    Bank Name
                </p>
            </td>
            <td align="center" valign="top">
                <p>
                    Client Name
                </p>
            </td>
            <td align="center" valign="top">
                <p>
                    Stack Number
                </p>
            </td>
            
            
        </tr>
        @foreach($escalationArray as $key => $val)
        
        <tr>
            <td align="center" valign="bottom">
                <h6>
                    {{$key+1}}
                </h6>
            </td>
            <td align="center" valign="bottom">
                <h5>
                    {{ $val->description }}
                </h5>
            </td>
            <td align="center" valign="bottom">
                <h5>
                    {{ $val->bank_name }}
                </h5>
            </td>
            <td align="center" valign="bottom">
                <h5>
                    {{ $val->client_name }}
                </h5>
            </td>
            <td align="center" valign="bottom">
                <h5>
                    {{ $val->stack_no }}
                </h5>
            </td>
            
            
        </tr>
        @endforeach

        
        
    </tbody>
</table>

<br><br>
Regards
<br>
Arya Warehouse
