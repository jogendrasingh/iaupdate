@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Locations</h3>
    <p>
        <a href="{{ route('admin.locations.create') }}" class="btn btn-success">@lang('global.app_add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($locations) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Location</th>
                        <th>State</th>
                        
                        <th>&nbsp;</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($locations) > 0)
                        @foreach ($locations as $location)
                            <tr data-entry-id="{{ $location->id }}">
                                <td>{{ $location->id }}</td>
                                <td>{{ $location->location }}</td>
                                <td>{{ $location->state }}</td>
                                
                                <td>
                                    <a href="{{ route('admin.locations.edit',[$location->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.locations.destroy', $location->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    
@endsection