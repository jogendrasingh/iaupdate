@extends('layouts.app')

@section('content')
    <h3 class="page-title">Locations</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.locations.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <label for="state_id" class="control-label">States*</label>
                    <select class="form-control select2" required="" id="state_id" name="state_id" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        @foreach ($states as $s)                      
                            <option value="{{$s->id}}">{{$s->state}}</option>
                        @endforeach
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('state_id'))
                        <p class="help-block">
                            {{ $errors->first('state_id') }}
                        </p>
                    @endif
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 form-group">
                    <label for="location_name" class="control-label">Location*</label>
                    <input class="form-control" placeholder="" required="" name="location_name" type="text" id="location_name">
                    <p class="help-block"></p>
                    @if($errors->has('location_name'))
                        <p class="help-block">
                            {{ $errors->first('location_name') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

