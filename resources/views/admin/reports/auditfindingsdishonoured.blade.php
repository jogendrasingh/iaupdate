<?php 
    $role = Auth::user()->roles->pluck('name')->first();
?>
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')


@section('content')
    <h3 class="page-title">Audit Findings</h3>
    
    <div class="clearfix"></div>
    <label></label>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <form method="GET" action="{{ route('reports.auditfindingsdishonoured') }}" id="searchForm">
    <div class="col-md-12 row">
        <div class="col-md-5 form-group">
            <label for="from_date" class="control-label">From Date*</label>
            <input class="form-control searchDate" placeholder="" name="from_date" type="text" value="{{ old('from_date', $from_date) }}">
        </div>

        <div class="col-md-5 form-group">
            <label for="to_date" class="control-label">To Date*</label>
            <input class="form-control searchDate" placeholder="" name="to_date" type="text" value="{{ old('to_date', $to_date) }}">
        </div>

        <div class="col-md-2 form-group">
            <br>
            <button type="submit" class="btn btn-info">Search</button>
        </div>

        
    </div>
    <div class="clearfix"></div>
    </form>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($finalArr) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Nature of Audit Observation</th>
                        <th>CM</th>
                        <th>PWH</th>
                        <th>PWH-CM</th>
                        <th>Total</th>
                        
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($finalArr) > 0)
                        @foreach ($finalArr as $key=>$fa)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $fa['particular'] }}</td>
                                <td>
                                    @if($fa['CM']==0)
                                    {{$fa['CM']}} 
                                    @else
                                    <a href="{{route('reports.auditfindingsdetaildishonoured')}}?field={{$key}}&type=CM&fdate={{$from_date}}&tdate={{$to_date}}" class="btn btn-info">{{$fa['CM']}}</a>
                                    @endif
                                </td>
                                <td>@if($fa['PWH']==0)
                                    {{$fa['PWH']}} 
                                    @else
                                    <a href="{{route('reports.auditfindingsdetaildishonoured')}}?field={{$key}}&type=PWH&fdate={{$from_date}}&tdate={{$to_date}}" class="btn btn-info">{{$fa['PWH']}}</a>
                                    @endif</td>
                                <td>@if($fa['PWH-CM']==0)
                                    {{$fa['PWH-CM']}} 
                                    @else
                                    <a href="{{route('reports.auditfindingsdetaildishonoured')}}?field={{$key}}&type=PWH-CM&fdate={{$from_date}}&tdate={{$to_date}}" class="btn btn-info">{{$fa['PWH-CM']}}</a>
                                    @endif</td>
                                <td>{{ $fa['total'] }}</td>
                            
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>


        
    </div>
@stop

@section('javascript') 
<script type="text/javascript">
     //to initialize datepicker plugin  
      $('.searchDate').datepicker({
        autoclose: true,
        format:"dd-mm-yyyy"
      });
</script>
    
@endsection