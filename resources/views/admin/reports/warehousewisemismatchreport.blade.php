<?php 
    $role = Auth::user()->roles->pluck('name')->first();
?>
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')


@section('content')
    <h3 class="page-title">Stock Mismatch Report</h3>
    
    <div class="clearfix"></div>
    <label></label>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <form method="GET" action="{{ route('reports.warehousewisemismatchreport') }}" id="searchForm">
    <div class="col-md-12 row">
        <div class="col-md-5 form-group">
            <label for="from_date" class="control-label">From Date*</label>
            <input class="form-control searchDate" placeholder="" name="from_date" type="text" value="{{ old('from_date', $from_date) }}">
        </div>

        <div class="col-md-5 form-group">
            <label for="to_date" class="control-label">To Date*</label>
            <input class="form-control searchDate" placeholder="" name="to_date" type="text" value="{{ old('to_date', $to_date) }}">
        </div>

        <div class="col-md-2 form-group">
            <br>
            <button type="submit" class="btn btn-info">Search</button>
        </div>

        
    </div>
    <div class="clearfix"></div>
    </form>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($finalArr) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>Audit Id</th>
                        <th>Warehouse Code</th>
                        <th>Name of Warehouse</th>
                        <th>Stock Shartage(Count)</th>
                        <th>Stock Not Tallied(Count)</th>
                        <th>Audit Date</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($finalArr) > 0)
                        @foreach ($finalArr as $key=>$fa)
                            <tr>
                                <td>{{ $fa['id'] }}</td>
                                <td>{{ $fa['warehouse_code'] }}</td>
                                <td>{{ $fa['name_of_warehouse'] }}</td>
                                <td>
                                    @if(isset($fa['shortageCount']))
                                    <a href="{{route('reports.warehousewisemismatchreportdetail')}}?insId={{$fa['id']}}&type=SC&fdate={{$from_date}}&tdate={{$to_date}}" class="btn btn-info">{{$fa['shortageCount']}}</a> 
                                    @else
                                    {{ 0 }}
                                    @endif
                                </td>
                                <td>
                                    @if(isset($fa['notTalliedCount']))
                                    <a href="{{route('reports.warehousewisemismatchreportdetail')}}?insId={{$fa['id']}}&type=NT&fdate={{$from_date}}&tdate={{$to_date}}" class="btn btn-info">{{$fa['notTalliedCount']}}</a> 
                                    @else
                                    {{ 0 }}
                                    @endif
                                </td>
                                <td>{{ date("d-m-Y",strtotime($fa['created_at'])) }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>


        
    </div>
@stop

@section('javascript') 
<script type="text/javascript">
     //to initialize datepicker plugin  
      $('.searchDate').datepicker({
        autoclose: true,
        format:"dd-mm-yyyy"
      });
</script>
    
@endsection