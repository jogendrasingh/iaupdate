<?php 
    $role = Auth::user()->roles->pluck('name')->first();
?>
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')


@section('content')
    <h3 class="page-title">Stock Mismatch Report Details</h3>
    
    <div class="clearfix"></div>
    <label></label>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    
    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($resultArr) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Warehouse Code</th>
                        <th>Name Of Warehouse</th>
                        <th>Stack number</th>
                        <th>System Stock</th>
                        <th>Entered Stock</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($resultArr) > 0)
                        @foreach ($resultArr as $key=>$ra)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $ra->warehouse_code }}</td>
                                <td>{{ $ra->name_of_warehouse }}</td>
                                <td>{{ $ra->stack_no }}</td>
                                <td>{{ $ra->current_stock_bags }}</td>
                                <td>{{ $ra->escalation_value }}</td>
                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>


        
    </div>
@stop

@section('javascript') 
<script type="text/javascript">
    
</script>
    
@endsection