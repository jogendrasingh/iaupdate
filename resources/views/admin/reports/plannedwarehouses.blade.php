<?php 
    $role = Auth::user()->roles->pluck('name')->first();
?>
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')


@section('content')
    <h3 class="page-title">Current Month's Planned Warehouses</h3>
    @role('maker')
    <p>
        <!-- <a href="{{ route('auditinspection.list') }}" class="btn btn-success pull-right">Start Audit</a> -->
    </p>
    @endrole
    <div class="clearfix"></div>
    <label></label>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($misdata) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Warehouse Code</th>
                        <th>State</th>
                        <th>Name of Warehouse</th>
                        <th>Location</th>
                        <th>Created Date</th>
                        <th>Type of Warehouse</th>
                        <th>Auditor Name</th>
                        
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($misdata) > 0)
                        @foreach ($misdata as $md)
                            <tr data-entry-id="{{ $md->id }}">
                                <td>{{ $md->id }}</td>
                                <td>{{ $md->warehouse_code }}</td>
                                <td>{{ $md->state }}</td>
                                <td>{{ $md->name_of_warehouse }}</td>
                                <td>{{ $md->location_name }}</td>
                                <td>{{ date("d-m-Y",strtotime($md->created_at)) }}</td>
                                <td>{{ $md->type_of_warehouse }}</td>
                                <td>{{ $md->maker_name }}</td>
                                
    

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>


        
    </div>
@stop

@section('javascript') 
<script type="text/javascript">
    
// function assign(id){
//      var confirmMsg = confirm("Are You sure, you want to assign this warehouse to checker?");
//      var _token = "<?php echo csrf_token();?>";
//         if (confirmMsg == true) {
//             $("#assignButton").hide();
//             $.ajax({
//             url: "{{ route('auditinspection.assign') }}",
//             data: {"id":id,"_token":_token},
//             type: 'POST',
//             dataType: 'json',
//             success: function(response) {
//             console.log(response);    
//            if(response.value=='Success'){
//                location.href="{{ route('auditinspection.index')}}";
              
//            }else if(response.value=='Error'){
//                location.href="{{ route('auditinspection.index')}}";
//            }
//            },
//             error: function() { alert("Failed"); }
//         }); 
//         } else {
//             location.href="{{ route('auditinspection.index')}}";
//         }
// }

</script>
    
@endsection