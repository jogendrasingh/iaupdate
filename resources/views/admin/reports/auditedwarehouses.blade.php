<?php 
    $role = Auth::user()->roles->pluck('name')->first();
?>
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')


@section('content')
    <h3 class="page-title">Current Month's Audited Warehouses</h3>
    
    <div class="clearfix"></div>
    <label></label>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($auditinspections) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Warehouse Code</th>
                        <th>State</th>
                        <th>Name of Warehouse</th>
                        <th>Location</th>
                        <th>Created Date</th>
                        <th>Type of Warehouse</th>
                        <th>Auditor Name</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($auditinspections) > 0)
                        @foreach ($auditinspections as $ai)
                            <tr data-entry-id="{{ $ai->id }}">
                                <td>{{ $ai->id }}</td>
                                <td>{{ $ai->warehouse_code }}</td>
                                <td>{{ $ai->state }}</td>
                                <td>{{ $ai->name_of_warehouse }}</td>
                                <td>{{ $ai->location_name }}</td>
                                <td>{{ date("d-m-Y",strtotime($ai->created_at)) }}</td>
                                <td>{{ $ai->type_of_warehouse }}</td>
                                <td>{{ $ai->auditor_name }}</td>
                                

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>


        
    </div>
@stop

@section('javascript') 
<script type="text/javascript">
    

</script>
    
@endsection