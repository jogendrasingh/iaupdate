@extends('layouts.app')
@section('content')
    <h3 class="page-title">@lang('global.users.title')</h3>
    
    {!! Form::model($user, ['method' => 'PUT', 'route' => ['admin.users.update', $user->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('mobile', 'Mobile*', ['class' => 'control-label']) !!}
                    {!! Form::text('mobile', old('mobile'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('mobile'))
                        <p class="help-block">
                            {{ $errors->first('mobile') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
                    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('email'))
                        <p class="help-block">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('password', 'Password', ['class' => 'control-label']) !!}
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('password'))
                        <p class="help-block">
                            {{ $errors->first('password') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('roles', 'Roles*', ['class' => 'control-label']) !!}
                    {!! Form::select('roles[]', $roles, old('roles') ? old('role') : $user->roles()->pluck('name', 'name'), ['class' => 'form-control select2', 'required' => '','id'=>'role','onchange'=>'getParent()']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('roles'))
                        <p class="help-block">
                            {{ $errors->first('roles') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row" id="checker">
                <div class="col-xs-12 form-group">
                    <label for="parent_id" class="control-label">Select Checker*</label>
                    <select class="form-control select2"  id="parent_id" name="parent_id" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        @foreach ($checkers as $c)                      
                            <option value="{{$c->id}}">{{$c->name}}</option>
                        @endforeach
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('checkers'))
                        <p class="help-block">
                            {{ $errors->first('checkers') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row" id="level2">
                <div class="col-xs-12 form-group">
                    <label for="parent_id" class="control-label">Select Level 2*</label>
                    <select class="form-control select2"  id="parent_id" name="parent_id" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        @foreach ($level2 as $l)                      
                            <option value="{{$l->id}}">{{$l->name}}</option>
                        @endforeach
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('level2'))
                        <p class="help-block">
                            {{ $errors->first('level2') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row" id="level1">
                <div class="col-xs-12 form-group">
                    <label for="parent_id" class="control-label">Select Level 1*</label>
                    <select class="form-control select2"  id="parent_id" name="parent_id" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        @foreach ($level1 as $l)                      
                            <option value="{{$l->id}}">{{$l->name}}</option>
                        @endforeach
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('level1'))
                        <p class="help-block">
                            {{ $errors->first('level1') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row" id="displayState">
                <div class="col-xs-12 form-group">
                    <label for="state_id" class="control-label">States*</label>
                    <select class="form-control select2"  id="state_id" name="state_id" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        @foreach ($states as $s)                      
                            <option value="{{$s->id}}">{{$s->state}}</option>
                        @endforeach
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('states'))
                        <p class="help-block">
                            {{ $errors->first('states') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop
@section('javascript') 
    
<script type="text/javascript">

//setTimeout(function(){
        getParent();
        $('#state_id').val("{{$user->state_id}}").trigger('change');
        $('#parent_id').val("{{$user->parent_id}}").trigger('change');
    //},1000);

function getParent(){
    var roleOfUser=$("#role").val();
        $("#checker").hide();
        $("#level2").hide();
        $('#displayState').hide();
       // if(roleOfUser=='maker' || roleOfUser=='checker'){
       //      $('#work').show();
       //  }else {
       //      $('#work').hide();
       //  }
        
       //alert(roleOfUser);
      if(roleOfUser=='maker' || roleOfUser=='level 1' || roleOfUser=='cluster head'){

        if(roleOfUser=='maker'){
            $("#checker").show();
            $("#level2").hide();
            $("#level1").hide();      
        }else if(roleOfUser=='level 1'){
            $("#level2").show();
            $("#checker").hide(); 
            $("#level1").hide(); 
        }else{
            $("#level1").show();
            $("#checker").hide(); 
            $("#level2").hide();
        }        

     }else{
      $("#checker").hide();
      $("#level2").hide(); 
      $("#level1").hide(); 
    }
    if(roleOfUser=='level 2' || roleOfUser=='level 1'){
        if(roleOfUser=='level 2'){
            $("#displayState").show();
        }else{
            $("#level2").show();
            $("#displayState").show();
    }
}
    

  }


// function getState(){
//         //alert('test');
//     //var roleOfUser=$("#role").val();
//      var _token = "<?php echo csrf_token();?>";   
//         $.ajax({
//             type: "POST",
//             url: "{{ url('state') }}",
//             data:{_token:_token},
//             success: function(response){
//             //console.log(response);    
//             $("#state_id").find('option').remove();          
//                 if(response!=''){
//                 //parsed = $.parseJSON(response);
//                 //console.log(parsed);
//                 $("#state_id").append($("<option></option>").attr("value","").text("Select"));    
//                 $.each(response, function(key, value) {
//                     $("#state_id")
//                         .append($("<option></option>")
//                         .attr("value",value.id)
//                         .text(value.state)); 
//                  }); 

//                 $("#state_id option[value="+<?=$user->state_id;?>+"]").prop("selected", true);
               
//             }
//        }
//     });
// }    
</script>
@endsection