@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Assigned Warehouse Data Edit</h3>
    
    <form method="POST" action="{{ route('importexcel.saveupdatedmisdata',[$misdatadetails->id]) }}" enctype="multipart/form-data">
            {{csrf_field()}}   
        <div class="panel panel-default">
            <div class="panel-heading">
                Edit
            </div>

            <input name="mis_excel_id" type="hidden" id="mis_excel_id" value="{{$misdatadetails->mis_excel_id}}">
             
            <div class="panel-body">               
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="state" class="control-label">State*</label>
                        <input class="form-control" placeholder="" required="" name="state" type="text" id="state" value="{{$misdatadetails->state}}">
                        <p class="help-block"></p>
                        @if($errors->has('state'))
                            <p class="help-block">
                                {{ $errors->first('state') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="warehouse_code" class="control-label">Warehouse Code*</label>
                        <input class="form-control" placeholder="" required="" name="warehouse_code" type="text" id="warehouse_code" value="{{$misdatadetails->warehouse_code}}">
                        <p class="help-block"></p>
                        @if($errors->has('warehouse_code'))
                            <p class="help-block">
                                {{ $errors->first('warehouse_code') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="name_of_warehouse" class="control-label">Name of Warehouse*</label>
                        <input class="form-control" placeholder="" required="" name="name_of_warehouse" type="text" id="name_of_warehouse" value="{{$misdatadetails->name_of_warehouse}}">
                        <p class="help-block"></p>
                        @if($errors->has('name_of_warehouse'))
                            <p class="help-block">
                                {{ $errors->first('name_of_warehouse') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="location_name" class="control-label">Location Name*</label>
                        <input class="form-control" placeholder="" required="" name="location_name" type="text" id="location_name" value="{{$misdatadetails->location_name}}">
                        <p class="help-block"></p>
                        @if($errors->has('location_name'))
                            <p class="help-block">
                                {{ $errors->first('location_name') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="type_of_warehouse" class="control-label">Type of Warehouse*</label>
                        <input class="form-control" placeholder="" required="" name="type_of_warehouse" type="text" id="type_of_warehouse" value="{{$misdatadetails->type_of_warehouse}}">
                        <p class="help-block"></p>
                        @if($errors->has('type_of_warehouse'))
                            <p class="help-block">
                                {{ $errors->first('type_of_warehouse') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="warehouse_start_date" class="control-label">Warehouse Start Date*</label>
                        <input class="form-control" placeholder="" required="" name="warehouse_start_date" type="text" id="warehouse_start_date" value="{{$misdatadetails->warehouse_start_date}}">
                        <p class="help-block"></p>
                        @if($errors->has('warehouse_start_date'))
                            <p class="help-block">
                                {{ $errors->first('warehouse_start_date') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="client_name" class="control-label">Client Name*</label>
                        <input class="form-control" placeholder="" required="" name="client_name" type="text" id="client_name" value="{{$misdatadetails->client_name}}">
                        <p class="help-block"></p>
                        @if($errors->has('client_name'))
                            <p class="help-block">
                                {{ $errors->first('client_name') }}
                            </p>
                        @endif
                    </div>
                </div>
                
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="name_of_area_manager" class="control-label">Name of Area Manager*</label>
                        <input class="form-control" placeholder="" required="" name="name_of_area_manager" type="text" id="name_of_area_manager" value="{{$misdatadetails->name_of_area_manager}}">
                        <p class="help-block"></p>
                        @if($errors->has('name_of_area_manager'))
                            <p class="help-block">
                                {{ $errors->first('name_of_area_manager') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="maker_name" class="control-label">Maker Name*</label>
                        <input class="form-control" placeholder="" required="" name="maker_name" type="text" id="maker_name" value="{{$misdatadetails->maker_name}}">
                        <p class="help-block"></p>
                        @if($errors->has('maker_name'))
                            <p class="help-block">
                                {{ $errors->first('maker_name') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="col-xs-12 form-group">
                        <label for="name_of_cluster_head" class="control-label">Cluster Head Name*</label>
                        <input class="form-control" placeholder="" required="" name="name_of_cluster_head" type="text" id="name_of_cluster_head" value="{{$misdatadetails->name_of_cluster_head}}">
                        <p class="help-block"></p>
                        @if($errors->has('name_of_cluster_head'))
                            <p class="help-block">
                                {{ $errors->first('name_of_cluster_head') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="form-group">
                      <button type="submit" class="btn btn-info pull-left">Update</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </form>     
@stop

@section('javascript') 
    <script>
        
    </script>
@endsection