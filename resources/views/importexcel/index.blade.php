@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Assigned File</h3>
    <p>
        <a href="{{ route('importexcel.uploadmis') }}" class="btn btn-success">Upload File</a>
        <a href="{{ route('importexcel.dtrdownload') }}" class="btn btn-success" >DTR Stock</a>
        <a href="{{ route('importexcel.cmdownload') }}" class="btn btn-success" >CM Stock</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($misdata) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Uploaded File</th>
                        <th>Upload Date</th>
                        <th>Imported Data</th>
                        <th>Skipped Data</th>                       
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($misdata) > 0)
                        @foreach ($misdata as $md)
                            <tr>
                                <td>{{ $md->id }}</td>
                                <td>{{ $md->mis_excel_name }}</td>
                                <td>{{ date('d-m-Y H:i:s', strtotime($md->created_at)) }}</td>
                                <td><a href="{{ url('/storage/misexcel/imported').'/'.$md->mis_excel_name}}" class=""><span class="badge">{{$md->imported_count}}</span></a></td>
                                <td><a href="{{ url('/storage/misexcel/skipped').'/'.$md->mis_excel_name}}" class=""><span class="badge">{{$md->skipped_count}}</span></a></td>
                                <td>
                                    <a href="{{ url('/storage/misexcel').'/'.$md->mis_excel_name}}" class="btn btn-xs btn-info">Download</a>
                                    <a href="{{ route('importexcel.viewmisdata', [$md->id]) }}" class="btn btn-xs btn-primary">View</a>
                                    </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

        
        
    </div>
@stop

@section('javascript') 
    <script>
       
    </script>
@endsection