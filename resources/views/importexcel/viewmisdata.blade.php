@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Assigned Warehouse Details</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($misdatadetails) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>State</th>
                        <th>Warehouse Code</th>                       
                        <th>Warehouse Name</th>
                        <th>Location Name</th>
                        <th>Type of Warehouse</th>
                        <th>Warehouse Start Date</th>                       
                        <th>Client Name</th>
                        <th>Area Manager Name</th>
                        <th>Maker Name</th>
                        <th>Cluster Head Name</th>                       
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($misdatadetails) > 0)
                        @foreach ($misdatadetails as $md)
                            <tr>
                                <td>{{ $md->id }}</td>
                                <td>{{ $md->state }}</td>
                                <td>{{ $md->warehouse_code }}</td>
                                <td>{{ $md->name_of_warehouse }}</td>
                                <td>{{ $md->location_name }}</td>
                                <td>{{ $md->type_of_warehouse }}</td>
                                <td>{{ $md->warehouse_start_date }}</td>
                                <td>{{ $md->client_name }}</td>
                                <td>{{ $md->name_of_area_manager }}</td>
                                <td>{{ $md->maker_name }}</td>
                                <td>{{ $md->name_of_cluster_head }}</td>
                                <td>
                                    <a href="{{ route('importexcel.editmisdata', [$md->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                </td>
                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

        
        
    </div>
@stop

@section('javascript') 
    <script>
       
    </script>
@endsection