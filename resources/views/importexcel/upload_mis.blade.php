@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Upload File</h3>
    <!-- @if(count($errors) > 0)
    <div class="alert alert-danger">
     Upload Validation Error<br><br>
     <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif -->
   @if($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif

    @if($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif

    <form method="POST" action="{{ route('importexcel.savemis') }}" enctype="multipart/form-data">
            {{csrf_field()}}   
        <div class="panel panel-default">
            <div class="panel-heading">
                Upload Warehouse Assigning File
            </div>
             
            <div class="panel-body">               
                <div class="row">
                    <div class="col-xs-12 form-group">
                      <label for="misexcel">Upload File</label>
                      <input type="file" accept=".xls,.xlsx,.csv" id="misexcel" name="misexcel">
                      <p class="help-block">Please select Excel (xlsx, xls) file to upload.</p>
                    </div>
                </div>
                <div class="col-xs-12 row">
                    <div class="form-group">
                      <button type="submit" class="btn btn-info pull-left">Upload</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </form>     
@stop

@section('javascript') 
    <script>
        
    </script>
@endsection