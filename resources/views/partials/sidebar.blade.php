@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<?php 
    $role = Auth::user()->roles->pluck('name')->first();
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            
            @can('users_manage')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span class="title">@lang('global.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.permissions.index') }}">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                @lang('global.permissions.title')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.roles.index') }}">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                @lang('global.roles.title')
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-user"></i>
                            <span class="title">
                                @lang('global.users.title')
                            </span>
                        </a>
                    </li>
                    
                </ul>
            </li>
            @endcan

            
            @can('states_manage')
            <li class="{{ $request->segment(2) == 'states' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.states.index') }}">
                    <i class="fa fa-user"></i>
                    <span class="title">
                        @lang('global.states.title')
                    </span>
                </a>
            </li>
            <li class="{{ $request->segment(2) == 'locations' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.locations.index') }}">
                    <i class="fa fa-user"></i>
                    <span class="title">
                        Locations
                    </span>
                </a>
            </li>
            @endcan

            @can('mis_manage')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span class="title">Warehouse Assignment</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $request->segment(1) == 'importexcel' ? 'active active-sub' : '' }}">
                        <a href="{{ route('importexcel.index') }}">
                            <i class="fa fa-user"></i>
                            <span class="title">
                                Warehouse Assignment
                            </span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(2) == 'viewallassignedwarehouses' ? 'active active-sub' : '' }}">
                        <a href="{{ route('importexcel.viewallassignedwarehouses') }}">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">
                                Assigned Warehouses
                            </span>
                        </a>
                    </li>
                    
                </ul>
            </li>
            @endcan
            @can('inspection_manage')
            <li class="{{ $request->segment(1) == 'auditinspection' ? 'active active-sub' : '' }}">
                <a href="{{ route('auditinspection.index') }}">
                    <i class="fa fa-user"></i>
                    <span class="title">
                        Audit Inspection
                    </span>
                </a>
            </li>
            @endcan
            @can('reports_manage')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file"></i>
                    <span class="title">Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <!-- <li class="{{ $request->segment(2) == 'plannedwarehouses' ? 'active active-sub' : '' }}">
                        <a href="{{ route('reports.plannedwarehouses') }}">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">
                                 Planned Warehouses
                            </span>
                        </a>
                    </li>

                    <li class="{{ $request->segment(2) == 'auditedwarehouses' ? 'active active-sub' : '' }}">
                        <a href="{{ route('reports.auditedwarehouses') }}">
                            <i class="fa fa-pie-chart"></i>
                            <span class="title">
                                Warehouse Audited
                            </span>
                        </a>
                    </li> -->

                    <li class="{{ $request->segment(2) == 'clientwiseplannedwarehouses' ? 'active active-sub' : '' }}">
                        <a href="{{ route('reports.clientwiseplannedwarehouses') }}">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">
                                Clientwise Planned Warehouses
                            </span>
                        </a>
                    </li>

                    <li class="{{ $request->segment(2) == 'auditfindings' ? 'active active-sub' : '' }}">
                        <a href="{{ route('reports.auditfindings') }}">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">
                                Audit Findings
                            </span>
                        </a>
                    </li>
                    @role('administrator')
                    <li class="{{ $request->segment(2) == 'auditfindingsdishonoured' ? 'active active-sub' : '' }}">
                        <a href="{{ route('reports.auditfindingsdishonoured') }}">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">
                                Audit Findings(Dishonoured)
                            </span>
                        </a>
                    </li>

                    <li class="{{ $request->segment(2) == 'warehousewisemismatchreport' ? 'active active-sub' : '' }}">
                        <a href="{{ route('reports.warehousewisemismatchreport') }}">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">
                                Stock Mismatch
                            </span>
                        </a>
                    </li>  
                    @endrole
                </ul>
            </li>
            @endcan
            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key"></i>
                    <span class="title">Change password</span>
                </a>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('global.app_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('global.logout')</button>
{!! Form::close() !!}


<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">

    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" style="position: absolute; top: 50px; right: 15px;z-index: 1;">
          <div class="toast-header">
              <i class="bi bi-wifi"></i>&nbsp;&nbsp;&nbsp;
              <strong class="mr-auto"><span class="text-success">Internet is Connected</span></strong>
          </div>        
      </div>
<script>

var status = 'online';
var current_status = 'online';

function check_internet_connection()
{
    if(navigator.onLine)
    {
        status = 'online';
    }
    else
    {
        status = 'offline';
    }

    if(current_status != status)
    {
        if(status == 'online')
        {
            $('i.bi').addClass('bi-wifi');
            $('i.bi').removeClass('bi-wifi-off');
            $('.mr-auto').html("<span class='text-success'>Internet is connected</span>");
        }
        else
        {
            $('i.bi').addClass('bi-wifi-off');
            $('i.bi').removeClass('bi-wifi');
            $('.mr-auto').html("<span class='text-danger'>Internet is disconnected</span>");
        }

        current_status = status;

        $('.toast').toast({
            autohide:false
        });

        $('.toast').toast('show');
    }
}

check_internet_connection();

setInterval(function(){
    check_internet_connection();
}, 1000);

</script>

