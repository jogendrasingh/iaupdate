@extends('layouts.app')

@section('content')
    <h3 class="page-title">Audit Inspection</h3>
<?php
    $auditorId = Auth::user()->id;
    //echo $auditorId;die;
?>

<style type="text/css">
    .error{
        color:red;
    }
</style>
    @if(isset($auditinspection))
        <form method="POST" action="{{ route('auditinspection.update',[$auditinspection->id]) }}" enctype="multipart/form-data" id="auditFormEdit">
    @elseif(isset($misdata))
        <form method="POST" action="{{ route('auditinspection.save') }}" enctype="multipart/form-data" id="auditForm">
       <input type="hidden" name="misdata_id" value="{{ $misdata->id }}">     
    @endif

    <input type="hidden" name="_method" value="{{ isset($auditinspection) ? 'PUT' : 'POST' }}">
    
            {{csrf_field()}}

    <input type="hidden" name="auditor_id" value="{{$auditorId}}"/>        
    
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Audit Inspection
            </div>
        
        <div class="panel-body">
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="location_closed" class="control-label">Location Closed*</label>
                    <select class="form-control select2" id="location_closed" name="location_closed" tabindex="-1" aria-hidden="true" onchange="hideShowSection(this.value,['section1','section11'])">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('location_closed'))
                        <p class="help-block">
                            {{ $errors->first('location_closed') }}
                        </p>
                    @endif
                </div>
                <div id='section1'>
                <div class="col-md-6 form-group">
                    <label for="location_not_started_before_audit" class="control-label">Location Not Started Before Audit*</label>
                    <select class="form-control select2" id="location_not_started_before_audit" name="location_not_started_before_audit" tabindex="-1" aria-hidden="true" onchange="hideShowSection(this.value,['section11'])">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('location_not_started_before_audit'))
                        <p class="help-block">
                            {{ $errors->first('location_not_started_before_audit') }}
                        </p>
                    @endif
                </div>
                </div>
            </div>
            <div id="section11">
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="cm_available_during_visit" class="control-label">CM/Supervisor available during the visit*</label>
                    <select class="form-control select2" id="cm_available_during_visit" name="cm_available_during_visit" tabindex="-1" aria-hidden="true" onchange="hideShow(this.value);">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('cm_available_during_visit'))
                        <p class="help-block">
                            {{ $errors->first('cm_available_during_visit') }}
                        </p>
                    @endif
                </div>
                <div id="section2">
                <div class="col-md-6 form-group">
                    <label for="whole_warehouse_under_fumigation" class="control-label">Whole Warehouse Under Fumigation</label>
                    <select class="form-control select2" id="whole_warehouse_under_fumigation" name="whole_warehouse_under_fumigation" tabindex="-1" aria-hidden="true" onchange="hideShowSection(this.value,['section22'])">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('whole_warehouse_under_fumigation'))
                        <p class="help-block">
                            {{ $errors->first('whole_warehouse_under_fumigation') }}
                        </p>
                    @endif
                </div>    
                
                </div>
            </div>
            <div class="col-md-12 row" id="cmNotAvailable" style="display: none;">
                <div class="col-md-6 form-group">
                    <label for="cm_not_available_remark" class="control-label">Remark:</label>
                    <textarea class="form-control" name="cm_not_available_remark" id="cm_not_available_remark">{{ $auditinspection->cm_not_available_remark or '' }}</textarea>
                    <p class="help-block"></p>
                    @if($errors->has('cm_not_available_remark'))
                        <p class="help-block">
                            {{ $errors->first('cm_not_available_remark') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="selfie_of_auditor_with_closed_warehouse_gate" class="control-label">Selfie of Auditor with Closed Warehouse Gate:</label>
                    <input type="file" name="selfie_of_auditor_with_closed_warehouse_gate"  class="form-control" />
                    @if(isset($auditinspection->selfie_of_auditor_with_closed_warehouse_gate) && $auditinspection->selfie_of_auditor_with_closed_warehouse_gate !='')
                    <img src="/storage/inspectionImages/selfieOfAuditor/{{$auditinspection->selfie_of_auditor_with_closed_warehouse_gate}}" width="100" width="100">
                    @endif
                </div>
            </div>
                 
            <div id="section22">
            <div class="col-md-12 row">
                <!-- <div class="col-md-6 form-group">
                    <label for="who_has_the_control_on_lock_and_key" class="control-label">Who has the control on lock and key?*</label>
                    <input class="form-control" placeholder="" name="who_has_the_control_on_lock_and_key" type="text" id="who_has_the_control_on_lock_and_key" value="{{ $auditinspection->who_has_the_control_on_lock_and_key or $misdata->lock_key_control }}">
                    <p class="help-block"></p>
                    @if($errors->has('who_has_the_control_on_lock_and_key'))
                        <p class="help-block">
                            {{ $errors->first('who_has_the_control_on_lock_and_key') }}
                        </p>
                    @endif
                </div> -->
                <div class="col-md-6 form-group">
                    <label for="who_has_the_control_on_lock_and_key" class="control-label">Who has the control on lock and key?*</label>
                    <select class="form-control select2" id="who_has_the_control_on_lock_and_key" name="who_has_the_control_on_lock_and_key" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Arya">Arya</option>
                        <option value="Warehouse Manager">Warehouse Manager</option>
                        <option value="Joint">Joint</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('who_has_the_control_on_lock_and_key'))
                        <p class="help-block">
                            {{ $errors->first('who_has_the_control_on_lock_and_key') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="auditor_name" class="control-label">Auditor Name*</label>
                    <input class="form-control" placeholder="" name="auditor_name" type="text" id="auditor_name" value="{{ $auditinspection->auditor_name or Auth::user()->name }}">
                    <p class="help-block"></p>
                    @if($errors->has('auditor_name'))
                        <p class="help-block">
                            {{ $errors->first('auditor_name') }}
                        </p>
                    @endif
                </div>
            </div>    
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="warehouse_code" class="control-label">Warehouse Code*</label>
                    <input class="form-control" placeholder="" name="warehouse_code" type="text" id="warehouse_code" value="{{ $auditinspection->warehouse_code or $misdata->warehouse_code }}">
                    <p class="help-block"></p>
                    @if($errors->has('warehouse_code'))
                        <p class="help-block">
                            {{ $errors->first('warehouse_code') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="state_id" class="control-label">State*</label>
                    <input  class="form-control" placeholder="" name="state" type="text" id="state_id" value="{{ $auditinspection->state or $misdata->state }}">
                    <p class="help-block"></p>
                    @if($errors->has('state_id'))
                        <p class="help-block">
                            {{ $errors->first('state') }}
                        </p>
                    @endif
                </div>
            </div>
            
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="name_of_warehouse" class="control-label">Name of Warehouse*</label>
                    <input class="form-control" placeholder="" name="name_of_warehouse" type="text" id="name_of_warehouse" value="{{ $auditinspection->name_of_warehouse or $misdata->name_of_warehouse }}">
                    <p class="help-block"></p>
                    @if($errors->has('name_of_warehouse'))
                        <p class="help-block">
                            {{ $errors->first('name_of_warehouse') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="location_name" class="control-label">Location Name*</label>
                    <input class="form-control" placeholder="" name="location_name" type="text" id="location_name" value="{{ $auditinspection->location_name or $misdata->location_name }}">
                    <p class="help-block"></p>
                    @if($errors->has('location_name'))
                        <p class="help-block">
                            {{ $errors->first('location_name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="warehouse_start_date" class="control-label">Warehouse Start Date*</label>
                    <input class="form-control" placeholder="" name="warehouse_start_date" type="text" id="warehouse_start_date" value="<?php 
                        if(isset($auditinspection->warehouse_start_date)){
                            echo date('d-m-Y', strtotime($auditinspection->warehouse_start_date));
                        }else{
                            echo date('d-m-Y', strtotime($misdata->warehouse_start_date));
                        }?>">
                    <p class="help-block"></p>
                    @if($errors->has('warehouse_start_date'))
                        <p class="help-block">
                            {{ $errors->first('warehouse_start_date') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="type_of_warehouse" class="control-label">Type of Warehouse*</label>
                    <input class="form-control" placeholder="" name="type_of_warehouse" type="text" id="type_of_warehouse" value="{{ $auditinspection->type_of_warehouse or $misdata->type_of_warehouse }}">
                    <p class="help-block"></p>
                    @if($errors->has('type_of_warehouse'))
                        <p class="help-block">
                            {{ $errors->first('type_of_warehouse') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="type_of_structure" class="control-label">Type of Structure</label>
                    <select class="form-control select2" id="type_of_structure" name="type_of_structure" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Licensed">Licensed</option>
                        <option value="Unlicensed">Unlicensed</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('type_of_structure'))
                        <p class="help-block">
                            {{ $errors->first('type_of_structure') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="unrecorded_stock" class="control-label">Unrecorded Stock</label>
                    <input class="form-control" placeholder="" name="unrecorded_stock" type="number" id="unrecorded_stock" value="{{ $auditinspection->unrecorded_stock or '' }}">
                    <p class="help-block"></p>
                    @if($errors->has('unrecorded_stock'))
                        <p class="help-block">
                            {{ $errors->first('unrecorded_stock') }}
                        </p>
                    @endif
                </div>
            </div>

            <?php
                if((isset($currentStock) && !empty($currentStock) && count($currentStock) !=0) || (isset($currentStockCotton) && !empty($currentStockCotton)) && count($currentStockCotton) !=0){
                $currentStockArr = array();    
                if(isset($currentStock) && !empty($currentStock)){    
                //echo "<pre>";    
                //print_r($currentStockCotton);die;    
                foreach ($currentStock as $key => $value) {
                    $currentStockArr[] = (array)$value;        
                    }
                $last_transaction_date_system=max(array_column($currentStockArr, 'max_created_at'));             
                }

                if(isset($currentStockCotton) && !empty($currentStockCotton)){    
                    
                foreach ($currentStockCotton as $key => $value) {
                    $currentStockArr[] = (array)$value;        
                    }
                $last_transaction_date_system=max(array_column($currentStockArr, 'max_created_at'));             
                }    
            ?>        
            <input type="hidden" name="last_transaction_date_system" value="{{date('Y-m-d',strtotime($last_transaction_date_system))}}"/>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="last_transaction_date" class="control-label">Last Transaction Date*</label>
                    <input class="form-control" placeholder="" name="last_transaction_date" type="text" id="last_transaction_date" value="<?php 
                        if(isset($auditinspection->last_transaction_date)){
                            echo date('d-m-Y', strtotime($auditinspection->last_transaction_date));
                        }else{
                            echo '';
                        }?>">
                    <p class="help-block"></p>
                    @if($errors->has('last_transaction_date'))
                        <p class="help-block">
                            {{ $errors->first('last_transaction_date') }}
                        </p>
                    @endif
                </div>
            </div>
            <?php
                }
            ?>
            
            <?php 
                if(isset($currentStock)){
                    $arrayCount = count($currentStock); 
                } 
                if(isset($currentStockCm)){
                    $arrayCount1 = count($currentStockCm);
                }
                if(isset($currentStockCotton)){
                    $arrayCount2 = count($currentStockCotton);
                }        

            ?>

            @if(isset($currentStockCotton) && count($currentStockCotton)>0)
            <div class="col-md-12 row table-responsive">
                <table class="table table-bordered" id="table_2">
                    <thead>
                        <tr>
                            <th style="width: 7.5%;">Chamber No.</th>
                            <th style="width: 7.5%;">Stack No.</th>
                            <th style="width: 10%;">Client/Borrower Name</th>
                            <th style="width: 15%;">Client/Borrower Name Matching</th>
                            <th style="width: 15%;">Bank Name Matching </th>
                            <th style="width: 12%;">Stack Status 1</th>
                            <th style="width: 15%;">Stack Status 2</th>
                            <th style="width: 10%;">Stack Health Status</th>
                            <th style="width: 8%;">Number Of Bales</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($currentStockCotton as $key => $cs)

                        
                        <input type="hidden" name="current_stock_bags[]" value="{{$cs->current_stock_bags}}"/>
                            <input type="hidden" name="current_stock_qty[]" value="{{$cs->current_stock_qty}}"/>
                        <input type="hidden" name="max_created_at[]" value="{{date('Y-m-d',strtotime($cs->max_created_at))}}"/>    
                        <tr>
                            <td>{{$cs->gdn_no or $cs->chamber_number}}
                                <input type="hidden" name="chamber_number[]" value="{{$cs->gdn_no or $cs->chamber_number}}">
                            </td>
                            <td>{{$cs->stk_no or $cs->stack_number}}
                                <input type="hidden" name="stack_number[]" id="stack_number_{{$key}}" value="{{$cs->stk_no or $cs->stack_number}}"/>
                            </td>
                            <td>{{$cs->client_name}}-{{$cs->current_stock_bags}}
                                <input type="hidden" name="client_name[]" value="{{$cs->client_name}}">
                                <input type="hidden" name="client_id[]" @if(isset($cs->clientId))value="{{$cs->clientId}}" @endif>
                                
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="borrower_name_matching_{{$key}}" name="borrower_name_matching[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                    <option value="Stack Card Displayed and Match">Stack Card Displayed and Match</option>
                                    <option value="Stack Card Displayed and Not Match">Stack Card Displayed and Not Match</option>
                                    <option value="Stack Card Not Displayed">Stack Card Not Displayed</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('borrower_name_matching'))
                                    <p class="help-block">
                                        {{ $errors->first('borrower_name_matching') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="bank_name_matching_{{$key}}" name="bank_name_matching[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                    <option value="Not Applicable">Not Applicable</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                    
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('bank_name_matching'))
                                    <p class="help-block">
                                        {{ $errors->first('bank_name_matching') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2 stackStatus" id="stack_status_1_{{$key}}" name="stack_status_1[{{$key}}]" onchange="getStackStatus2(this.value,'{{$key}}' , '{{$cs->client_name}}')" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                    <option value="Countable">Countable</option>
                                    <option value="Uncountable">Uncountable</option>
                                    <!-- <option value="Under Fumigation">Under Fumigation</option> -->
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('stack_status_1'))
                                    <p class="help-block">
                                        {{ $errors->first('stack_status_1') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="stack_status_2_{{$key}}" name="stack_status_2[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('stack_status_2'))
                                    <p class="help-block">
                                        {{ $errors->first('stack_status_2') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="stack_health_status_{{$key}}" name="stack_health_status[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <!-- <option value="">Select</option> -->
                                    <option value="Average">Average</option>
                                    <option value="Good">Good</option>
                                    <option value="Infestation">Infestation</option>
                                    <option value="Atta Formation">Atta Formation</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('stack_health_status'))
                                    <p class="help-block">
                                        {{ $errors->first('stack_health_status') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <input class="form-control number_of_bags" placeholder=""  name="number_of_bags[{{$key}}]" type="text" id="number_of_bags_{{$key}}" value="{{ $auditinspection->number_of_bags or ''}}">
                                <p class="help-block"></p>
                                @if($errors->has('number_of_bags'))
                                    <p class="help-block">
                                        {{ $errors->first('number_of_bags') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                        </tr>
                       
                        @endforeach

                    </tbody>
                </table>
                <input type="hidden" id="arrayCount" name="arrayCount" value="{{$arrayCount2}}">
            </div> 
            @endif


            @if(isset($currentStock) && count($currentStock)>0)
            <div class="col-md-12 row table-responsive">
                <table class="table table-bordered" id="table_1">
                    <thead>
                        <tr>
                            <th style="width: 7.5%;">Chamber No.</th>
                            <th style="width: 7.5%;">Stack No.</th>
                            <th style="width: 10%;">Client/Borrower Name</th>
                            <th style="width: 15%;">Client/Borrower Name Matching</th>
                            <th style="width: 15%;">Bank Name Matching </th>
                            <th style="width: 12%;">Stack Status 1</th>
                            <th style="width: 15%;">Stack Status 2</th>
                            <th style="width: 10%;">Stack Health Status</th>
                            <th style="width: 8%; display:none;">Approx. Number Of Bags</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($currentStock as $key => $cs)

                        
                        <input type="hidden" name="current_stock_bags[]" value="{{$cs->current_stock_bags}}"/>
                            <input type="hidden" name="current_stock_qty[]" value="{{$cs->current_stock_qty}}"/>
                        <input type="hidden" name="max_created_at[]" value="{{date('Y-m-d',strtotime($cs->max_created_at))}}"/>    
                        <tr id="stack_row_{{$key}}">
                            <td>{{$cs->gdn_no or $cs->chamber_number}}
                                <input type="hidden" name="chamber_number[]" value="{{$cs->gdn_no or $cs->chamber_number}}">
                            </td>
                            <td>{{$cs->stk_no or $cs->stack_number}}
                                <input type="hidden" name="stack_number[]" id="stack_number_{{$key}}" value="{{$cs->stk_no or $cs->stack_number}}"/>
                            </td>
                            <?php 
                                $ccName = str_replace(" ","",$cs->client_name);
                                $ccName = str_replace("(","",$ccName);
                                $ccName = str_replace(")","",$ccName);
                                $ccName = str_replace("&","",$ccName);
                                $ccName = str_replace(".","",$ccName);
                                $ccName = str_replace(",","",$ccName);

                            ?>
                            <td class="btn-link" onclick="myFunctions('null','{{$key}}', '{{$cs->client_name}}'),showPV('{{$cs->stack_number or $cs->stk_no}}','{{$ccName}}','{{$cs->gdn_no or $cs->chamber_number}}','{{$key}}'),stackwiseOverallTotal('{{$key}}')" >{{$cs->client_name}}<!-- -{{$cs->current_stock_bags}} -->
                                <input type="hidden" name="client_name[]" value="{{$cs->client_name}}">
                                <input type="hidden" name="client_id[]" @if(isset($cs->clientId))value="{{$cs->clientId}}" @endif>
                                
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="borrower_name_matching_{{$key}}" name="borrower_name_matching[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <!-- <option value="">Select</option> -->
                                    <option value="Stack Card Displayed and Match">Stack Card Displayed and Match</option>
                                    <option value="Stack Card Displayed and Not Match">Stack Card Displayed and Not Match</option>
                                    <option value="Stack Card Not Displayed">Stack Card Not Displayed</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('borrower_name_matching'))
                                    <p class="help-block">
                                        {{ $errors->first('borrower_name_matching') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="bank_name_matching_{{$key}}" name="bank_name_matching[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <!-- <option value="">Select</option> -->
                                    <option value="Not Applicable">Not Applicable</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                    
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('bank_name_matching'))
                                    <p class="help-block">
                                        {{ $errors->first('bank_name_matching') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2 stackStatus" id="stack_status_1_{{$key}}" name="stack_status_1[{{$key}}]" onchange="myFunctions(this.value,'{{$key}}' , '{{$cs->client_name}}')" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                    <option value="Countable">Countable</option>
                                    <option value="Uncountable">Uncountable</option>
                                    <!-- <option value="Under Fumigation">Under Fumigation</option> -->
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('stack_status_1'))
                                    <p class="help-block">
                                        {{ $errors->first('stack_status_1') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="stack_status_2_{{$key}}" name="stack_status_2[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('stack_status_2'))
                                    <p class="help-block">
                                        {{ $errors->first('stack_status_2') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="stack_health_status_{{$key}}" name="stack_health_status[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <!-- <option value="">Select</option> -->
                                    <option value="Average">Average</option>
                                    <option value="Good">Good</option>
                                    <option value="Infestation">Infestation</option>
                                    <option value="Atta Formation">Atta Formation</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('stack_health_status'))
                                    <p class="help-block">
                                        {{ $errors->first('stack_health_status') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td style="display:none;">
                                <div class="form-group">
                                <input class="form-control number_of_bags" placeholder=""  name="number_of_bags[{{$key}}]" type="text" id="number_of_bags_{{$key}}" value="{{ $auditinspection->number_of_bags or ''}}">
                                <p class="help-block"></p>
                                @if($errors->has('number_of_bags'))
                                    <p class="help-block">
                                        {{ $errors->first('number_of_bags') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                        </tr>
                       
                        @endforeach

                    </tbody>
                </table>
                <input type="hidden" id="arrayCount" name="arrayCount" value="{{$arrayCount}}">
            </div>  

            
            <div id="swipe" @if(!isset($healthStatusArr)) style="display: none" @endif>
                <h6 id="swipe_borrower_name"></h6>
                <div class="col-md-12 row table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            @if(isset($healthStatusArr))
                            <th>Client Name</th>
                            @endif
                            <th>Stack No</th>
                            <th>No of Block(A)</th>
                            <th>Danda(B)</th>
                            <th>Patti(C)</th>
                            <th>(D+P)(D)</th>
                            <th>Height(E)</th>
                            <th>(+ Bags) (F)</th>
                            <th>(- Bags) (G)</th>
                            <th>Total = (A*(B+C)*E)+F-G</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(!isset($healthStatusArr))    
                        @foreach($currentStock as $key => $cs)
                        
                        <tr class="swipe_re_{{$key}}" id="swipe_tr_{{$key}}_0" style="display: none;">
                            
                            <td align="center">
                            <input type="hidden" name="swipe_borrower_name_{{$key}}" value="{{$cs->client_name}}"/>
                            <input readonly="" type="text" name="stackNo_{{$key}}[]" style="width: 50px;" id="stackNo_{{$key}}_0"></td>
                            <td align="center"><input type="number" name="noOfBlock_{{$key}}[]" style="width: 50px;" id="noOfBlock_{{$key}}_0" readonly="" onkeyup="calculation('{{$key}}',0)"></td>
                            <td align="center"><input type="number" name="danda_{{$key}}[]" style="width: 50px;" id="danda_{{$key}}_0" readonly="" onkeyup="calculation('{{$key}}',0)"></td>
                            <td align="center"><input type="number" name="patti_{{$key}}[]" style="width: 50px;" id="patti_{{$key}}_0" readonly="" onkeyup="calculation('{{$key}}',0)"></td>
                            <td align="center"><input type="number" name="d_{{$key}}[]" style="width: 50px;" id="d_{{$key}}_0" readonly=""></td>
                            <td align="center"><input type="number" name="height_{{$key}}[]" style="width: 50px;" id="height_{{$key}}_0" readonly="" onkeyup="calculation('{{$key}}',0)"></td>
                            <td align="center"><input type="number" name="bagsF_{{$key}}[]" style="width: 50px;" id="bagsF_{{$key}}_0" readonly="" onkeyup="calculation('{{$key}}',0)"></td>
                            <td align="center"><input type="number" name="bagsG_{{$key}}[]" style="width: 50px;" id="bagsG_{{$key}}_0" readonly="" onkeyup="calculation('{{$key}}',0)"></td>
                            <td align="center"><input type="number" name="total_{{$key}}[]" style="width: 50px;" id="total_{{$key}}_0" readonly="" class="stackwise_total"></td>
                            <td><button type="button" id="swipe_re_{{$key}}_0" class="btn btn-info" onclick="addmore('{{$key}}')">Add More</button></td>
                        
                        </tr>
                        @endforeach
                        @else

                        <?php $i=-1;$j=-1;$k=0;$flag=true;
                             $borrowerName='';$stackNo=NULL;$chamNo=NULL;
                            ?>      
                        @foreach($healthStatusArr as $key => $cs)
                            @if($cs['borrower_name']==$borrowerName)
                                <?php $i=$i; ?>
                            @else
                                <?php $i++;?>                           
                            @endif
                            <?php 
                                $borrowerName = $cs['borrower_name'];
                            ?>

                            @if(($cs['stack_no']==$stackNo) && ($cs['borrower_name']==$bName) && ($cs['cham_no']==$chamNo))
                                <?php $j=$j;$k++; 
                                      $flag=false;  
                                ?>
                            @else
                                <?php $j++;$flag=true;$k=0;
                                    $cName = str_replace(" ","",$cs['borrower_name']);
                                    $cName = str_replace("(","",$cName);
                                    $cName = str_replace(")","",$cName);
                                    $cName = str_replace("&","",$cName);
                                    $cName = str_replace(".","",$cName);
                                    $cName = str_replace(",","",$cName);
                                ?>  
                                <input type="hidden" name="borrower_name_{{$j}}"  value="{{$cs['borrower_name']}}" id="borrower_name_{{$j}}">
                                <input type="hidden" name="chamberwise_stock_id_{{$j}}" value="{{$cs['chamberwise_stock_id']}}" id="chamberwise_stock_id_{{$j}}">
                                <input type="hidden" name="cham_no_{{$j}}" value="{{$cs['cham_no']}}" id="cham_no_{{$j}}">

                            @endif
                            <?php 
                                $stackNo = $cs['stack_no'];
                                $bName = $cs['borrower_name'];
                                $chamNo = $cs['cham_no']; 
                            ?>

                            
                        
                        <tr class="tr_{{$cs['stack_no']}}_{{$cName}}_{{$cs['cham_no']}} hidetr swipe_re_{{$j}}" style="display: none;" id="swipe_tr_{{$j}}_{{$k}}">
                            <td align="center">
                                {{$cs['borrower_name']}}
                            </td>
                            <td align="center"><input readonly="" type="text" name="stackNo_{{$j}}[]" style="width: 50px;" value="{{$cs['stack_no']}}" id="stackNo_{{$j}}_{{$k}}"></td>
                            <td align="center"><input type="number" name="noOfBlock_{{$j}}[]" style="width: 50px;" value="{{$cs['no_of_block']}}" id="noOfBlock_{{$j}}_{{$k}}" onkeyup="calculation('{{$j}}',0)"></td>
                            <td align="center"><input type="number" name="danda_{{$j}}[]" style="width: 50px;"  value="{{$cs['danda']}}" id="danda_{{$j}}_{{$k}}" onkeyup="calculation('{{$j}}',0)"></td>
                            <td align="center"><input type="number" name="patti_{{$j}}[]" style="width: 50px;" value="{{$cs['patti']}}" id="patti_{{$j}}_{{$k}}" onkeyup="calculation('{{$j}}',0)"></td>
                            <td align="center"><input type="number" name="d_{{$j}}[]" style="width: 50px;" value="{{$cs['danda_plus_patti']}}" id="d_{{$j}}_{{$k}}"></td>
                            <td align="center"><input type="number" name="height_{{$j}}[]" style="width: 50px;" value="{{$cs['height']}}" id="height_{{$j}}_{{$k}}" onkeyup="calculation('{{$j}}',0)"></td>
                            <td align="center"><input type="number" name="bagsF_{{$j}}[]" style="width: 50px;" value="{{$cs['plus_bags']}}" id="bagsF_{{$j}}_{{$k}}" onkeyup="calculation('{{$j}}',0)"></td>
                            <td align="center"><input type="number" name="bagsG_{{$j}}[]" style="width: 50px;" value="{{$cs['minus_bags']}}" id="bagsG_{{$j}}_{{$k}}" onkeyup="calculation('{{$j}}',0)"></td>
                            <td align="center"><input type="number" name="total_{{$j}}[]" style="width: 50px;" value="{{$cs['total']}}" id="total_{{$j}}_{{$k}}"  class="stackwise_total"></td>
                            @if($flag)
                                <td style="display: block ruby">
                                <button type="button" id="swipe_re_{{$j}}_{{$k}}" class="btn btn-info" onclick="editaddmore('{{$j}}')">Add More</button> 
                                <button type="button" id="swipe_re_{{$j}}_{{$k}}" class="btn btn-primary" onclick="savepv(this)">Save</button>
                                </td>
                            @else
                                 <td>
                                    <button type="button" id="swipe_re_{{$j}}_{{$k}}" class="btn btn-danger" onclick="" value="">Remove</button>
                                </td>                        
                            @endif
                            
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
               
                <div class="col-md-2 form-group pull-right">
                    <label class="control-label">Overall Total</label>
                    <input class="form-control" placeholder="" name="" type="text" id="overall_total" value="">
                    
                </div>
                <div class="col-md-3 form-group pull-left">
                    <label class="control-label">Stackwise Overall Total</label>
                    <input class="form-control" placeholder="" name="" type="text" id="stackwiseOverall_total" value="">
                    
                </div>
            </div>
            </div>
            @endif

            @if(isset($currentStockCm) && (count($currentStockCm)>0))
            <div class="col-md-12 row table-responsive">
                <table class="table table-bordered" id="table_2">
                    <thead>
                        <tr>
                            <th style="width:6%;">Name of Bank</th>
                            <th style="width:6%;">Client/ Borrower Name</th>
                            <th style="width:6%;">Number of Bags</th>
                            <th style="width:6%;">Quantity</th>
                            <th style="width:13%;">Client/Borrower Name Matching</th>
                            <th style="width:13%;">Bank Name Matching </th>
                            <th style="width:13%;">Stack Status 1</th>
                            <th style="width:15%;">Stack Status 2</th>
                            <th style="width:12%;">Stack Health Status</th>
                            <th style="width:7%;">Funded Number Of Bags</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($currentStockCm as $key => $cs)
                        <tr>
                            <td>  {{$cs->client_name_cm or $cs->client_name}}
                                <input type="hidden" name="client_name_cm[]" value="{{$cs->client_name_cm or $cs->client_name}}">
                                <input type="hidden" name="client_id_cm[]" value="{{$cs->clientId_cm or $cs->client_id}}">
                                
                            </td>
                            <td>
                                {{$cs->borrowerName_cm or $cs->borrower_name}}
                                <input type="hidden" name="borrowerName_cm[]" value="{{$cs->borrowerName_cm or $cs->borrower_name }}"/>
                            </td>
                            <td>
                                {{$cs->closing_bag }}
                                <input type="hidden" name="closing_bag[]" value="{{$cs->closing_bag}}"/>
                            </td>
                            <td>
                                {{$cs->closing_qty}}
                                <input type="hidden" name="closing_qty[]" value="{{$cs->closing_qty}}"/>
                            </td>                          
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="borrower_name_matching_cm_{{$key}}" name="borrower_name_matching_cm[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                    <option value="Stack Card Displayed and Match">Stack Card Displayed and Match</option>
                                    <option value="Stack Card Displayed and Not Match">Stack Card Displayed and Not Match</option>
                                    <option value="Stack Card Not Displayed">Stack Card Not Displayed</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('borrower_name_matching_cm'))
                                    <p class="help-block">
                                        {{ $errors->first('borrower_name_matching_cm') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="bank_name_matching_cm_{{$key}}" name="bank_name_matching_cm[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                    <option value="Pledge Card Displayed">Pledge Card Displayed</option>
                                    <option value="Pledge Card Not Displayed">Pledge Card Not Displayed</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('bank_name_matching_cm'))
                                    <p class="help-block">
                                        {{ $errors->first('bank_name_matching_cm') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2 stackStatus" id="stack_status_1_cm_{{$key}}" onchange="getStackStatus2Cm(this.value,'{{$key}}')" name="stack_status_1_cm[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                    <option value="Countable">Countable</option>
                                    <option value="Uncountable">Uncountable</option>
                        
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('stack_status_1_cm'))
                                    <p class="help-block">
                                        {{ $errors->first('stack_status_1_cm') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="stack_status_2_cm_{{$key}}" name="stack_status_2_cm[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <option value="">Select</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('stack_status_2_cm'))
                                    <p class="help-block">
                                        {{ $errors->first('stack_status_2_cm') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control select2" id="stack_health_status_cm_{{$key}}" name="stack_health_status_cm[{{$key}}]" tabindex="-1" aria-hidden="true">
                                    <!-- <option value="">Select</option> -->
                                    <option value="Average">Average</option>
                                    <option value="Good">Good</option>
                                    <option value="Infestation">Infestation</option>
                                    <option value="Atta Formation">Atta Formation</option>
                                </select>
                                <p class="help-block"></p>
                                @if($errors->has('stack_health_status_cm'))
                                    <p class="help-block">
                                        {{ $errors->first('stack_health_status_cm') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <input class="form-control" placeholder=""  name="number_of_bags_cm[{{$key}}]" type="text" id="number_of_bags_cm_{{$key}}" value="{{ $auditinspection->number_of_bags_cm or ''}}">
                                <p class="help-block"></p>
                                @if($errors->has('number_of_bags_cm'))
                                    <p class="help-block">
                                        {{ $errors->first('number_of_bags_cm') }}
                                    </p>
                                @endif
                            </div>
                            </td>
                        </tr>
                       
                        @endforeach

                    </tbody>
                </table>
                <input type="hidden" id="arrayCount1" name="arrayCount1" value="{{$arrayCount1}}">
            </div>
            
            @endif 

            
            </div>
            </div>

        </div>
        </div>

        <div class="col-xs-12 row">
            <div class="form-group">
                @if(isset($auditinspection))
                    <button type="submit" class="btn btn-info pull-left">Update</button>
                @else
                     <button type="submit" class="btn btn-info pull-left">Add</button>
                @endif        
            </div>
            <div class="clearfix"></div>
        </div>

    </form>
@stop
@section('javascript')
<script src="{{ url('adminlte/plugins/jqueryvalidation/jquery.validate.min.js') }}"></script>    

    <script type="text/javascript">
        $("#auditFormEdit").submit(function()
    {         
        $("[name^=stackNo_]").each(function () {
            $(this).remove();
        });

        $("[name^=noOfBlock_]").each(function () {
            $(this).remove();
        });

        $("[name^=danda_]").each(function () {
            $(this).remove();
        });    

        $("[name^=patti_]").each(function () {
            $(this).remove();
        });

        $("[name^=d_]").each(function () {
            $(this).remove();
        });

        $("[name^=height_]").each(function () {
            $(this).remove();
        });

        $("[name^=bagsF_]").each(function () {
            $(this).remove();
        });

        $("[name^=bagsG_]").each(function () {
            $(this).remove();
        });

        $("[name^=total_]").each(function () {
            $(this).remove();
        });
        //alert('submitted');
        return true;
    });

  
      $('#warehouse_start_date,#last_transaction_date').datepicker({
        autoclose: true,
        format:"dd-mm-yyyy"
      });

    function savepv(element){
    //alert('test');   
    var elementParts = element.id.split("_");
    var num = elementParts[2];
    //var borrowerName = document.getElementById('borrower_name'+num);
    var chammberwiseStockId = document.getElementById('chamberwise_stock_id_'+num).value;
    var stackNo = document.querySelectorAll('[name="stackNo_'+num+'[]"]');
    var stackNoArr = [];
    stackNo.forEach(function(sn,i){
        if(sn.value!=""){
        stackNoArr[i] = sn.value;
        }else{
        stackNoArr[i] = null;    
        }
    });

    var noOfBlock = document.querySelectorAll('[name="noOfBlock_'+num+'[]"]');
    var noOfBlockArr = [];
    noOfBlock.forEach(function(sn,i){
        if(sn.value!=""){
        noOfBlockArr[i] = sn.value;
        }else{
        noOfBlockArr[i] = null;    
        }
    });

    var danda = document.querySelectorAll('[name="danda_'+num+'[]"]');
    var dandaArr = [];
    danda.forEach(function(sn,i){
        if(sn.value!=""){
        dandaArr[i] = sn.value;
        }else{
        dandaArr[i] = null;    
        }
    });

    var patti = document.querySelectorAll('[name="patti_'+num+'[]"]');
    var pattiArr = [];
    patti.forEach(function(sn,i){
        if(sn.value!=""){
        pattiArr[i] = sn.value;
        }else{
        pattiArr[i] = null;    
        }
    });

    var d = document.querySelectorAll('[name="d_'+num+'[]"]');
    var dArr = [];
    d.forEach(function(sn,i){
        if(sn.value!=""){
        dArr[i] = sn.value;
        }else{
        dArr[i] = null;    
        }
    });

    var height = document.querySelectorAll('[name="height_'+num+'[]"]');
    var heightArr = [];
    height.forEach(function(sn,i){
        if(sn.value!=""){
        heightArr[i] = sn.value;
        }else{
        heightArr[i] = null;    
        }
    });

    var bagsF = document.querySelectorAll('[name="bagsF_'+num+'[]"]');
    var bagsFArr = [];
    bagsF.forEach(function(sn,i){
        if(sn.value!=""){
        bagsFArr[i] = sn.value;
        }else{
        bagsFArr[i] = null;    
        }
    });

    var bagsG = document.querySelectorAll('[name="bagsG_'+num+'[]"]');
    var bagsGArr = [];
    bagsG.forEach(function(sn,i){
        if(sn.value!=""){
        bagsGArr[i] = sn.value;
    }else{
        bagsGArr[i] = null;
    }
    });

    var total = document.querySelectorAll('[name="total_'+num+'[]"]');
    var totalArr = [];
    total.forEach(function(sn,i){
        if(sn.value!=""){
        totalArr[i] = sn.value;
    }else{
        totalArr[i] = null;
    }
    });
    //console.log(totalArr);

    var _token = "<?php echo csrf_token();?>";   
        $.ajax({
            type: "POST",
            url: "{{ route('auditinspection.savepv') }}",
            data:{_token:_token,chamberwise_stock_id:chammberwiseStockId,stackNo:stackNoArr,noOfBlock:noOfBlockArr,danda:dandaArr,patti:pattiArr,d:dArr,height:heightArr,bagsF:bagsFArr,bagsG:bagsGArr,total:totalArr},
            dataType:"json",
            success: function(response){
            //console.log(response);              
            if(response.value=='Success'){
                alert('Data Updated Successfully');                
                $('#stack_row_'+num).css("background",'lightgreen');               
            }else{
                alert('No Data Updated');
            }
        }
    });
}


      


        $(document).ready(function () {

              $(window).keydown(function(event){
                if(event.keyCode == 13) {
                  event.preventDefault();
                  return false;
                }
              });
            

            $('#auditForm,#auditFormEdit').validate({ 
                rules: {
                    location_closed: {
                        required: true,
                    },
                    location_not_started_before_audit: {
                        required: true,
                    },
                    cm_available_during_visit: {
                        required: true,
                    },
                    whole_warehouse_under_fumigation:{
                        required: true,
                    },
                    warehouse_code: {
                        required: true,
                    },
                    state: {
                        required: true,
                    },
                    auditor_name: {
                        required: true,
                    },
                    name_of_warehouse: {
                        required: true,
                    },
                    location_name: {
                        required: true,
                    },
                    warehouse_start_date: {
                        required: true,
                    },
                    type_of_warehouse: {
                        required: true,
                    },
                    type_of_structure:{
                        required:true,
                    },
                    who_has_the_control_on_lock_and_key: {
                        required: true,
                    },
                    
                    
                },
                // highlight: function (element) {
                //     $(element).parent().addClass('error')
                // },
                // unhighlight: function (element) {
                //     $(element).parent().removeClass('error')
                // }
            });

            $("[name^=borrower_name_matching]").each(function () {
                $(this).rules("add", {
                    required: true,
                });
            });

            $("[name^=bank_name_matching]").each(function () {
                $(this).rules("add", {
                    required: true,
                });
            });

            $("[name^=stack_status_1]").each(function () {
                $(this).rules("add", {
                    required: true,
                });
            });

            $("[name^=stack_status_2]").each(function () {
                $(this).rules("add", {
                    required: true,
                });
            });

            $("[name^=stack_health_status]").each(function () {
                $(this).rules("add", {
                    required: true,
                });
            });

            $("[name^=number_of_bags]").each(function () {
                $(this).rules("add", {
                    required: true,
                });
            });

        });


        function hideShowSection(val,sectionIds){

          if(val.trim()=='No'){
                sectionIds.forEach(function(ele){
                    $('#'+ele).show();
                });
            }else{
                sectionIds.forEach(function(ele){
                    $('#'+ele).hide();
                });
            }
        }


        function hideShow(val){
            //console.log(val);

            if(val.trim()=='No'){
                $('#cmNotAvailable').show();
                $('#section2').hide();
                $('#section22').hide();    
            }else{
                $('#cmNotAvailable').hide();
                $('#section2').show();
                $('#section22').show();
            }
        }

        function showPV(stackNo,clientName,cham_no,key)
        {
           // console.log(stackNo+"--"+clientName);
           var val = $("#stack_status_1_"+key).val();
            if(val=='Countable'){
                $(".hidetr").hide();
                $(".tr_"+stackNo+'_'+clientName+'_'+cham_no).show();
            }
        }


        function myFunctions(val,key,borrower){

            var $header_row = $('#table_1').find('tr').find('th:nth-child(9)');
            var $data_row = $('#table_1').find('tr').find('td:nth-child(9)');
            //$header_row.hide();
            //$data_row.hide();    
            //$('.number_of_bags').show();
            // select2-stack_status_1_0-container
            // $("stack_status_1_"+key+"-container").val()
            
            if(val == '' || val == 'null' || val == undefined)
            {
                var val = $("#stack_status_1_"+key).val();
            }

            

            if(val == '' || val == undefined)
            {
                alert('Select Stack Status');
                //die();
            }

            if(val=='Countable'){
                $header_row.show();
                $data_row.show();
                

                $(".stackStatus").each(function() {
                    var index = $(this).attr('id').split('_')[3];
                    if($(this).val()=='Countable' && $(this).val()!=''){
                         $('#number_of_bags_'+index).val("");
                         $('#number_of_bags_'+index).hide();   
                        }else{
                            $('#number_of_bags_'+index).show();
                        }                        
                });
                
                
                abc(key,borrower,val);
                getStackStatus2(val,key);
            }else{
                //alert(val);
                $header_row.show();
                $data_row.show();

                $(".stackStatus").each(function() {
                    var index = $(this).attr('id').split('_')[3];
                    if($(this).val() =='Uncountable' && $(this).val()!=''){
                         
                         $('#number_of_bags_'+index).show();   
                        }else{
                            //$('#number_of_bags_'+index).val("");
                            $('#number_of_bags_'+index).hide();
                        }                        
                });

                //$('#number_of_bags_'+key).show();

                //$("#swipe").hide()
                getStackStatus2(val,key);
            }

            
            $("#stackNo_"+key+"_0").val($("#stack_number_"+key).val());
            
        }

        var stackStatus = {
                Countable: ["Countable", "Countable Top Side And Wall Touch 4 Side", "Countable Wall Touch 2 Side", "Countable Wall Touch 3 Side","Countable Fallen"],
                Uncountable: ["Uncountable Inward/Outward Running","Uncountable Under Fumigation","Uncountable Fallen", "Uncountable Wall Touch"],
            };

        function getStackStatus2(val,key){
            
            if (val.length == 0) document.getElementById("stack_status_2_"+key).innerHTML = "<option>Select</option>";
            else {
                var stackStatusOpt = "";

                for (ss in stackStatus[val]) {
                    stackStatusOpt += "<option value='"+stackStatus[val][ss]+"'>" + stackStatus[val][ss] + "</option>";
                }
                document.getElementById("stack_status_2_"+key).innerHTML = stackStatusOpt;
            }

            
        }

        function getStackStatus2Cm(val,key){
            
            if (val.length == 0) document.getElementById("stack_status_2_cm_"+key).innerHTML = "<option>Select</option>";
            else {
                var stackStatusOpt = "";

                for (ss in stackStatus[val]) {
                    stackStatusOpt += "<option value='"+stackStatus[val][ss]+"'>" + stackStatus[val][ss] + "</option>";
                }
                document.getElementById("stack_status_2_cm_"+key).innerHTML = stackStatusOpt;
            }

            
        }



     

      function abc(key,borrower,val)
      { 
        // alert(val);
        if(val=='Countable'){
        $("#swipe").show();
        $(".swipe_re_"+key+"").show();

        var count = $("#arrayCount").val();
        $("#swipe_borrower_name").html('Borrower Name -'+borrower);
        @if(!isset($healthStatusArr))
        
        @endif
        for (i = 0; i < count; i++) {
          if(i!=key)
          {
            $(".swipe_re_"+i+"").hide();
          }
            
        }
        }
        
      }

      function addmore(key)
      {
        var $tr    = $(".swipe_re_"+key+"").last();

        var id = $('[id^="swipe_re_'+key+'_"]:last');

        var num = parseInt(id.prop("id").substr(id.prop("id").lastIndexOf('_') + 1))+1;

        var $clone = $tr.clone();

        $clone.find("#swipe_re_"+key+"_"+(num-1)).attr("id","swipe_re_"+key+"_"+(num));
        $clone.find("#swipe_tr_"+key+"_"+(num-1)).attr("id","swipe_tr_"+key+"_"+(num));
        $clone.find("#noOfBlock_"+key+"_"+(num-1)).attr("id","noOfBlock_"+key+"_"+(num));

        var clickfun = $clone.find("#noOfBlock_"+key+"_"+(num)).attr("onkeyup");
        var funname = clickfun.substring(0,clickfun.indexOf("("));       
        $clone.find("#noOfBlock_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");
        
        $clone.find("#stackNo_"+key+"_"+(num-1)).attr("id","stackNo_"+key+"_"+(num));
        $clone.find("#danda_"+key+"_"+(num-1)).attr("id","danda_"+key+"_"+(num));
        $clone.find("#danda_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#patti_"+key+"_"+(num-1)).attr("id","patti_"+key+"_"+(num));
        $clone.find("#patti_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#d_"+key+"_"+(num-1)).attr("id","d_"+key+"_"+(num));
        $clone.find("#height_"+key+"_"+(num-1)).attr("id","height_"+key+"_"+(num));
        $clone.find("#height_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#bagsF_"+key+"_"+(num-1)).attr("id","bagsF_"+key+"_"+(num));
        $clone.find("#bagsF_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#bagsG_"+key+"_"+(num-1)).attr("id","bagsG_"+key+"_"+(num));
        $clone.find("#bagsG_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#total_"+key+"_"+(num-1)).attr("id","total_"+key+"_"+(num));
        // $clone.find(':input').val('');
        $clone.find(':input:not([name="stackNo_'+key+'[]"])').val('');
        $clone.find(':button').attr("class","btn btn-danger");
        $clone.find(':button').html("Remove");
        $clone.find(':button').attr("onclick", "").unbind("click");
        $clone.find(':button').on('click', remove);
        $tr.after($clone);
        
      }

      function editaddmore(key)
      {
        var $tr    = $(".swipe_re_"+key+"").last();

        var id = $('[id^="swipe_re_'+key+'_"]:last');

        var num = parseInt(id.prop("id").substr(id.prop("id").lastIndexOf('_') + 1))+1;
        var $clone = $tr.clone();

        $clone.find("#swipe_re_"+key+"_"+(num-1)).attr("id","swipe_re_"+key+"_"+(num));
        $clone.find("#swipe_tr_"+key+"_"+(num-1)).attr("id","swipe_tr_"+key+"_"+(num));
        $clone.find("#noOfBlock_"+key+"_"+(num-1)).attr("id","noOfBlock_"+key+"_"+(num));

        var clickfun = $clone.find("#noOfBlock_"+key+"_"+(num)).attr("onkeyup");
        var funname = clickfun.substring(0,clickfun.indexOf("("));       
        $clone.find("#noOfBlock_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");
        
        $clone.find("#stackNo_"+key+"_"+(num-1)).attr("id","stackNo_"+key+"_"+(num));
        $clone.find("#danda_"+key+"_"+(num-1)).attr("id","danda_"+key+"_"+(num));
        $clone.find("#danda_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#patti_"+key+"_"+(num-1)).attr("id","patti_"+key+"_"+(num));
        $clone.find("#patti_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#d_"+key+"_"+(num-1)).attr("id","d_"+key+"_"+(num));
        $clone.find("#height_"+key+"_"+(num-1)).attr("id","height_"+key+"_"+(num));
        $clone.find("#height_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#bagsF_"+key+"_"+(num-1)).attr("id","bagsF_"+key+"_"+(num));
        $clone.find("#bagsF_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#bagsG_"+key+"_"+(num-1)).attr("id","bagsG_"+key+"_"+(num));
        $clone.find("#bagsG_"+key+"_"+(num)).attr("onkeyup",funname+"("+key+","+""+num+")");

        $clone.find("#total_"+key+"_"+(num-1)).attr("id","total_"+key+"_"+(num));
        // $clone.find(':input').val('');
        $clone.find(':input:not([name="stackNo_'+key+'[]"])').val('');
        $clone.find(':button').attr("class","btn btn-danger");
        $clone.find(':button').html("Remove");
        $clone.find(':button').attr("onclick", "").unbind("click");
        $clone.find(':button').on('click', remove);
        var buttons = $clone.find(':button');
        if (buttons[1] === null || buttons[1] === undefined) {
          delete buttons[1];
        }else{
            buttons[1].remove();
        }
        $tr.after($clone);
        
      }

      function remove(val)
      {
        $(this).parent().parent().remove();
      }

      function overallTotal(){
        $('#overall_total').val("");
        var sum = 0;
        var x = document.getElementsByClassName("stackwise_total");
        for(var i = 0; i<x.length; i++){
           if(x[i].value !='') 
            sum += parseInt(x[i].value);
        }
        $('#overall_total').val(sum);

      }

      function stackwiseOverallTotal(key){
        $('#stackwiseOverall_total').val("");
        var sum = 0;
        var x = $("[id^='total_"+key+"']");
        //console.log(x);
        for(var i = 0; i<x.length; i++){
           if(x[i].value !='') 
            sum += parseInt(x[i].value);
        }
        //console.log(sum)
        $('#stackwiseOverall_total').val(sum);

      }

      function calculation(key,num)
      {
        
        var noOfBlock = $("#noOfBlock_"+key+"_"+(num)).val() || 1;
        var danda = $("#danda_"+key+"_"+(num)).val() || 0;
        var patti = $("#patti_"+key+"_"+(num)).val() || 0;
        var danda_plus_patti = parseInt(danda) + parseInt(patti);
        var height = $("#height_"+key+"_"+(num)).val() || 1;
        var plus_bags = $("#bagsF_"+key+"_"+(num)).val() || 0;
        var minus_bags = $("#bagsG_"+key+"_"+(num)).val() || 0;
        //console.log(noOfBlock + "--" +danda + "--"+ patti);
        var total = parseInt((parseInt(noOfBlock) * danda_plus_patti * parseInt(height)) + parseInt(plus_bags) - parseInt(minus_bags)); 
        //console.log(total);
        $("#d_"+key+"_"+(num)).val(danda_plus_patti);
        $("#total_"+key+"_"+(num)).val(total);
        overallTotal();
        stackwiseOverallTotal(key)
        
      }


//To set values in edit form or when validation fails

        @if(isset($auditinspection)/* or (old()!==null)*/)
        $('#location_closed').val("{{$auditinspection->location_closed or old('location_closed')}}").trigger('change');
        $('#location_not_started_before_audit').val("{{$auditinspection->location_not_started_before_audit or old('location_not_started_before_audit')}}").trigger('change');
        $('#cm_available_during_visit').val("{{$auditinspection->cm_available_during_visit or old('cm_available_during_visit')}}").trigger('change');
        $('#whole_warehouse_under_fumigation').val("{{$auditinspection->whole_warehouse_under_fumigation or old('whole_warehouse_under_fumigation')}}").trigger('change');
        $('#type_of_structure').val("{{$auditinspection->type_of_structure or old('type_of_structure')}}").trigger('change');
        $('#who_has_the_control_on_lock_and_key').val("{{$auditinspection->who_has_the_control_on_lock_and_key or old('who_has_the_control_on_lock_and_key')}}").trigger('change');
        
        
    @foreach($currentStock as $key=>$cs)    
        $('#borrower_name_matching_{{$key}}').val("{{$cs->borrower_name_matching}}").trigger('change');
        $('#bank_name_matching_{{$key}}').val("{{$cs->bank_name_matching}}").trigger('change');
        $('#stack_status_1_{{$key}}').val("{{$cs->stack_status_1}}").trigger('change');
        $('#stack_status_2_{{$key}}').val("{{$cs->stack_status_2}}").trigger('change');
        $('#stack_health_status_{{$key}}').val("{{$cs->stack_health_status}}").trigger('change');
        $('#number_of_bags_{{$key}}').val("{{$cs->number_of_bags}}").trigger('change');
    @endforeach 
    
    @foreach($currentStockCm as $key=>$cs)    
        $('#borrower_name_matching_cm_{{$key}}').val("{{$cs->borrower_name_matching}}").trigger('change');
        $('#bank_name_matching_cm_{{$key}}').val("{{$cs->bank_name_matching}}").trigger('change');
        $('#stack_status_1_cm_{{$key}}').val("{{$cs->stack_status_1}}").trigger('change');
        $('#stack_status_2_cm_{{$key}}').val("{{$cs->stack_status_2}}").trigger('change');
        $('#stack_health_status_cm_{{$key}}').val("{{$cs->stack_health_status}}").trigger('change');
        $('#number_of_bags_cm_{{$key}}').val("{{$cs->number_of_bags}}").trigger('change');
    @endforeach

@endif
      
      
    </script>    
@endsection



