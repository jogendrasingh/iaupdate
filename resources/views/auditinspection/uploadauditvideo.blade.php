@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
<?php 
    $role = Auth::user()->roles->pluck('name')->first();
?>

@section('content')
    <h3 class="page-title">Audit Inspection</h3>
    
    <div class="clearfix"></div>
    <label></label>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    

    <div class="panel panel-default">
        <div class="panel-heading">
           Upload Audit Video
        </div>
        <div class="container">
        @if(isset($auditvideos))
            <form method="POST" action="{{ route('auditinspection.updateauditvideo',[$auditvideos->id]) }}" enctype="multipart/form-data" id="videoForm">
        @else
            <form method="POST" action="{{ route('auditinspection.saveauditvideo') }}" enctype="multipart/form-data" id="videoForm">
        @endif

        <input type="hidden" name="_method" value="{{ isset($auditvideos) ? 'PUT' : 'POST' }}">    
        {{csrf_field()}}
        <div class="panel-body">
                
            <div class="col-md-12 row">    
                <div class="col-md-6 form-group">
                    <label for="audit_videos" class="control-label">Audit Videos*</label>
                    <div class="table-responsive">  
                        <input type="hidden" name="inspection_id" value="<?=$id?>">
                        <table class="table table-bordered" id="dynamic_field">
                         
                            <tr>  
                                <td>
                                    <input type="file" name="audit_videos[]"  class="form-control" id="video_0"/>
                                    @if(isset($auditvideos->video_1) && $auditvideos->video_1 !='')
                                    <a href="{{$auditvideos->video_1}}" target="_blank">Watch <i class="fa fa-play" aria-hidden="true"></i></a>
                                    @endif                                                        
                                </td>  
                                <td>
                                    <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                                </td>  
                            </tr>
                            @if(isset($auditvideos->video_2) && $auditvideos->video_2 !='')
                            <tr id="row2">
                                <td>
                                    <input type="file" name="audit_videos[]" class="form-control" id="video_1">
                                    <a href="{{$auditvideos->video_2}}" target="_blank">Watch <i class="fa fa-play" aria-hidden="true"></i></a>
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                            @endif
                            @if(isset($auditvideos->video_3) && $auditvideos->video_3 !='')
                            <tr id="row3">
                                <td>
                                    <input type="file" name="audit_videos[]" class="form-control" id="video_2">
                                    <a href="{{$auditvideos->video_3}}" target="_blank">Watch <i class="fa fa-play" aria-hidden="true"></i></a>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            @endif
                            @if(isset($auditvideos->video_4) && $auditvideos->video_4 !='')
                            <tr id="row4">
                                <td>
                                    <input type="file" name="audit_videos[]" class="form-control" id="video_3">
                                    <a href="{{$auditvideos->video_4}}" target="_blank">Watch <i class="fa fa-play" aria-hidden="true"></i></a>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            @endif
                            @if(isset($auditvideos->video_5) && $auditvideos->video_5 !='')
                            <tr id="row5">
                                <td>
                                    <input type="file" name="audit_videos[]" class="form-control" id="video_4">
                                    <a href="{{$auditvideos->video_5}}" target="_blank">Watch <i class="fa fa-play" aria-hidden="true"></i></a>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            @endif
                             
                       </table>    
                    </div>
                    
                </div>
                
            </div>
            
        </div>

        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
        <br>
        </div>
        
    </div>
@stop

@section('javascript') 
    <script type="text/javascript">
        //To dynamically add image tag
      //var i=1;
      var noOfVideos =0;  
      $('#add').click(function(){ 
        var noOfVideos = document.querySelectorAll('input[type="file"]'); 
        if(noOfVideos.length<5){
           //i++;  
           $('#dynamic_field').append(
            '<tr id="row'+(noOfVideos.length+1)+'"><td><input type="file" name="audit_videos[]"  class="form-control" /></td><td><button type="button" name="remove" id="'+(noOfVideos.length+1)+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        }else{
            alert('You can\'t upload more than 5 videos');
        }  
      });  

      //to dynamically remove image tag
      $(document).on('click', '.btn_remove', function(){
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });
    </script>
@endsection