@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
<?php 
    $role = Auth::user()->roles->pluck('name')->first();
?>

@section('content')
    <h3 class="page-title">Audit Inspection</h3>
    
    <div class="clearfix"></div>
    <label></label>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($assignedaudit) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Warehouse Code</th>
                        <th>State</th>
                        <th>Name of Warehouse</th>
                        <th>Location</th>
                        <th>Warehouse Type</th>
                        <th>Action</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($assignedaudit) > 0)
                        @foreach ($assignedaudit as $aa)
                            <tr data-entry-id="{{ $aa->id }}">
                                <td>{{ $aa->id }}</td>
                                <td>{{ $aa->warehouse_code }}</td>
                                <td>{{ $aa->state }}</td>
                                <td>{{ $aa->name_of_warehouse }}</td>
                                <td>{{ $aa->location_name }}</td>
                                <td>{{ $aa->type_of_warehouse }}</td>
                                <td>
                                    <?php
                                        if($role=='maker'){
                                    ?>
                                    <a href="{{ route('auditinspection.add',[$aa->id]) }}" class="btn btn-xs btn-info">Add</a>
                                    <?php } ?>
                                    

                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>


        
    </div>
@stop

@section('javascript') 
    
@endsection