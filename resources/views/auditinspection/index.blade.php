<?php 
    $role = Auth::user()->roles->pluck('name')->first();
?>
@inject('HelperTrait', 'App\Http\Controllers\AuditInspectionController')
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')


@section('content')
    <h3 class="page-title">Audit Inspection</h3>
    @role('maker')
    <p>
        <a href="{{ route('auditinspection.list') }}" class="btn btn-success pull-right">Start Audit</a>
    </p>
    @endrole
    @role('guest')
    <p>
        <a href="{{ route('importexcel.dtrdownload') }}" class="btn btn-success" >DTR Stock</a>
        <a href="{{ route('importexcel.cmdownload') }}" class="btn btn-success" >CM Stock</a>
    </p>
    @endrole
    <div class="clearfix"></div>
    <label></label>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <form method="GET" action="{{ route('auditinspection.index') }}" id="searchForm">
    <div class="col-md-12 row">
        <div class="col-md-5 form-group">
            <label for="from_date" class="control-label">From Date*</label>
            <input class="form-control searchDate" placeholder="" name="from_date" type="text" value="{{ old('from_date', $from_date) }}">
        </div>

        <div class="col-md-5 form-group">
            <label for="to_date" class="control-label">To Date*</label>
            <input class="form-control searchDate" placeholder="" name="to_date" type="text" value="{{ old('to_date', $to_date) }}">
        </div>

        <div class="col-md-2 form-group">
            <br>
            <button type="submit" class="btn btn-info">Submit</button>
        </div>

        
    </div>
    <div class="clearfix"></div>
    </form>

    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($auditinspections) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th style="width:9%;">Id</th>
                        <th style="width:9%;">Warehouse Code</th>
                        <th style="width:8%;">State</th>
                        <th style="width:8%;">Name of Warehouse</th>
                        <th style="width:8%;">Location</th>
                        <th style="width:8%;">Created Date</th>
                        <th style="width:8%;">Type of Warehouse</th>
                        <th style="width:8%;">Warehouse Status</th>
                        <th style="width:8%;">Status</th>
                        <th style="width:8%;">Auditor Name</th>
                        <th style="width:8%;">AM</th>
                        <th style="width:8%;">Cluster Head</th>
                        <th style="width:8%;">Upload/Edit Audit Video</th>
                        <th style="width:8%;">Action</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($auditinspections) > 0)
                        @foreach ($auditinspections as $ai)
                            <tr data-entry-id="{{ $ai->id }}">
                                <td>{{ $ai->id }}</td>
                                <td>{{ $ai->warehouse_code }}</td>
                                <td>{{ $ai->state }}</td>
                                <td>{{ $ai->name_of_warehouse }}</td>
                                <td>{{ $ai->location_name }}</td>
                                <td>{{ date("d-m-Y",strtotime($ai->created_at)) }}</td>
                                <td>{{ $ai->type_of_warehouse }}</td>
                                <td>
                                    @if($ai->location_closed == 'Yes')
                                    Location Closed
                                    @elseif($ai->location_not_started_before_audit=='Yes')
                                    Location Not Started
                                    @elseif($ai->cm_available_during_visit=='No')
                                    CM Not Available <br>
                                    @if($ai->reschedule_date != '')
                                    <button type="button" class="btn btn-primary">
                                      Rescheduled On <span class="badge badge-light"><?=$ai->reschedule_date?></span>
                                    </button>
                                    @endif
                                    @elseif($ai->whole_warehouse_under_fumigation=='Yes')
                                    Warehouse Is Under Fumigation <br>
                                    @if($ai->reschedule_date != '')
                                    <button type="button" class="btn btn-primary">
                                      Rescheduled On <span class="badge badge-light"><?=$ai->reschedule_date?></span>
                                    </button>
                                    @endif
                                    @else
                                    @if($ai->approved_status != 4)
                                    Escalation Replied-<br> By AM<span class="badge text-danger">{{ $HelperTrait->getRepliedEscalations($ai->id) }}</span>,<br>By CH<span class="badge text-danger">{{ $HelperTrait->getLevel1Escalations($ai->id) }}</span>,<br>Open<span class="badge text-danger">{{ $HelperTrait->getNotRepliedEscalations($ai->id) }}</span>
                                    @endif
                                    @endif    
                                    </td>
                                @if($ai->approved_status == 0)
                                <td>
                                    Maker Level   
                                </td>
                                @elseif($ai->approved_status == 1)
                                <td>
                                    Checker Level
                                </td>
                                @elseif($ai->approved_status == 2)
                                <td>
                                    Checker Approved
                                </td>
                                @elseif($ai->approved_status == 4)
                                <td>
                                    Final Approved
                                </td>
                                @else
                                <td>
                                    Checker Rejected
                                </td>
                                @endif
                                <td>{{ $ai->auditor_name }}</td>
                                <td>{{ $ai->name_of_last_visit_area_manager }}</td>
                                <td>{{ $ai->name_of_last_visit_cluster_manager }}</td>
                                <td>
                                    @if($ai->video_flag==0)
                                    <a href="{{ route('auditinspection.uploadauditvideo',[$ai->id]) }}" class="btn btn-xs btn-primary">Upload Video</a>
                                    @else
                                    <a href="{{ route('auditinspection.editauditvideo',[$ai->id]) }}" class="btn btn-xs btn-primary">Edit Video</a>
                                    @endif
                                </td>
                                <td>
                                 @if($ai->location_closed != 'Yes' && $ai->location_not_started_before_audit != 'Yes' && $ai->cm_available_during_visit != 'No' && $ai->whole_warehouse_under_fumigation != 'Yes' )   

                                    <?php
                                        if($role=='maker' || $role=='checker'){
                                    ?>
                                    @if($ai->approved_status == 0 || $ai->approved_status == 3)

                                          <?php
                                            $date1 = strtotime(date("Y-m-d", strtotime($ai->created_at)) . " +30 day");
                                            //echo $date1;
                                            if(strtotime(date('Y-m-d')) < $date1){ ?>
                                                <a href="{{ route('auditinspection.edit',[$ai->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                            <?php
                                               }
                                            ?>
                                    @endif
                                <?php } ?>
                                    <?php
                                        if($role=='maker'){
                                    ?>
                                    @if($ai->approved_status == 0)
                                    <button type="button" id="assignButton" class="btn btn-success"  onClick="assign({{$ai->id}})">Assign Warehouse</button>
                                    @endif
                                <?php } ?>
                                    
            
                                    <a href="{{ route('auditinspection.view',[$ai->id]) }}" class="btn btn-xs btn-primary">View</a>

                                @else

                                <?php
                                        if($role=='maker'){
                                    ?>
                                    @if($ai->approved_status == 0)
                                    <button type="button" id="assignButton" class="btn btn-success"  onClick="assign({{$ai->id}})">Assign Warehouse</button>
                                    @endif
                                <?php } ?>

                                @endif    
                                </td>
                                



                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>


        
    </div>
@stop

@section('javascript') 
<script type="text/javascript">

//to initialize datepicker plugin  
      $('.searchDate').datepicker({
        autoclose: true,
        format:"dd-mm-yyyy"
      });    
    
function assign(id){
     var confirmMsg = confirm("Are You sure, you want to assign this warehouse to checker?");
     var _token = "<?php echo csrf_token();?>";
        if (confirmMsg == true) {
            $("#assignButton").hide();
            $.ajax({
            url: "{{ route('auditinspection.assign') }}",
            data: {"id":id,"_token":_token},
            type: 'POST',
            dataType: 'json',
            success: function(response) {
            console.log(response);    
           if(response.value=='Success'){
               location.href="{{ route('auditinspection.index')}}";
              
           }else if(response.value=='Error'){
               location.href="{{ route('auditinspection.index')}}";
           }
           },
            error: function() { alert("Failed"); }
        }); 
        } else {
            location.href="{{ route('auditinspection.index')}}";
        }
}

</script>
    
@endsection