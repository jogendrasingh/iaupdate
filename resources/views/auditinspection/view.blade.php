@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
<?php
    $role = Auth::user()->roles->pluck('name')->first();
?>
<style type="text/css">
    table tr th{
        background-color: #e0e0e0 !important;
    }
    table tr th,td{
       border: 1px solid #ddd !important;
       font-size: 14px;
    }
</style>
@section('content')
    <h3 class="page-title">Audit Inspection</h3>
    
    
    <div class="panel panel-default">
        <div class="panel-heading">
            View
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered dt-select">
                    <tr>
                        <th >Id</th>
                        <td>{{ $auditinspection->id }}</td>
                        <th>Warehouse Code</th>
                        <td>{{ $auditinspection->warehouse_code }}</td>
                    </tr>
                    <tr>
                        <th >Location Closed</th>
                        <td>{{ $auditinspection->location_closed }}</td>
                        <th> Location Not Started Before Audit</th>
                        <td>{{ $auditinspection->location_not_started_before_audit }}</td>
                    </tr>
                    <tr>    
                        <th>State</th>
                        <td>{{ $auditinspection->state }}</td>
                        <th>Name of Warehouse</th>
                        <td>{{ $auditinspection->name_of_warehouse }}</td>
                    </tr>    
                    <tr>    
                        <th>Location</th>
                        <td>{{ $auditinspection->location_name }}</td>
                        <th>Warehouse Start Date</th>
                        <td>{{ $auditinspection->warehouse_start_date }}</td>
                    </tr>
                    <tr>    
                        <th>Type of Warehouse</th>
                        <td>{{ $auditinspection->type_of_warehouse }}</td>
                        <th>CM Available During Visit?</th>
                        <td>{{ $auditinspection->cm_available_during_visit }}</td>
                    </tr>
                    <tr>
                        <th>Arya Flex Banner</th>    
                        <td>{{ $auditinspection->arya_flex_banner }}</td>    
                        <th>Fire Fighting Equipment Installed?</th>
                        <td>{{ $auditinspection->fire_fighting_equipment_installed }}</td>
                    </tr>
                    @if(isset($auditinspection->number_of_ffe) && $auditinspection->number_of_ffe !='')
                    <tr>
                        <th> Number of FFE </th>    
                        <td>{{ $auditinspection->number_of_ffe }}</td>    
                        <th> Number of FFE Expired </th>
                        <td>{{ $auditinspection->number_of_ffe_expired }}</td>
                    </tr>
                    @endif
                    @if(isset($auditinspection->expired_ffe_image) && $auditinspection->expired_ffe_image !='')
                    <?php
                        $expiryDate = explode("|", $auditinspection->expiry_date);
                        $picture = explode("|", $auditinspection->expired_ffe_image);
                        foreach ($expiryDate as $key => $ed) {   
                    ?>
                    <tr>
                        <th>FFE Expiry Date </th>
                        <td>{{ $ed }}</td>
                        <th> Expired FFE Image:</th>
                        <td>
                        <?php $path="";
                            if(file_exists(public_path().'/storage/inspectionImages/ffeExpiredImages/'.$auditinspection->id.'/'.$picture[$key])){
                                $path=url('/')."/storage/inspectionImages/ffeExpiredImages/";
                            }else{
                                $path="https://aryapro.s3.ap-south-1.amazonaws.com/iastaging.aryacma.co.in/public/storage/inspectionImages/ffeExpiredImages/";
                            }
                        ?>
                        <img src="{{$path}}{{$auditinspection->id}}/{{$picture[$key]}}" width="100" width="100">
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                    @endif
                    <tr>    
                        <th>Live Electricity</th>
                        <td>{{ $auditinspection->live_electricity }}</td>
                        <th>Proper Ventilation in the Warehouse</th>    
                        <td>{{ $auditinspection->proper_ventilation_in_the_warehouse }}</td>
                    </tr>
                    <tr>    
                        <th>Plinth Height is Less Than 1 Feet</th>
                        <td>{{ $auditinspection->plinth_height_is_less_than_1_feet }}</td>
                        <th>Dunnage Material Used</th>    
                        <td>{{ $auditinspection->dunnage_material_used }}</td>
                    </tr>
                    <tr>    
                        <th>Who has the control on lock and key?</th>
                        <td>{{ $auditinspection->who_has_the_control_on_lock_and_key }}</td>
                        <th>Lock & Key compromise</th>    
                        <td>{{ $auditinspection->lock_and_key_compromise }}</td>
                    </tr>
                    <tr>    
                        <th>Any physical damage of warehouse structure?</th>
                        <td>{{ $auditinspection->any_physical_damage_to_warehouse_structure }}</td>
                    </tr>
                    @if(isset($auditinspection->structure_image) && $auditinspection->structure_image !='')
                    <tr>
                        <th> Detail of physical damage of warehouse structure?</th>
                        <td>{{$auditinspection->detail_of_physical_damage_to_warehouse_structure}}</td>
                        <th>Structure Image</th>
                        <td>
                            <?php $path="";
                                if(file_exists(public_path().'/storage/inspectionImages/damageStructure/'.$auditinspection->id.'/'.$auditinspection->structure_image)){
                                    $path="/storage/inspectionImages/damageStructure/";
                                }else{
                                    $path="https://aryapro.s3.ap-south-1.amazonaws.com/iastaging.aryacma.co.in/public/storage/inspectionImages/damageStructure/";
                                }
                            ?>
                            <img src="{{$path}}{{$auditinspection->id}}/{{$auditinspection->structure_image}}" width="100" width="100">
                        </td>
                    </tr>
                    @endif
                    <tr>    
                        <th>Auditor Name</th>
                        <td>{{ $auditinspection->auditor_name }}</td>
                        <th>Actual Date of Audit</th>    
                        <td>{{ $auditinspection->actual_date_of_audit }}</td>
                    </tr>
                    <tr>    
                        <th>Reason of Delay</th>
                        <td>{{ $auditinspection->reason_of_delay }}</td>
                        <!-- <th>Date of last Visit (Area Manager) </th>    
                        <td>{{ $auditinspection->date_of_last_visit_of_area_manager }}</td> -->
                    </tr>
                    <tr>    
                        <th>Name of Area Manager </th>
                        <td>{{ $auditinspection->name_of_last_visit_area_manager }}</td>
                        <th>Date of last Visit Ops Team  </th>    
                        <td>{{ $auditinspection->date_of_last_visit_of_area_manager }}</td>
                    </tr>
                    <tr>    
                        <th>Name of Last Visit Ops Team </th>
                        <td>{{ $auditinspection->name_of_last_visit_cluster_manager }}</td>
                        <th>Unrecorded Stock  </th>    
                        <td>{{ $auditinspection->unrecorded_stock }}</td>
                    </tr>    
                    <tr>        
                        <th>Type of Structure</th>
                        <td>{{ $auditinspection->type_of_structure }}</td>
                        <th>Fumigation Record Available at Warehouse  </th>    
                        <td>{{ $auditinspection->fumigation_record_available_at_warehouse }}</td>
                    </tr>
                   <tr>    
                        <th> Are all the registers available? </th>
                        <td>{{$auditinspection->are_all_the_registers_available}}</td>
                        <th>Are all the registers updated?*  </th>
                        <td>{{$auditinspection->are_all_the_registers_updated}}</td>
                    </tr>
                    <tr>    
                        <th> Name of the Register Not Available </th>
                        <td>{{$auditinspection->name_of_register_not_available}}</td>
                        <th>Name of the Register Not Updated  </th>
                        <td>{{$auditinspection->name_of_register_not_updated}}</td>
                    </tr>

                    <tr>
                        <th>All SVS,RO & Acknowledgments available?</th>    
                        <td>{{ $auditinspection->all_svs_ro_and_acknowledgments_available }}</td>
                        <th>Name of the CM</th>
                        <td>{{ $auditinspection->name_of_the_cm }}</td>
                    </tr>
                    <tr>    
                        <th>Date of Guard Deployed at Warehouse</th>    
                        <td>{{ $auditinspection->date_of_guard_deployment_at_warehouse }}</td>
                        <th>Security Day Guard Attendance</th>
                        <td>{{ $auditinspection->security_day_guard_attendance }}</td>
                    </tr>
                    <tr>    
                        <th>Security day guard available during the visit?</th>    
                        <td>{{ $auditinspection->security_day_guard_available_during_the_visit }}</td>
                        <th>Security Night Guard Attendance</th>
                        <td>{{ $auditinspection->security_night_guard_attendance }}</td>
                    </tr>
                    <tr>    
                        <th>Security guard attendance registers available</th>    
                        <td>{{ $auditinspection->security_guard_attendance_registers_available }}</td>
                        <th>Security guard attendance registers updated?</th>    
                        <td>{{ $auditinspection->security_guard_attendance_registers_updated }}</td>
                    </tr>
                    <tr>
                        <th>Cleanliness of warehouse</th>    
                        <td>{{ $auditinspection->cleanliness_of_warehouse }}</td>    
                        <th>Pending queries resolved?</th>
                        <td>{{ $auditinspection->pending_queries_resolved }}</td>
                    </tr>
                    <tr>
                        <th>Photo of visitor register attached</th>    
                        <td>{{ $auditinspection->photo_of_visiter_register_attached }}</td>    
                        <th>Any Other Observation/Remark</th>
                        <td>{{ $auditinspection->any_other_observation_or_remark }}</td>
                    </tr>
                    <tr>
                        <th>Stack Plan Displayed</th>    
                        <td>{{ $auditinspection->stack_plan_displayed }}</td>    
                        <th>Stack Plan Match With Physical Stack</th>
                        <td>{{ $auditinspection->stack_plan_match_with_physical_stack }}</td>
                    </tr>
                    @if(isset($auditinspection->observation_remark))
                    <tr>    
                        <th>Remark</th>
                        <td>{{ $auditinspection->observation_remark }}</td>
                    </tr>
                    @endif
                    <tr>
                        <th>Last Transaction Date(Entered)</th>    
                        <td>{{ $auditinspection->last_transaction_date }}</td>    
                        <th>Last Transaction Date(System)</th>
                        <td>{{ $auditinspection->last_transaction_date_system }}</td>
                    </tr>
                    @if(isset($auditinspection->image_1) && $auditinspection->image_1 !='')
                    <tr>
                        <th colspan="2">Image 1</th>
                        <td colspan="2">
                            <?php $path="";
                                if(file_exists(public_path().'/storage/inspectionImages/'.$auditinspection->id.'/'.$auditinspection->image_1)){
                                    $path="/storage/inspectionImages/";
                                }else{
                                    $path="https://aryapro.s3.ap-south-1.amazonaws.com/iastaging.aryacma.co.in/public/storage/inspectionImages/";
                                }
                            ?>
                            <img src="{{$path}}{{$auditinspection->id}}/{{$auditinspection->image_1}}" width="100" width="100">
                        </td>
                    </tr>
                    @endif
                    @if(isset($auditinspection->image_2) && $auditinspection->image_2 !='')
                    <tr>
                        <th colspan="2">Image 2</th>
                        <td colspan="2">
                            <?php $path="";
                                if(file_exists(public_path().'/storage/inspectionImages/'.$auditinspection->id.'/'.$auditinspection->image_2)){
                                    $path="/storage/inspectionImages/";
                                }else{
                                    $path="https://aryapro.s3.ap-south-1.amazonaws.com/iastaging.aryacma.co.in/public/storage/inspectionImages/";
                                }
                            ?>
                            <img src="{{$path}}{{$auditinspection->id}}/{{$auditinspection->image_2}}" width="100" width="100">
                        </td>
                    </tr>
                    @endif
                    @if(isset($auditinspection->image_3) && $auditinspection->image_3 !='')
                    <tr>
                        <th colspan="2">Image 3</th>
                        <td colspan="2">
                            <?php $path="";
                                if(file_exists(public_path().'/storage/inspectionImages/'.$auditinspection->id.'/'.$auditinspection->image_3)){
                                    $path="/storage/inspectionImages/";
                                }else{
                                    $path="https://aryapro.s3.ap-south-1.amazonaws.com/iastaging.aryacma.co.in/public/storage/inspectionImages/";
                                }
                            ?>
                            <img src="{{$path}}{{$auditinspection->id}}/{{$auditinspection->image_3}}" width="100" width="100">
                        </td>
                    </tr>
                    @endif
                    @if(isset($auditinspection->image_4) && $auditinspection->image_4 !='')
                    <tr>
                        <th colspan="2">Image 4</th>
                        <td colspan="2">
                            <?php $path="";
                                if(file_exists(public_path().'/storage/inspectionImages/'.$auditinspection->id.'/'.$auditinspection->image_4)){
                                    $path="/storage/inspectionImages/";
                                }else{
                                    $path="https://aryapro.s3.ap-south-1.amazonaws.com/iastaging.aryacma.co.in/public/storage/inspectionImages/";
                                }
                            ?>
                            <img src="{{$path}}{{$auditinspection->id}}/{{$auditinspection->image_4}}" width="100" width="100">
                        </td>
                    </tr>
                    @endif
                    @if(isset($auditinspection->image_5) && $auditinspection->image_5 !='')
                    <tr>
                        <th colspan="2">Image 5</th>
                        <td colspan="2">
                            <?php $path="";
                                if(file_exists(public_path().'/storage/inspectionImages/'.$auditinspection->id.'/'.$auditinspection->image_5)){
                                    $path="/storage/inspectionImages/";
                                }else{
                                    $path="https://aryapro.s3.ap-south-1.amazonaws.com/iastaging.aryacma.co.in/public/storage/inspectionImages/";
                                }
                            ?>
                            <img src="{{$path}}{{$auditinspection->id}}/{{$auditinspection->image_5}}" width="100" width="100">
                        </td>
                    </tr>
                    @endif



            </table>
            <br>
            @if(isset($chamberwiseStock) && count($chamberwiseStock)>0)
            <h3 class="h3 text-center">Chamber and Stack wise Stock</h3>
            <table class="table">
            <tr>
                <th>Chamber Number</th>
                <th>Stack Number</th>
                <th>Client name</th>
                <th>Current Stock bags</th>
                <th>Current Stock Quantity</th>
                <!-- <th>Stack Status 1</th> -->
                <th>Stack Status 2</th>
                <th>Stack Health Status</th>
                <th>Uncountable Bags</th>
                <th>Countable Bags(PV)</th>
                <th>Unrecorded Stock</th>
                <th>Difference</th>
            </tr>
            <?php
                $totalCurrentStockBags = 0;
                $totalCurrentStockQuantity = 0;
                $totalUncountableBags = 0;
                $totalCountableBags = 0;
                $totalDifferenceStack =0;

            ?>
            @foreach($chamberwiseStock as $stock)
            <?php
                $totalCurrentStockBags += $stock->current_stock_bags;
                $totalCurrentStockQuantity += $stock->current_stock_qty;
                $totalUncountableBags += $stock->number_of_bags;
                $totalCountableBags += $stock->countable_number_of_bags;

                $totalDifferenceStack  += $stock->current_stock_bags - $stock->number_of_bags - $stock->countable_number_of_bags + $auditinspection->unrecorded_stock;
            ?>
            <tr>
                <td>{{$stock->chamber_number}}</td>
                <td>{{$stock->stack_number}}</td>
                <td>{{$stock->client_name}}</td>
                <td>{{$stock->current_stock_bags}}</td>
                <td>{{$stock->current_stock_qty}}</td>
                <!-- <td>{{$stock->stack_status_1}}</td> -->
                <td>{{$stock->stack_status_2}}</td>
                <td>{{$stock->stack_health_status}}</td>
                <td>{{$stock->number_of_bags}}</td>
                <td>{{$stock->countable_number_of_bags}}</td>
                <td></td>
                <td>{{-($stock->current_stock_bags - $stock->number_of_bags - $stock->countable_number_of_bags)}}</td>
            </tr>
            @endforeach
            <tr>
                <td>Unrecorded Stock</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ $auditinspection->unrecorded_stock }}</td>
                <td>{{ $auditinspection->unrecorded_stock }}</td>
            </tr>
            <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th>{{$totalCurrentStockBags}}</th>
                <th>{{$totalCurrentStockQuantity}}</th>
                <th></th>
                <th></th>
                <th>{{ $totalUncountableBags }}</th>
                <th>{{ $totalCountableBags }}</th>
                <th>{{ $auditinspection->unrecorded_stock }}</th>
                <th>{{ ($totalUncountableBags + $totalCountableBags +$auditinspection->unrecorded_stock) - $totalCurrentStockBags}}</th>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>PWH Total</th>
                <th>{{ $totalUncountableBags + $totalCountableBags +$auditinspection->unrecorded_stock}}</th>
            </tr>
        </table>
        <br>
        @endif
        

        @if(isset($clientwiseStock) && count($clientwiseStock)>0)
        <h3 class="h3 text-center">Clientwise Stock</h3>
            <table class="table">
            <tr>
                <th>Client Name</th>
                <th>Borrower Name</th>
                <th>Closing Bags</th>
                <th>Closing Quantity</th>
                <th>Stack Status 1</th>
                <th>Stack Status 2</th>
                <th>Stack Health Status</th>
                <th>Number Of Bags</th>
                <th>Difference</th>
            <?php
                $totalClosingBags= 0;
                $totalClosingQty = 0;
                $totalNoOfBags = 0;
                $totalDifference = 0;
            ?>
            @foreach($clientwiseStock as $stock)
            <?php
                $totalClosingBags += $stock->closing_bag;
                $totalClosingQty  += $stock->closing_qty;
                $totalNoOfBags    += $stock->number_of_bags;
                $totalDifference  += $stock->closing_bag - $stock->number_of_bags;
            ?>
            <tr>
                <td>{{$stock->client_name}}</td>
                <td>{{$stock->borrower_name}}</td>
                <td>{{$stock->closing_bag}}</td>
                <td>{{$stock->closing_qty}}</td>
                <td>{{$stock->stack_status_1}}</td>
                <td>{{$stock->stack_status_2}}</td>
                <td>{{$stock->stack_health_status}}</td>
                <td>{{$stock->number_of_bags}}</td>
                <td>{{$stock->number_of_bags-$stock->closing_bag}}</td>
            </tr>
            @endforeach
            <tr>
                <th>Total</th>
                <th></th>
                <th>{{$totalClosingBags}}</th>
                <th>{{$totalClosingQty}}</th>
                <th></th>
                <th></th>
                <th></th>
                <th>{{$totalNoOfBags}}</th>
                <th>{{$totalNoOfBags-$totalClosingBags}}</th>
            </tr>
        </table>
        @endif
        <br>

        <div class="col-xs-12">
            
        <?php
            if($role=='checker'){
              if($auditinspection->type_of_warehouse=='PWH-CM'){  
                $totalPwhStock = $totalUncountableBags + $totalCountableBags +$auditinspection->unrecorded_stock;
                $totalCmStock = $totalNoOfBags;
              }else{
                $totalPwhStock = 0;
                $totalCmStock = 0;
              }  
        ?> 
        @if($auditinspection->approved_status == '1' && $totalPwhStock >= $totalCmStock)   
         <button type="button" id="approveButton"  class="btn btn-success pull-left"  onClick="approve({{$auditinspection->id}})">Approve</button>
         <button type="button" id="rejectButton_checker"  class="btn btn-warning pull-right"  onClick="reject({{$auditinspection->id}})">Reject</button>
        @endif 
        <?php
            }
        ?>
        </div>
        <div class="clearfix"></div>
        <br>

        @if(!empty($escalations))
            <h3 class="h3 text-center">Escalations Queries</h3>
            <table class="table">
            <tr>
                <th>S.No</th>
                <th>Stack Number</th>
                <th>Bank Name</th>
                <th>Client Name</th>
                <th>Escalation Query</th>
                <th>Updated values</th>
                <th>Query Status</th>
                <th>Query Category</th>
                <th>Date</th>
                <th>User</th>
                <th>Action</th>
            </tr>
            
            @foreach($escalations as $e)
            <?php 
                $counter = $loop->iteration;
            ?>
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$e->stack_no}}</td>
                <td>{{$e->bank_name}}</td>
                <td>{{$e->client_name}}</td>
                <td>{{$e->description}}</td>
                <td>{{$e->escalation_value}}</td>
                @if($e->status=='0')
                <td><span class="label label-primary">Open</span></td>
                @elseif($e->status=='1')
                <td><span class="label label-success">Closed</span></td>
                @elseif($e->status=='2')
                <td><span class="label label-info">Replied</span></td>
                @elseif($e->status=='3')
                <td><span class="label label-warning">Reopen</span></td>
                @elseif($e->status=='4')
                <td><span class="label label-danger">At Level 1</span></td>
                @endif
                @if($e->dishonour_status=='0')
                <td><span class="label label-success">Honoured</span></td>
                @elseif($e->dishonour_status=='1')
                <td><span class="label label-danger">Dishonoured</span></td>
                @endif
                <td>{{date('d-m-Y',strtotime($e->created_at))}}</td>
                <td>{{$e->name}}</td>
                <td>
                    <?php
                        if($role=='checker'){
                    ?>
                    @if($e->status == 2)
                    
                    <button type="button" name='accept' value='accept' id="acceptButton{{$e->id}}" onclick="confirmAccept('{{$e->id}}','{{$auditinspection->id}}')"  class="btn btn-primary" >Accept</button>
                    
                    <button type="button" name='reject' value='reject' id="rejectButton2{{$e->id}}"  class="btn btn-warning" data-toggle="modal" data-target="#myModal" onClick="getEscalation({{$e->id}})">Reject</button>
                    @endif
                    <?php
                        }
                    ?>

                    <?php
                        if($role=='cluster head'){
                    ?>
                    @if($e->status == 0 or $e->status == 3)
                     <?php
                            $date1 = strtotime(date("Y-m-d", strtotime($e->created_at)) . " +30 day");
                            //echo $date1;
                            if(strtotime(date('Y-m-d')) < $date1){ ?>
                    <button type="button" name='reply' value="{{$e->field_type}}" id="rejectButton{{$e->id}}"  class="btn btn-primary" data-toggle="modal" data-target="#myModal" onClick="getEscalation({{$e->id}},this.value)">Reply</button>
                    <?php
                           }
                        ?>
                    @endif
                    <?php
                        }
                    ?>

                    <?php
                        if($role=='level 1'){
                    ?>
                    @if($e->status == 4 )
                     <?php
                            $date1 = strtotime(date("Y-m-d", strtotime($e->created_at)) . " +30 day");
                            //echo $date1;
                            if(strtotime(date('Y-m-d')) < $date1){ ?>
                    <button type="button" name='accept' value='accept' id="acceptButton{{$e->id}}" onclick="confirmAccept('{{$e->id}}','{{$auditinspection->id}}')"  class="btn btn-primary" >Accept</button>
                                
                    <button type="button" name='reply' value="{{$e->field_type}}" id="rejectButton{{$e->id}}"  class="btn btn-primary" data-toggle="modal" data-target="#myModal" onClick="getEscalation({{$e->id}},this.value)"> Update Reply</button>
                    <?php
                           }
                        ?>
                    @endif
                    <?php
                        }
                    ?>

                    <?php
                        if($role=='level 3' || $role=='level 4'){
                    ?>
                    @if($e->status != 1)   
                    <button type="button" name='reject' value='reject' id="rejectButton{{$e->id}}"  class="btn btn-primary" data-toggle="modal" data-target="#myModal" onClick="getEscalation({{$e->id}})">Comments</button>
                    @endif
                    <?php
                        }
                    ?> 
                    
                </td>
        
            </tr>
            
                @if(isset($e->auditReply))
                    @foreach($e->auditReply as $ar)
                    <tr>        
                       <td align="right">{{$counter}}.{{$loop->iteration}}</td>
                       <td colspan='5'>{{$ar->comment}}
                       @if(isset($ar->till_date)) 
                        By <span class="label label-info">{{$ar->till_date}}</span>
                       @endif 
                        <td> 
                        <td></td>    
                       <td>{{date('d-m-Y',strtotime($ar->created_at))}}</td>
                       <td>{{ $ar->name}}</td>
                       <td></td>
                    </tr>    
                    @endforeach
                    
                @endif
            
            

            @endforeach
            
        </table>
        @endif
        </div>
        @if(isset($chamberwiseStock) && count($chamberwiseStock)>0)
        @if(isset($healthStatusArr) && count($healthStatusArr)>0)
        <h3 class="h3 text-center">PV Entered</h3>
            <table class="table">
            <tr>
                <th>Chamberwise Stock Id</th>
                <th>Stack Number</th>
                <th>Client name</th>
                <th>No. of Block(A)</th>
                <th>Danda(B)</th>
                <th>Patti(C)</th>
                <th>(Danda+Patti)(D)</th>
                <th>Height(E)</th>
                <th>+Bags(F)</th>
                <th>-Bags(G)</th>
                <th>Total(A*(B+C)*E)+F-G</th>
                <th>Match Status</th>
            </tr>
            @foreach($healthStatusArr as $hs)
            <tr>
                <td>{{$hs['chamberwise_stock_id']}}</td>
                <td>{{$hs['stack_no']}}</td>
                <td>{{$hs['borrower_name']}}</td>
                <td>{{$hs['no_of_block']}}</td>
                <td>{{$hs['danda']}}</td>
                <td>{{$hs['patti']}}</td>
                <td>{{$hs['danda_plus_patti']}}</td>
                <td>{{$hs['height']}}</td>
                <td>{{$hs['plus_bags']}}</td>
                <td>{{$hs['minus_bags']}}</td>
                <td>{{$hs['total']}}</td>
                <td>@if($hs['match_status']==0)
                    Not Matched
                    @endif
                </td>
            </tr>
            @endforeach
        </table>
        @endif
        @endif
        <br>


          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> 
                        <?php
                            if($role=='level 3' || $role=='level 4'|| $role=='checker'){
                        ?>
                        Comments :-
                        <?php
                         }else{
                        ?>
                        Escalation Query Reply :-
                        <?php
                            }
                        ?>    
                        </h4>
                </div>
                <form action="" mehtod="post" onsubmit="return saveComment();" id="commentForm">
                <div class="modal-body">
                        
                            {{csrf_field()}}
                        <div class="error"></div>
                        <div class='row'>
                            <div class='col-md-10'>
                                
                               <?php
                                if($role=='level 3' || $role=='level 4' || $role=='checker'){
                                ?>
                                <textarea name="comment" id="comment" class="form-control"></textarea>
                                <?php
                                 }else{
                                ?>
                                <div class="form-group" id="comment_container">
                                
                                </div>
                                <?php
                                    }
                                ?>
                                <br>
                                <?php
                                    if($role=='level 1'){
                                ?>
                                <div class="form-group" id="till_date_container">
                                    <label for="till_date" class="control-label">Date till this query will be resolved*</label>
                                    <input class="form-control" placeholder="" name="till_date" type="text" id="till_date" value="">
                                    
                                </div>
                                <?php
                                    }
                                ?>

                                <input type="hidden" name="audit_inspection_id" id="audit_inspection_id" value="<?php echo $auditinspection->id;?>">
                                <input type="hidden" name="audit_escalation_id" id="audit_escalation_id" value="">
                            </div>
                            
                        </div>
                        
                </div>
                <div class="modal-footer">
                  <button type="submit" name="Submit" id="replyId" class="btn btn-primary">Submit</button>
                </div>
                </form>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->


        


        

        
    </div>
@stop

@section('javascript')
    <script type="text/javascript">        
    $('#till_date').datepicker({
        autoclose: true,
        format:"dd-mm-yyyy",
        todayHighlight: true,
        startDate: '0d'
      });

    function approve(id){
     var confirmMsg = confirm("Are You sure, you want to approve this audit?");
     var _token = "<?php echo csrf_token();?>";
        if (confirmMsg == true) {
            $("#approveButton").hide();
            $.ajax({
            url: "{{ route('auditinspection.approve') }}",
            data: {"id":id,"_token":_token},
            type: 'POST',
            dataType: 'json',
            success: function(response) {
            console.log(response);    
           if(response.value=='Success'){
               location.href="{{ route('auditinspection.index')}}";
              
           }else if(response.value=='Error'){
               location.href="{{ route('auditinspection.index')}}";
           }
           },
            error: function() { alert("Failed"); }
        }); 
        } else {
            location.href="{{ route('auditinspection.index')}}";
        }
    }

    function reject(id){
     var confirmMsg = confirm("Are You sure, you want to reject this audit?");
     var _token = "<?php echo csrf_token();?>";
        if (confirmMsg == true) {
            $("#rejectButton_checker").hide();
            $.ajax({
            url: "{{ route('auditinspection.reject') }}",
            data: {"id":id,"_token":_token},
            type: 'POST',
            dataType: 'json',
            success: function(response) {
            console.log(response);    
           if(response.value=='Success'){
               location.href="{{ route('auditinspection.index')}}";
              
           }else if(response.value=='Error'){
               location.href="{{ route('auditinspection.index')}}";
           }
           },
            error: function() { alert("Failed"); }
        }); 
        } else {
            location.href="{{ route('auditinspection.index')}}";
        }
    }

    function getEscalation(id,val='')
        {
            $("#audit_escalation_id").val(id);
            
            if(val!=''){
                getReplyOptions(val);
            }else{

            }
        }


    function saveComment() {
        $(".error").text('');    
        var id = $("#audit_inspection_id").val();
        var comment = $("#comment").val();
        //var till_date = $("#till_date").val();
        //var _token = "<?php echo csrf_token();?>";
        if(comment==''/* && till_date==''*/){
        $(".error").text("Comment can't be blank"); 
        return false;
        }else{
        $("#replyId").hide();
        var eId = $("#audit_escalation_id").val();
        $.ajax({
            url: '{{route("auditinspection.addcomment") }}',
            data: $('#commentForm').serialize(),
            type: 'POST',
            //dataType: 'json',
            success: function (response) {
                alert(response);
                $('#comment').val('');
                $("#rejectButton"+eId).hide();
                $("#rejectButton2"+eId).hide();
                $("#acceptButton"+eId).hide();
                $('#myModal').modal('toggle');
                $("#replyId").show();
                 //location.href = "{{ route('auditinspection.view',[$auditinspection->id])}}";
            },
            error: function () {
                alert('Failed!');
            }
        });
        return false;
       }  
    }


    function confirmAccept(escId,inspId) {
        
        var inspId = inspId;
        var eId = escId;
        var _token = "<?php echo csrf_token();?>";
        var role = "<?php echo $role;?>";
        if(role=='checker'){
            var confirmMsg = confirm("Are You sure, you want to accept audit escalation  !");    
        }else{
            var confirmMsg = confirm("Are You sure, you want to accept this reply from cluster head !");
        }
        
        if (confirmMsg == true) {
        
            $.ajax({
                url: '{{route("auditinspection.check") }}',
                data: {id:eId,_token:_token},
                type: 'POST',
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    if (response.value == 'Success') {
                    $("#rejectButton2"+eId).hide();
                    $("#acceptButton"+eId).hide();
                    $('#myModal2').modal('toggle');
                
                        //location.href = "{{ route('auditinspection.view',[$auditinspection->id])}}";
                    }
                },
                error: function () {
                    alert('Failed!');
                }
            });
             return false;
        } else {
            
        }
    }

// Code to generate dynamic reply options based on queries

var reply = {
                fire_fighting_equipment_installed: ["FFE will be installed as on", "FFE has already been installed","I have already infromed client to instal FFE because it's clients responsibility","we have informed bank and client to install FFE","FFE has already been installed","i have already infromed client to instal FFE because it's clients responsibility"],
                dunnage_material_used: ["We have already informed to client and bank", "We will inform to client and bank","Dunnage is already used"],
                arya_flex_banner:["Arya banner will be installed","Arya banner has already been installed","We have informed to banker that the warehouse owner is refusing to put banner"],
                live_electricity:["Electricity will disconnected as on","electricity has already been disconnected","Electricity is  using  for  our CCTV Camera","We have informed client for disconnect electricity because client has been using it for sorting and grading of commodity","We have informed Banker for disconnect electricity because client has been using it for sorting and grading of commodity"],
                proper_ventilation_in_the_warehouse:["Proper ventilation has already approved by client and bank","I already informed to bank for ventilation","Proper ventilation has already approved by bank"],
                plinth_height_is_less_than_1_feet:["I already informed to client for less plinth height and client has already approved it","I already informed to bank for less plinth height","Plinth height is not less than 1 feet"],
                lock_and_key_compromise:["I have sensitized CM/Supervisor","Lock and key has been changed","Lock and key is not compromised because it is licensed warehouse","Lock and key is not compromised"],
                any_physical_damage_to_warehouse_structure:["Informed to warehouse owner and banker for WH Repairing","Warehouse repairing has already been done","It's an minor damage and has no harm to the commodity"],
                fumigation_record_available_at_warehouse:["Fumigation record will be done as on","Fumigation record has already done","Already informed bank / Clientthat the resposibility for fumigation is on the warehouse owner"],
                are_all_the_registers_available:["Register will be reached","register has been reached"],
                are_all_the_registers_updated:["Register will be updated","Register has already been updated"],
                all_svs_ro_and_acknowledgments_available:["We have trained the CM/Supervisor, from the next time he will put all records at the godown","Now all document available and updated at warehouse"],
                date_of_guard_deployment_at_warehouse:["In the starting of the warehouse we have informed to the security agency","Guard appointed as per client approval/requirement","Guard appointed on same day when location started","Guard appointed on same day when stock inward in warehouse","Security Guard is not applicable"],
                cleanliness_of_warehouse:["Cleaning will be done as on","Cleaning has already done","We have informed the client and bank, it's not our responsibility"],
                borrower_name_matching:["Stack Card Will displayed as on","Stack Card has already displayed as on","Stack card is already updated","Stack dispatched/released"],
                bank_name_matching:["Pledge Card will be displayed","Pledge Card has already displayed","Stack released and dispatched"],
                stack_status_2:["I had informed bank that the stock has been touced by walls","It is approved by the banker, and the stacking has been done in presence of our CM","It is approved by the Client and Bank, and the stacking has been done in presence of our CM","We have informed bank and client for the repair the stack","Routine fumigation work in progress","We have informed bank and client for the repair the stack","We have informed  client for the repair the stack","Transaction running in warehouse","Stack dispatched and released","Restacking done"],
                stack_health_status:["We infromed bank and client","Fumigation will be planned","Already taken letter from client regarding this stock","Stack released and dispatched","Stock is ok, no atta formation found"],
                security_day_guard_available_during_the_visit:['He is on sanctioned leave',"I have already marked Absent","Security Guard is not applicable"],
                pending_queries_resolved:["I will resolve this warehouse queries","I have already resolved all queries"],
                security_guard_attendance_registers_available:["The security guard attendance register will provided","The security guard attendance register is already provided","Security Guard is not applicable"],
                security_guard_attendance_registers_updated:["The security guard attendance register is already updated","I will mark absent the guard attendance","Security Guard is not applicable"],
                stack_status_1:["Stock matched with DTR, Warehouse Record and Auditor PV","Stack dispatched and released"],
                any_other_observation_or_remark:['Other Remark'],
                date_of_last_visit_of_cluster_manager:["CH will visit this warehouse on date","CH already visited after audit"]


            };



function getReplyOptions(val) {

    var fieldsWithDate = ['borrower_name_matching','bank_name_matching','stack_status_2','stack_health_status','fire_fighting_equipment_installed','arya_flex_banner','live_electricity','date_of_last_visit_of_cluster_manager','fumigation_record_available_at_warehouse','are_all_the_registers_available','date_of_guard_deployment_at_warehouse','cleanliness_of_warehouse'];
        if(fieldsWithDate.indexOf(val) != -1)
        {  
           // element found
           $('#till_date_container').show();
        }else{
            $('#till_date_container').hide();
        }

    document.getElementById("comment_container").innerHTML = "";
    var options='';
    
    if (reply[val] == null || reply[val]=='undefined' || reply[val]=='') {
        //options += '<option value="">' + 'No reply option available yet' + '</option>';
        options ='<textarea name="comment" id="comment" class="form-control"></textarea>';
        
    }else {

            options ='<select class="form-control" id="comment" name="comment" >';                                                          
            options += '<option value="">Select</option>';

        for (var r in reply[val]) {
        
            options += '<option value="' + reply[val][r] + '">' + reply[val][r] + '</option>';
            
        }
        options +='</select>';
        
    }    

    //console.log(options);

    document.getElementById("comment_container").innerHTML = options;    

    //console.log(options);

    
}

   
    </script>
@endsection