@extends('layouts.app')

@section('content')
<style type="text/css">
    .error{
        color:red;
    }
</style>
    <h3 class="page-title">Audit Inspection</h3>
    @if(isset($auditinspection))
        <form method="POST" action="{{ route('auditinspection.updatemore',[$auditinspection->id]) }}" enctype="multipart/form-data" id="auditForm">
    @else
        <form method="POST" action="{{ route('auditinspection.savemore',[$id]) }}" enctype="multipart/form-data" id="auditForm">
    @endif

    <input type="hidden" name="_method" value="{{ isset($auditinspection) ? 'PUT' : 'POST' }}">
    <?php
      //echo Auth::user()->name;  die; 
    ?>    

            {{csrf_field()}}   
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Audit Inspection
            </div>
        
        <div class="panel-body">
            <div class="col-md-12 row">
                
                <div class="col-md-6 form-group">
                    <label for="fire_fighting_equipment_installed" class="control-label">Fire Fighting Equipment Installed*</label>
                    <select class="form-control select2" id="fire_fighting_equipment_installed" name="fire_fighting_equipment_installed" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('fire_fighting_equipment_installed'))
                        <p class="help-block">
                            {{ $errors->first('fire_fighting_equipment_installed') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group" id="section21" >
                    <label for="number_of_ffe" class="control-label">Number of FFE</label>
                    <select class="form-control" id="number_of_ffe" name="number_of_ffe" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('number_of_ffe'))
                        <p class="help-block">
                            {{ $errors->first('number_of_ffe') }}
                        </p>
                    @endif
                </div>
                
            </div>
            
            <div class="col-md-12 row" id="section22">
                
                <div class="col-md-6 form-group">
                    <label for="number_of_ffe_expired" class="control-label">Number of FFE Expired</label>
                    <select class="form-control" id="number_of_ffe_expired" name="number_of_ffe_expired" tabindex="-1" aria-hidden="true"  onchange="hideShowSection1(this.value,['section23'])">
                        <option value="0">Select</option>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('number_of_ffe_expired'))
                        <p class="help-block">
                            {{ $errors->first('number_of_ffe_expired') }}
                        </p>
                    @endif
                </div>
                 
            </div>
            <?php 
                if(isset($auditinspection)){
                    $expDateArr = explode("|",$auditinspection->expiry_date);
                    $pictureArr = explode("|",$auditinspection->expired_ffe_image);

                    foreach ($expDateArr as $key => $ed) {
                ?> 

                <div class="col-md-12 row">
                
                <div class="col-md-6 form-group">
                    <label for="expiry_date" class="control-label">Expiry Date*</label>
                    <input class="form-control expiryDate" placeholder="" name="expiry_date[]" type="text" value="{{ $ed }}">
                    <p class="help-block"></p>
                    @if($errors->has('expiry_date'))
                        <p class="help-block">
                            {{ $errors->first('expiry_date') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="expired_ffe_image" class="control-label">Expired FFE Image:*</label>
                    <input type="file" name="expired_ffe_image[]"  class="form-control" />
                    @if(isset($pictureArr[$key]) && $pictureArr[$key] !='')
                    <img src="/storage/inspectionImages/ffeExpiredImages/{{$auditinspection->id}}/{{$pictureArr[$key]}}" width="100" width="100">
                    @endif
                </div>
                

            </div>


            <?php   
                    }
                }else{    
            ?>            

            <div class="list_wrapper pb-5" id="section23" style="display: none;">
            <div class="col-md-12 row">
                
                <div class="col-md-6 form-group">
                    <label for="expiry_date" class="control-label">Expiry Date*</label>
                    <input class="form-control expiryDate" placeholder="" name="expiry_date[]" type="text" value="{{ $auditinspection->expiry_date or '' }}">
                    <p class="help-block"></p>
                    @if($errors->has('expiry_date'))
                        <p class="help-block">
                            {{ $errors->first('expiry_date') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-5 form-group">
                    <label for="expired_ffe_image" class="control-label">Expired FFE Image:*</label>
                    <input type="file" name="expired_ffe_image[]"  class="form-control" />
                    @if(isset($auditinspection->expired_ffe_image) && $auditinspection->expired_ffe_image !='')
                    <img src="/storage/inspectionImages/expiredFFE/{{$auditinspection->id}}/{{$auditinspection->expired_ffe_image}}" width="100" width="100">
                    @endif
                </div>
                <div class="col-md-1 form-group">
                    <br>
                   <button class="btn btn-primary list_add_button" type="button">+</button>
                </div>

            </div>

            </div>
            <?php
                }
            ?>
            
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="name_of_last_visit_area_manager" class="control-label">Name of Area Manager:</label>
                    <select class="form-control select2"  id="name_of_last_visit_area_manager" name="name_of_last_visit_area_manager" tabindex="-1" aria-hidden="true" onchange="getClusterHeads(this)">
                        <option value="">Select</option>
                        @foreach ($level1 as $l)                      
                            <option value="{{$l->name}}" data-amid="{{$l->id}}">{{$l->name}}</option>
                        @endforeach
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('name_of_last_visit_area_manager'))
                        <p class="help-block">
                            {{ $errors->first('name_of_last_visit_area_manager') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="dunnage_material_used" class="control-label">Dunnage material used*</label>
                    <select class="form-control select2" id="dunnage_material_used" name="dunnage_material_used" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('dunnage_material_used'))
                        <p class="help-block">
                            {{ $errors->first('dunnage_material_used') }}
                        </p>
                    @endif
                </div>
                
            </div>
        
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="arya_flex_banner" class="control-label">Arya Flex Banner*</label>
                    <select class="form-control select2" id="arya_flex_banner" name="arya_flex_banner" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('arya_flex_banner'))
                        <p class="help-block">
                            {{ $errors->first('arya_flex_banner') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="live_electricity" class="control-label">Live Electricity*</label>
                    <select class="form-control select2" id="live_electricity" name="live_electricity" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                        
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('live_electricity'))
                        <p class="help-block">
                            {{ $errors->first('live_electricity') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="proper_ventilation_in_the_warehouse" class="control-label">Proper ventilation in the warehouse*</label>
                    <select class="form-control select2" id="proper_ventilation_in_the_warehouse" name="proper_ventilation_in_the_warehouse" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                        
                        
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('proper_ventilation_in_the_warehouse'))
                        <p class="help-block">
                            {{ $errors->first('proper_ventilation_in_the_warehouse') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="plinth_height_is_less_than_1_feet" class="control-label">Plinth Height is Less Than 1 Feet*</label>
                    <select class="form-control select2" id="plinth_height_is_less_than_1_feet" name="plinth_height_is_less_than_1_feet" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                        
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('plinth_height_is_less_than_1_feet'))
                        <p class="help-block">
                            {{ $errors->first('plinth_height_is_less_than_1_feet') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="lock_and_key_compromise" class="control-label">Lock and Key compromise*</label>
                    <select class="form-control select2" id="lock_and_key_compromise" name="lock_and_key_compromise" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                        
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('lock_and_key_compromise'))
                        <p class="help-block">
                            {{ $errors->first('lock_and_key_compromise') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="any_physical_damage_to_warehouse_structure" class="control-label">Any physical damage of warehouse structure?:*</label>
                    <select class="form-control select2" id="any_physical_damage_to_warehouse_structure" name="any_physical_damage_to_warehouse_structure" tabindex="-1" aria-hidden="true" onchange="hideShow(this.value);">
                        <option value="">Select</option>
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                        
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('any_physical_damage_to_warehouse_structure'))
                        <p class="help-block">
                            {{ $errors->first('any_physical_damage_to_warehouse_structure') }}
                        </p>
                    @endif
                </div>
                    

            </div>

            <div class="col-md-12 row" id="structureDamage" style="display: none;">
                <div class="col-md-6 form-group">
                    <label for="detail_of_physical_damage_to_warehouse_structure" class="control-label">Detail of physical damage of warehouse structure?:*</label>
                    <textarea class="form-control" name="detail_of_physical_damage_to_warehouse_structure" id="detail_of_physical_damage_to_warehouse_structure">{{$auditinspection->detail_of_physical_damage_to_warehouse_structure or ''}}</textarea>
                    <p class="help-block"></p>
                    @if($errors->has('detail_of_physical_damage_to_warehouse_structure'))
                        <p class="help-block">
                            {{ $errors->first('detail_of_physical_damage_to_warehouse_structure') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="structure_image" class="control-label">Structure Image:*</label>
                    <input type="file" name="structure_image"  class="form-control" />
                    @if(isset($auditinspection->structure_image) && $auditinspection->structure_image !='')
                    <img src="/storage/inspectionImages/damageStructure/{{$auditinspection->id}}/{{$auditinspection->structure_image}}" width="100" width="100">
                    @endif
                </div>
            </div>
            
            
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="actual_date_of_audit" class="control-label">Actual date of audit</label>
                    <input class="form-control" placeholder="" name="actual_date_of_audit" type="text" id="actual_date_of_audit" value="{{ $auditinspection->actual_date_of_audit or '' }}">
                    <p class="help-block"></p>
                    @if($errors->has('actual_date_of_audit'))
                        <p class="help-block">
                            {{ $errors->first('actual_date_of_audit') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="reason_of_delay" class="control-label">Reason of delay</label>
                    <input class="form-control" placeholder="" name="reason_of_delay" type="text" id="reason_of_delay" value="{{ $auditinspection->reason_of_delay or '' }}">
                    <p class="help-block"></p>
                    @if($errors->has('reason_of_delay'))
                        <p class="help-block">
                            {{ $errors->first('reason_of_delay') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-12 row">
                <!-- <div class="col-md-6 form-group">
                    <label for="date_of_last_visit_of_area_manager" class="control-label">Date of last Visit (Area Manager): </label>
                    <input class="form-control" placeholder="" name="date_of_last_visit_of_area_manager" type="text" id="date_of_last_visit_of_area_manager" value="{{ $auditinspection->date_of_last_visit_of_area_manager or '' }}">
                    <p class="help-block"></p>
                    @if($errors->has('date_of_last_visit_of_area_manager'))
                        <p class="help-block">
                            {{ $errors->first('date_of_last_visit_of_area_manager') }}
                        </p>
                    @endif
                </div> -->

                

            </div>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="date_of_last_visit_of_cluster_manager" class="control-label">Date of last Visit Ops Team</label>
                    <input class="form-control" placeholder="" name="date_of_last_visit_of_cluster_manager" type="text" id="date_of_last_visit_of_cluster_manager" value="{{ $auditinspection->date_of_last_visit_of_cluster_manager or '' }}">
                    <p class="help-block"></p>
                    @if($errors->has('date_of_last_visit_of_cluster_manager'))
                        <p class="help-block">
                            {{ $errors->first('date_of_last_visit_of_cluster_manager') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="name_of_last_visit_cluster_manager" class="control-label">Name of Cluster Head:  </label>
                    
                    <select class="form-control select2"  id="name_of_last_visit_cluster_manager" name="name_of_last_visit_cluster_manager" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>    
                    </select>
                </div>
            </div>
            <div class="col-md-12 row">
            
                <div class="col-md-6 form-group">
                    <label for="fumigation_record_available_at_warehouse" class="control-label">Fumigation record available at warehouse:</label>
                    <select class="form-control select2" id="fumigation_record_available_at_warehouse" name="fumigation_record_available_at_warehouse" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('fumigation_record_available_at_warehouse'))
                        <p class="help-block">
                            {{ $errors->first('fumigation_record_available_at_warehouse') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="are_all_the_registers_available" class="control-label">Are all the registers available?*</label>
                    <select class="form-control select2" id="are_all_the_registers_available" name="are_all_the_registers_available" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('are_all_the_registers_available'))
                        <p class="help-block">
                            {{ $errors->first('are_all_the_registers_available') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-12 row">
                
                <div class="col-md-6 form-group">
                    <label for="are_all_the_registers_updated" class="control-label">Are all the registers  updated?*</label>
                    <select class="form-control select2" id="are_all_the_registers_updated" name="are_all_the_registers_updated" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('are_all_the_registers_updated'))
                        <p class="help-block">
                            {{ $errors->first('are_all_the_registers_updated') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="all_svs_ro_and_acknowledgments_available" class="control-label">All SVS,RO and Acknowledgments available?*</label>
                    <select class="form-control select2" id="all_svs_ro_and_acknowledgments_available" name="all_svs_ro_and_acknowledgments_available" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('all_svs_ro_and_acknowledgments_available'))
                        <p class="help-block">
                            {{ $errors->first('all_svs_ro_and_acknowledgments_available') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="name_of_the_cm" class="control-label">Name of the CM/Supervisor</label>
                    <input class="form-control" placeholder="" name="name_of_the_cm" type="text" id="name_of_the_cm" value="{{ $auditinspection->name_of_the_cm or '' }}">
                    <p class="help-block"></p>
                    @if($errors->has('name_of_the_cm'))
                        <p class="help-block">
                            {{ $errors->first('name_of_the_cm') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="date_of_guard_deployment_at_warehouse" class="control-label">Date of Guard Deployment at Warehouse*</label>
                    <input class="form-control" placeholder="" name="date_of_guard_deployment_at_warehouse" type="text" id="date_of_guard_deployment_at_warehouse" value="{{ $auditinspection->date_of_guard_deployment_at_warehouse or '' }}">
                    <p class="help-block"></p>
                    @if($errors->has('date_of_guard_deployment_at_warehouse'))
                        <p class="help-block">
                            {{ $errors->first('date_of_guard_deployment_at_warehouse') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="security_day_guard_attendance" class="control-label">Security Day Guard Attendance</label>
                    <input class="form-control" placeholder="" name="security_day_guard_attendance" type="text" id="security_day_guard_attendance" value="{{ $auditinspection->security_day_guard_attendance or '' }}">
                    <p class="help-block"></p>
                    @if($errors->has('security_day_guard_attendance'))
                        <p class="help-block">
                            {{ $errors->first('security_day_guard_attendance') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group" id="securityAvailable">
                    <label for="security_day_guard_available_during_the_visit" class="control-label">Security day guard available during the visit</label>
                    <select class="form-control select2" id="security_day_guard_available_during_the_visit" name="security_day_guard_available_during_the_visit" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="Not Applicable">Not Applicable</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('security_day_guard_available_during_the_visit'))
                        <p class="help-block">
                            {{ $errors->first('security_day_guard_available_during_the_visit') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="security_night_guard_attendance" class="control-label">Security Night Guard Attendance: </label>
                    <input class="form-control" placeholder="" name="security_night_guard_attendance" type="text" id="security_night_guard_attendance" value="{{ $auditinspection->security_night_guard_attendance or '' }}" onkeyup="hideShowSection1(this.value,['section11','section12'])">
                    <p class="help-block"></p>
                    @if($errors->has('security_night_guard_attendance'))
                        <p class="help-block">
                            {{ $errors->first('security_night_guard_attendance') }}
                        </p>
                    @endif
                </div>
                <div id="section11">
                <div class="col-md-6 form-group">
                    <label for="security_guard_attendance_registers_available" class="control-label">Security guard attendance registers available?*</label>
                    <select class="form-control select2" id="security_guard_attendance_registers_available" name="security_guard_attendance_registers_available" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="Not Applicable">Not Applicable</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('security_guard_attendance_registers_available'))
                        <p class="help-block">
                            {{ $errors->first('security_guard_attendance_registers_available') }}
                        </p>
                    @endif
                </div>
                </div>
            </div>
            
            <div class="col-md-12 row">
                <div id="section12">
                <div class="col-md-6 form-group">
                    <label for="security_guard_attendance_registers_updated" class="control-label">Security guard attendance registers  updated?</label>
                    <select class="form-control select2" id="security_guard_attendance_registers_updated" name="security_guard_attendance_registers_updated" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="Not Applicable">Not Applicable</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('security_guard_attendance_registers_updated'))
                        <p class="help-block">
                            {{ $errors->first('security_guard_attendance_registers_updated') }}
                        </p>
                    @endif
                </div>
                </div>
                <div class="col-md-6 form-group">
                    <label for="cleanliness_of_warehouse" class="control-label">Cleanliness of warehouse</label>
                    <select class="form-control select2" id="cleanliness_of_warehouse" name="cleanliness_of_warehouse" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes Clean">Yes Clean</option>
                        <option value="Not Clean">Not Clean</option>
                        <option value="Sweep-age not Collected">Sweep-age not Collected</option>
                        <option value="Transaction Area Not Clean">Transaction Area Not Clean</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('cleanliness_of_warehouse'))
                        <p class="help-block">
                            {{ $errors->first('cleanliness_of_warehouse') }}
                        </p>
                    @endif
                </div>
                
            </div>
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="pending_queries_resolved" class="control-label">Pending queries resolved</label>
                    <select class="form-control select2" id="pending_queries_resolved" name="pending_queries_resolved" tabindex="-1" aria-hidden="true">
                        <!-- <option value="">Select</option> -->
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('pending_queries_resolved'))
                        <p class="help-block">
                            {{ $errors->first('pending_queries_resolved') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    <label for="photo_of_visiter_register_attached" class="control-label">Photo of visitor register attached*</label>
                    <select class="form-control select2" id="photo_of_visiter_register_attached" name="photo_of_visiter_register_attached" tabindex="-1" aria-hidden="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('photo_of_visiter_register_attached'))
                        <p class="help-block">
                            {{ $errors->first('photo_of_visiter_register_attached') }}
                        </p>
                    @endif
                </div>
                
            </div>    
            <div class="col-md-12 row">
                <div class="col-md-6 form-group">
                    <label for="any_other_observation_or_remark" class="control-label">Any Other Observation/Remark:</label>
                    <select class="form-control select2" id="any_other_observation_or_remark" name="any_other_observation_or_remark" tabindex="-1" aria-hidden="true" onchange="showRemark(this.value);">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('any_other_observation_or_remark'))
                        <p class="help-block">
                            {{ $errors->first('any_other_observation_or_remark') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group" id="observe_remark" style="display:none;">
                    <label for="observation_remark" class="control-label">Remark:*</label>
                    <textarea class="form-control" name="observation_remark" id="observation_remark">{{$auditinspection->observation_remark or ''}}</textarea>
                    <p class="help-block"></p>
                    @if($errors->has('observation_remark'))
                        <p class="help-block">
                            {{ $errors->first('observation_remark') }}
                        </p>
                    @endif
                </div>
            </div>
                <div class="col-md-6 form-group">
                    <label for="warehouse_images" class="control-label">Warehouse Images*</label>
                    <div class="table-responsive">  
                       <table class="table table-bordered" id="dynamic_field">
                         
                            <tr>  
                                <td>
                                    <input type="file" name="warehouse_images[]"  class="form-control" id="image_0"/>
                                    @if(isset($auditinspection->image_1) && $auditinspection->image_1 !='')
                                    <img src="/storage/inspectionImages/{{$auditinspection->id}}/{{$auditinspection->image_1}}" width="100" width="100">
                                    @endif                                                        
                                </td>  
                                <td>
                                    <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                                </td>  
                            </tr>
                            @if(isset($auditinspection->image_2) && $auditinspection->image_2 !='')
                            <tr id="row2">
                                <td>
                                    <input type="file" name="warehouse_images[]" class="form-control" id="image_1">
                                    <img src="/storage/inspectionImages/{{$auditinspection->id}}/{{$auditinspection->image_2}}" width="100" width="100">
                                </td>
                                <td>
                                   <!--  
                                    <button type="button" name="remove" id="2" class="btn btn-danger btn_remove">X</button> -->
                                </td>
                            </tr>
                            @endif
                            @if(isset($auditinspection->image_3) && $auditinspection->image_3 !='')
                            <tr id="row3">
                                <td>
                                    <input type="file" name="warehouse_images[]" class="form-control" id="image_2">
                                    <img src="/storage/inspectionImages/{{$auditinspection->id}}/{{$auditinspection->image_3}}" width="100" width="100">
                                </td>
                                <td>
                                    <!-- <button type="button" name="remove" id="3" class="btn btn-danger btn_remove">X</button> -->
                                </td>
                            </tr>
                            @endif
                            @if(isset($auditinspection->image_4) && $auditinspection->image_4 !='')
                            <tr id="row4">
                                <td>
                                    <input type="file" name="warehouse_images[]" class="form-control" id="image_3">
                                    <img src="/storage/inspectionImages/{{$auditinspection->id}}/{{$auditinspection->image_4}}" width="100" width="100">
                                </td>
                                <td>
                                    <!-- <button type="button" name="remove" id="4" class="btn btn-danger btn_remove">X</button> -->
                                </td>
                            </tr>
                            @endif
                            @if(isset($auditinspection->image_5) && $auditinspection->image_5 !='')
                            <tr id="row5">
                                <td>
                                    <input type="file" name="warehouse_images[]" class="form-control" id="image_4">
                                    <img src="/storage/inspectionImages/{{$auditinspection->id}}/{{$auditinspection->image_5}}" width="100" width="100">
                                </td>
                                <td>
                                    <!-- <button type="button" name="remove" id="5" class="btn btn-danger btn_remove">X</button> -->
                                </td>
                            </tr>
                            @endif
                             
                       </table>    
                    </div>
                    <p class="help-block"></p>
                    @if($errors->has('warehouse_images'))
                        <p class="help-block">
                            {{ $errors->first('warehouse_images') }}
                        </p>
                    @endif
                </div>
                
            </div>

                  
        </div>
        </div>

        <div class="col-xs-12 row">
            <div class="form-group">
              @if(isset($auditinspection))
                    <button type="submit" class="btn btn-info pull-left">Update</button>
                @else
                     <button type="submit" class="btn btn-info pull-left">Add</button>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>

    </form>
@stop

@section('javascript') 
<script src="{{ url('adminlte/plugins/jqueryvalidation/jquery.validate.min.js') }}"></script>
    <script type="text/javascript">



          function hideShowSection1(val,sectionIds){
           
          if(val.trim()!="0"){
                sectionIds.forEach(function(ele){
                    $('#'+ele).show();
                });
            }else{

                sectionIds.forEach(function(ele){
                    $('#'+ele).hide();
                });
            }
        }

        $(document).ready(function () {

            $('#auditForm').validate({ 
                rules: {
                    fire_fighting_equipment_installed: {
                        required: true,
                    },
                    arya_flex_banner: {
                        required: true,
                    },
                    live_electricity: {
                        required: true,
                    },
                    proper_ventilation_in_the_warehouse: {
                        required: true,
                    },
                    plinth_height_is_less_than_1_feet: {
                        required: true,
                    },
                    dunnage_material_used: {
                        required: true,
                    },
                    lock_and_key_compromise: {
                        required: true,
                    },
                    any_physical_damage_to_warehouse_structure: {
                        required: true,
                    },
                    are_all_the_registers_available: {
                        required: true,
                    },
                    are_all_the_registers_updated: {
                        required: true,
                    },
                    all_svs_ro_and_acknowledgments_available: {
                        required: true,
                    },
                    date_of_guard_deployment_at_warehouse: {
                        required: true,
                    },
                    photo_of_visiter_register_attached: {
                        required: true,
                    },
                    name_of_last_visit_cluster_manager: {
                        required: true,
                    },                 
                    
                },

            });

        });

        $("#security_day_guard_attendance").keyup(function(){
            
          if($(this).val().trim()=='0'){
            $('#securityAvailable').hide();
          }else{
            $('#securityAvailable').show();
          }
        });

        function hideShow(val){
            //console.log(val);
            if(val.trim()=='Yes'){
                $('#structureDamage').show();
            }else{
                $('#structureDamage').hide();
            }
        } 

        function showRemark(val){
            //console.log(val);
            if(val.trim()=='Yes'){
                $('#observe_remark').show();
            }else{
                $('#observe_remark').hide();
            }
        }

        @if(isset($auditinspection))

        $('#fire_fighting_equipment_installed').val("{{$auditinspection->fire_fighting_equipment_installed or old('fire_fighting_equipment_installed')}}").trigger('change');
        $('#number_of_ffe').val("{{$auditinspection->number_of_ffe or old('number_of_ffe')}}").trigger('change');
        $('#number_of_ffe_expired').val("{{$auditinspection->number_of_ffe_expired or old('number_of_ffe_expired')}}").trigger('change');
        $('#arya_flex_banner').val("{{$auditinspection->arya_flex_banner or old('arya_flex_banner')}}").trigger('change');
        $('#live_electricity').val("{{$auditinspection->live_electricity or old('live_electricity')}}").trigger('change');
        $('#proper_ventilation_in_the_warehouse').val("{{$auditinspection->proper_ventilation_in_the_warehouse or old('proper_ventilation_in_the_warehouse')}}").trigger('change');
        $('#proper_ventilation_in_the_warehouse').val("{{$auditinspection->proper_ventilation_in_the_warehouse or old('proper_ventilation_in_the_warehouse')}}").trigger('change');
        $('#plinth_height_is_less_than_1_feet').val("{{$auditinspection->plinth_height_is_less_than_1_feet or old('plinth_height_is_less_than_1_feet')}}").trigger('change');
        $('#dunnage_material_used').val("{{$auditinspection->dunnage_material_used or old('dunnage_material_used')}}").trigger('change');

        $('#lock_and_key_compromise').val("{{$auditinspection->lock_and_key_compromise}}").trigger('change');
        $('#any_physical_damage_to_warehouse_structure').val("{{$auditinspection->any_physical_damage_to_warehouse_structure}}").trigger('change');
        $('#detail_of_physical_damage_to_warehouse_structure').val("{{$auditinspection->detail_of_physical_damage_to_warehouse_structure}}").trigger('change');
        $('#name_of_last_visit_area_manager').val("{{$auditinspection->name_of_last_visit_area_manager}}").trigger('change');
        $('#fumigation_record_available_at_warehouse').val("{{$auditinspection->fumigation_record_available_at_warehouse}}").trigger('change');
        $('#are_all_the_registers_available').val("{{$auditinspection->are_all_the_registers_available}}").trigger('change');
        $('#are_all_the_registers_updated').val("{{$auditinspection->are_all_the_registers_updated}}").trigger('change');
        $('#all_svs_ro_and_acknowledgments_available').val("{{$auditinspection->all_svs_ro_and_acknowledgments_available}}").trigger('change');
        $('#security_day_guard_available_during_the_visit').val("{{$auditinspection->security_day_guard_available_during_the_visit}}").trigger('change');
        $('#security_guard_attendance_registers_available').val("{{$auditinspection->security_guard_attendance_registers_available}}").trigger('change');
        $('#security_guard_attendance_registers_updated').val("{{$auditinspection->security_guard_attendance_registers_updated}}").trigger('change');
        $('#pending_queries_resolved').val("{{$auditinspection->pending_queries_resolved}}").trigger('change');
        $('#photo_of_visiter_register_attached').val("{{$auditinspection->photo_of_visiter_register_attached}}").trigger('change');
        $('#cleanliness_of_warehouse').val("{{$auditinspection->cleanliness_of_warehouse}}").trigger('change');
        $('#any_other_observation_or_remark').val("{{$auditinspection->any_other_observation_or_remark}}").trigger('change');

        @endif

        @if(isset($misdata))
            $('#name_of_last_visit_area_manager').val("{{ $misdata->name_of_area_manager}}").trigger('change');
        @endif        

      //to initialize datepicker plugin  
      $('#actual_date_of_audit,#date_of_last_visit_of_area_manager,#date_of_last_visit_of_cluster_manager,#date_of_guard_deployment_at_warehouse').datepicker({
        autoclose: true,
        format:"yyyy-mm-dd"
      });


      //To dynamically add image tag
      //var i=1;
      var noOfImages =0;  
      $('#add').click(function(){ 
        var noOfImages = document.querySelectorAll('input[type="file"]'); 
        if(noOfImages.length<5){
           //i++;  
           $('#dynamic_field').append(
            '<tr id="row'+(noOfImages.length+1)+'"><td><input type="file" name="warehouse_images[]"  class="form-control" /></td><td><button type="button" name="remove" id="'+(noOfImages.length+1)+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        }else{
            alert('You can\'t upload more than 5 images');
        }  
      });  

      //to dynamically remove image tag
      $(document).on('click', '.btn_remove', function(){
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      }); 


    $(document).ready(function()

    {
    var x = 1; //Initial field counter
    
    
        //Once add button is clicked
    $('.list_add_button').click(function()
        {
        var noOfExpiredFfe = parseInt($('#number_of_ffe_expired').val());
        var list_maxField = noOfExpiredFfe; //Input fields increment limitation

        //Check maximum number of input fields
        if(x < list_maxField){ 
            x++; //Increment field counter
            var list_fieldHTML = '<div class="col-md-12 row"><div class="col-md-6 form-group"><label></label><input name="expiry_date[]" type="text" placeholder="" class="form-control expiryDate"/></div><div class="col-md-5 form-group"><label></label><input name="expired_ffe_image[]" type="file" placeholder="" class="form-control"/></div><div class="col-md-1 form-group"><br><a href="javascript:void(0);" class="list_remove_button btn btn-danger">-</a></div></div>'; //New input field html 
            $('.list_wrapper').append(list_fieldHTML); //Add field html
        }
        });
    
        //Once remove button is clicked
        $('.list_wrapper').on('click', '.list_remove_button', function()
        {
           $(this).closest('div.row').remove(); //Remove field html
           x--; //Decrement field counter
        });
    });

$(document).on('focus',".expiryDate", function(){
    $(this).datepicker({
        autoclose: true,
        format:"yyyy-mm-dd"
      });
});

function getClusterHeads(ele){
    var amId = $('option:selected', ele).attr('data-amid');
    //console.log(amId);
    var _token = "<?php echo csrf_token();?>";
    //console.log(_token);   
         $.ajax({
             type: "POST",
             url: "{{ route('auditinspection.clusterheads') }}",
             data:{_token:_token,amId:amId},
             success: function(response){
             //console.log(response);    
             $("#name_of_last_visit_cluster_manager").find('option').remove();          
                 if(response!=''){
                 //parsed = $.parseJSON(response);
                 //console.log(parsed);
                 $("#name_of_last_visit_cluster_manager").append($("<option></option>").attr("value","").text("Select"));    
                 $.each(response, function(key, value) {
                     $("#name_of_last_visit_cluster_manager")
                         .append($("<option></option>")
                         .attr("value",value.name)
                         .text(value.name)); 
                  }); 

                //$("#name_of_last_visit_cluster_manager option[value="+<?=$misdata->name_of_cluster_head;?>+"]").prop("selected", true);

                // var text1 = "<?=$misdata->name_of_cluster_head;?>";
                // $("#name_of_last_visit_cluster_manager option").filter(function() {
                //     return this.text == text1; 
                // }).attr('selected', true);
                var text1 = "<?= isset($auditinspection->name_of_last_visit_cluster_manager)? $auditinspection->name_of_last_visit_cluster_manager:"";?>";
                if(text1!=""){
                    $("#name_of_last_visit_cluster_manager option").filter(function() {
                        return this.text == text1; 
                    }).attr('selected', true);
                }else{
                    var text2 = "<?=$misdata->name_of_cluster_head;?>";
                    $("#name_of_last_visit_cluster_manager option").filter(function() {
                        return this.text == text2; 
                    }).attr('selected', true);
                }
               
             }
        }
     });
     
 }
    </script>    
@endsection